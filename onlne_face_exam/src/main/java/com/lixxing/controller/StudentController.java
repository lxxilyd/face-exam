package com.lixxing.controller;

import com.lixxing.model.ApiResult;
import com.lixxing.model.Student;
import com.lixxing.service.StudentService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Api(tags = "学生管理")
@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;
    @ApiOperation("分页查询所有学生")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",required = true,dataType = "Integer")
    })
    @GetMapping("/students/{page}/{size}")
    public ApiResult findAll(@PathVariable Integer page, @PathVariable Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Student> res = studentService.findAll(pageable);
        if (null == res) return  ApiResultHandler.buildApiResult(400,"分页查询所有学生",null);
        return  ApiResultHandler.buildApiResult(200,"分页查询所有学生",res);
    }
    @ApiOperation("获取指定学生Id的学生信息")
    @ApiImplicitParam(name = "studentId",value = "学生Id",required = true,dataType = "Integer")
    @GetMapping("/student/{studentId}")
    public ApiResult findById(@PathVariable("studentId") Integer studentId) {
        Student res = studentService.findById(studentId);
        if (res != null) {
        return ApiResultHandler.buildApiResult(200,"请求成功",res);
        } else {
            return ApiResultHandler.buildApiResult(404,"查询的用户不存在",null);
        }
    }
    @ApiOperation("删除指定学生Id的学生信息")
    @ApiImplicitParam(name = "studentId",value = "学生Id",required = true,dataType = "Integer")
    @DeleteMapping("/student/{studentId}")
    public ApiResult deleteById(@PathVariable("studentId") Integer studentId) {
        return ApiResultHandler.buildApiResult(200,"删除成功",studentService.deleteById(studentId));
    }
    @ApiOperation("修改学生密码")
    @ApiImplicitParam(name = "student",value = "学生实体数据",required = true,dataType = "Student")
    @PutMapping("/studentPWD")
    public ApiResult updatePwd(@RequestBody Student student) {
        int res = studentService.updatePwd(student);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"密码更新成功",null);
        }
        return ApiResultHandler.buildApiResult(200,"密码更新失败",null);
    }
    @ApiOperation("修改学生信息")
    @ApiImplicitParam(name = "student",value = "学生实体数据",required = true,dataType = "Student")
    @PutMapping("/student")
    public ApiResult update(@RequestBody Student student) {
        System.out.println(student);
        int res = studentService.update(student);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"更新成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"更新失败",res);
    }
    @ApiOperation("添加学生")
    @ApiImplicitParam(name = "student",value = "学生实体数据",required = true,dataType = "Student")
    @PostMapping("/student")
    public ApiResult add(@RequestBody Student student) {
        System.out.println(student);
        int res = studentService.add(student);
        if (res == 1) {
            return ApiResultHandler.buildApiResult(200,"添加成功",null);
        }else {
            return ApiResultHandler.buildApiResult(400,"添加失败",null);
        }
    }
}
