package com.lixxing.controller;

import com.lixxing.model.ApiResult;
import com.lixxing.model.JudgeQuestion;
import com.lixxing.service.JudgeQuestionService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Api(tags = "判断题管理")
@RestController
public class JudgeQuestionController {

    @Autowired
    private JudgeQuestionService judgeQuestionService;

    @ApiOperation("添加判断题")
    @ApiImplicitParam(name = "judgeQuestion",value = "判断题实体数据",
            required = true,dataType = "JudgeQuestion")
    @PostMapping("/judgeQuestion")
    public ApiResult add(@RequestBody JudgeQuestion judgeQuestion) {
        System.out.println(judgeQuestion);
        int res = judgeQuestionService.add(judgeQuestion);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"添加成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"添加失败",res);
    }

    @ApiOperation("查询最后一条记录id")
    @GetMapping("/judgeQuestionId")
    public ApiResult findOnlyQuestionId() {
        JudgeQuestion res = judgeQuestionService.findOnlyQuestionId();
        return  ApiResultHandler.buildApiResult(200,"查询成功",res);
    }

    @ApiOperation("查询指定Id的判断题信息")
    @ApiImplicitParam(name = "questionId",value = "判断题Id",required = true,dataType = "Integer")
    @GetMapping("/judgeQuestion/{questionId}")
    public ApiResult findByQuestionId(@PathVariable("questionId")Integer questionId) {
        JudgeQuestion res = judgeQuestionService.findByQuestionId(questionId);
        return ApiResultHandler.buildApiResult(200,"查询成功",res);
    }

    @ApiOperation("分页获取判断题信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/judgeQuestion/{page}/{size}")
    public ApiResult findJudgeQuestion(@PathVariable Integer page, @PathVariable Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<JudgeQuestion> res = judgeQuestionService.findAll(pageable);
        return ApiResultHandler.buildApiResult(200,"查询成功",res);
    }

    @ApiOperation("分页获取指定课程的判断题信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId",value = "课程Id",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/judgeQuestion/{courseId}/{page}/{size}")
    public ApiResult findJudgeQuestion(@PathVariable Integer courseId,
                                       @PathVariable Integer page,
                                       @PathVariable Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<JudgeQuestion> res = judgeQuestionService.findAll(pageable,courseId);
        return ApiResultHandler.buildApiResult(200,"查询成功",res);
    }

    @ApiOperation("修改判断题")
    @ApiImplicitParam(name = "multiQuestion",value = "判断题实体数据",required = true,dataType = "MultiQuestion")
    @PutMapping("/judgeQuestion")
    public ApiResult update(@RequestBody JudgeQuestion judgeQuestion) {
        int res = judgeQuestionService.update(judgeQuestion);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"更新成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"更新失败",res);
    }

    @ApiOperation("删除指定Id的判断题信息")
    @ApiImplicitParam(name = "questionId",value = "判断题Id",required = true,dataType = "Integer")
    @DeleteMapping("/judgeQuestion/{questionId}")
    public ApiResult delete(@PathVariable("questionId") Integer questionId) {
        System.out.println(questionId);
        int res = judgeQuestionService.delete(questionId);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"删除成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"删除失败",res);
    }
}
