package com.lixxing.controller;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.template.MetaTemplate;
import com.lixxing.model.*;
import com.lixxing.service.*;
import com.lixxing.utils.ApiResultHandler;
import com.lixxing.utils.ExportUtil;
import com.lixxing.utils.ImportUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.xwpf.usermodel.XWPFComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags = "试卷管理")
@RestController
public class PaperController {

    @Autowired
    private PaperService paperService;

    @ApiOperation("获取所有试卷")
    @GetMapping("/papers")
    public ApiResult<Paper> findAll() {
       ApiResult res =  ApiResultHandler.buildApiResult(200,"请求成功", paperService.findAll());
       return  res;
    }

    @ApiOperation("分页获取试卷信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",required = true,dataType = "Integer/PathVariable"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",required = true,dataType = "Integer/PathVariable"),
    })
    @GetMapping("/papers/{page}/{size}")
    public ApiResult findAll(@PathVariable("page") Integer page,
                             @PathVariable("size") Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Paper> all = paperService.findAll(pageable);
        return  ApiResultHandler.buildApiResult(200,"请求成功",all);
    }

    @ApiOperation("获取指定试卷Id的试卷信息")
    @ApiImplicitParam(name = "paperId",value = "试卷Id",required = true,dataType = "PathVariable/Integer")
    @GetMapping("/paper/{paperId}")
    public ApiResult findById(@PathVariable("paperId") Integer paperId) {
        Paper paper = paperService.findById(paperId);
        return  ApiResultHandler.buildApiResult(200,"请求成功",paper);
    }
    @ApiOperation("添加试卷信息")
    @ApiImplicitParam(name = "paper",value = "试卷实体数据",required = true,dataType = "Paper")
    @PostMapping("/paper")
    public ApiResult add(@RequestBody Paper paper) {
        System.out.println(paper);
        int res = paperService.add(paper);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"添加成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"添加失败",res);
    }

    @ApiOperation("下载指定Id的试卷")
    @ApiImplicitParam(name = "paperId",value = "试卷Id",required = true,dataType = "Integer")
    @GetMapping(value = "/paper/download/{paperId}")
    public void findByIdAndDownload(@PathVariable("paperId") Integer paperId,
                                      HttpServletResponse response) throws IOException {
        Paper paper = paperService.findById(paperId);
        response.setContentType("application/octet-stream");
        response.setHeader("Access-Control-Expose-Headers","Content-Disposition");
        response.setHeader("Content-Disposition", "attachment;filename="+URLEncoder.encode(paper.getPaperName()+".docx", "utf-8"));
        byte[] exportWord = ExportUtil.getExportWord(paper);
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(exportWord);
        outputStream.flush();
        outputStream.close();
    }

    @ApiOperation("导入试卷信息")
    @ApiImplicitParam(name = "paperId",value = "试卷Id",required = true,dataType = "Integer")
    @PostMapping(value = "/paper/upload")
    public ApiResult addPaperByUpload(@RequestParam("paper") MultipartFile paper) throws IOException, OpenXML4JException {
        ImportUtil.ImportWord(paper);
        return ApiResultHandler.buildApiResult(200,"导入成功",null);
    }

    @ApiOperation("修改试卷信息")
    @ApiImplicitParam(name = "paper",value = "试卷实体数据",required = true,dataType = "Paper")
    @PutMapping("/paper")
    public ApiResult update(@RequestBody Paper paper) {
        System.out.println(paper);
        int res = paperService.update(paper);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"修改成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"修改失败",res);
    }

    @ApiOperation("删除指定试卷Id的试卷信息")
    @ApiImplicitParam(name = "paperId",value = "试卷Id",required = true,dataType = "PathVariable/Integer")
    @DeleteMapping("/paper/{paperId}")
    public ApiResult delete(@PathVariable("paperId") Integer paperId) {
        int res = paperService.delete(paperId);
        if (res == 0){
            return  ApiResultHandler.buildApiResult(200,"删除失败",null);
        }
        return  ApiResultHandler.buildApiResult(200,"删除成功",null);
    }
}
