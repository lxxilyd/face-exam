package com.lixxing.controller;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lixxing.model.*;
import com.lixxing.service.*;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Api(tags = "作答结果管理")
@RestController
public class AnswerController {

    @Autowired
    private AnswerService answerService;
    @Autowired
    private ScoreService scoreService;

    @ApiOperation("分页查询所有作答结果")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/answers/{page}/{size}")
    public ApiResult findAll(@PathVariable("page") Integer page,
                                     @PathVariable("size") Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Answer> answerPage = answerService.findAll(pageable);
        return ApiResultHandler.buildApiResult(200,"分页查询所有作答结果",answerPage);
    }

    @ApiOperation("查询所有作答结果")
    @GetMapping("/answers")
    public ApiResult findAll(){
        List<Answer> answers = answerService.findAll();
        return ApiResultHandler.buildApiResult(200,"查询所有作答结果",answers);
    }

    @ApiOperation("查询指定id的作答结果")
    @ApiImplicitParam(name = "answerId",value = "作答结果id",required = true,dataType = "Long")
    @GetMapping("/answer/{answerId}")
    public ApiResult findById(@PathVariable("answerId") Long answerId){
        Answer answer = answerService.findById(answerId);
        return ApiResultHandler.buildApiResult(200,"查询指定id的作答结果",answer);
    }

    @ApiOperation("查询指定考试指定学生的作答结果")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "examCode",value = "考试Id",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "studentId",value = "学生Id",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/answer/{examCode}/{studentId}")
    public ApiResult findByStudentAndExamAnd(@PathVariable("examCode") Integer examCode,
                                             @PathVariable("studentId") Integer studentId){
        List<Answer> answers = answerService.findAllByStudentAndExam(
                new Student(studentId),new Exam(examCode));
        return ApiResultHandler.buildApiResult(200,"查询指定学生指定考试考试作答结果"
                ,answers);
    }

    @ApiOperation("根据作答结果计算成绩")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startTime",value = "作答开始时间",
                    required = true,dataType = "Long"),
            @ApiImplicitParam(name = "endTime",value = "作答结束时间",
                    required = true,dataType = "Long"),
    })
    @PostMapping("/answer/getScore/{startTime}/{endTime}")
    public ApiResult getScoreByAnswer(@RequestBody List<Answer> answers,
                                      @PathVariable("startTime") Long startTime,
                                      @PathVariable("endTime") Long endTime){
        Score score = answerService.getScore(answers, startTime, endTime);
        scoreService.add(score);
        return ApiResultHandler.buildApiResult(200,"查询指定学生指定考试考试作答结果"
                ,score);
    }



    @ApiOperation("查询指定id指定考试指定学生的作答结果")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "answerId",value = "作答结果id",
                    required = true,dataType = "Long"),
            @ApiImplicitParam(name = "student",value = "学生实体",
                    required = true,dataType = "Student"),
            @ApiImplicitParam(name = "exam",value = "考试实体",
                    required = true,dataType = "Exam")
    })
    @PostMapping("/answerBy/{answerId}")
    public ApiResult findByStudentAndExamAndId(@PathVariable Long answerId,
                                               @RequestBody Student student,
                                               @RequestBody Exam exam){
        Answer answer = answerService.findByStudentAndExamAndId(answerId,student,exam);
        return ApiResultHandler.buildApiResult(200,"查询指定id指定考试指定学生的作答结果",answer);
    }

    @ApiOperation("删除指定id的作答结果")
    @ApiImplicitParam(name = "answerId",value = "作答结果id",required = true,dataType = "Long")
    @DeleteMapping("/answer/{answerId}")
    public ApiResult delete(@PathVariable Long answerId){
        int res = answerService.delete(answerId);
        return ApiResultHandler.buildApiResult(200,"删除指定id的作答结果",res);
    }

    @ApiOperation("更新作答结果")
    @ApiImplicitParam(name = "answer",value = "作答结果实体数据",required = true,dataType = "Answer")
    @PutMapping("/answer")
    public ApiResult update(@RequestBody Answer answer){
        int res = answerService.update(answer);
        return ApiResultHandler.buildApiResult(200,"更新作答结果",res);
    }

    @ApiOperation("添加作答结果")
    @ApiImplicitParam(name = "answer",value = "作答结果实体数据",required = true,dataType = "Answer")
    @PostMapping("/answer")
    public ApiResult add(@RequestBody Answer answer){
        System.out.println(answer);
        int res = answerService.add(answer);
        return ApiResultHandler.buildApiResult(200,"更新作答结果",res);
    }

}
