package com.lixxing.controller;

import com.lixxing.model.ApiResult;
import com.lixxing.model.Course;
import com.lixxing.service.CourseService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Api(tags = "课程管理")
@RestController
public class CourseController {
    @Autowired
    private CourseService courseService;

    @ApiOperation("分页获取全部课程")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",required = true,dataType = "Integer/PathVariable"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",required = true,dataType = "Integer/PathVariable")
    })
    @GetMapping("/courses/{page}/{size}")
    public ApiResult findAll(@PathVariable("page")Integer page,
                             @PathVariable("size")Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        return ApiResultHandler.success(courseService.findAll(pageable));
    }
    @ApiOperation("获取全部课程")
    @GetMapping("/courses")
    public ApiResult findAll(){
        return ApiResultHandler.success(courseService.findAll());
    }
    @ApiOperation("获取指定课程Id的课程信息")
    @ApiImplicitParam(name = "courseId",value = "课程Id",required = true,dataType = "Integer/PathVariable")
    @GetMapping("/course/{courseId}")
    public ApiResult findById(@PathVariable("courseId") Integer courseId){
        return ApiResultHandler.success(courseService.findById(courseId));
    }
    @ApiOperation("删除指定课程Id的课程信息")
    @ApiImplicitParam(name = "courseId",value = "课程Id",required = true,dataType = "Integer/PathVariable")
    @DeleteMapping("/course/{courseId}")
    public ApiResult deleteById(@PathVariable("courseId") Integer courseId){
        courseService.delete(courseId);
        return ApiResultHandler.success();
    }
    @ApiOperation("修改指定课程Id的课程信息")
    @ApiImplicitParam(name = "course",value = "课程实体数据",required = true,dataType = "Course")
    @PutMapping("/course")
    public ApiResult update(@RequestBody Course course){
        return ApiResultHandler.success(courseService.update(course));
    }

    @ApiOperation("新增课程")
    @ApiImplicitParam(name = "course", value = "course实体数据" ,required = true, dataType = "Course")
    @PostMapping("/course")
    public ApiResult add(@RequestBody Course course){
        return ApiResultHandler.success(courseService.add(course));
    }

}
