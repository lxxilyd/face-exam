package com.lixxing.controller;

import com.lixxing.model.ApiResult;
import com.lixxing.model.FillQuestion;
import com.lixxing.service.FillQuestionService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Api(tags = "填空题管理")
@RestController
public class FillQuestionController {

    @Autowired
    private FillQuestionService fillQuestionService;

    @ApiOperation("查询指定Id的填空题信息")
    @ApiImplicitParam(name = "questionId",value = "填空题Id",required = true,dataType = "Integer")
    @GetMapping("/fillQuestion/{questionId}")
    public ApiResult findByQuestionId(@PathVariable("questionId")Integer questionId) {
        FillQuestion res = fillQuestionService.findByQuestionId(questionId);
        return ApiResultHandler.buildApiResult(200,"查询成功",res);
    }

    @ApiOperation("查询最后一条记录id")
    @GetMapping("/fillQuestionId")
    public ApiResult findOnlyQuestionId() {
        FillQuestion res = fillQuestionService.findOnlyQuestionId();
        return ApiResultHandler.buildApiResult(200,"查询成功",res);
    }

    @ApiOperation("分页获取填空题信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/fillQuestion/{page}/{size}")
    public ApiResult findFillQuestion(@PathVariable Integer page, @PathVariable Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<FillQuestion> res = fillQuestionService.findAll(pageable);
        return ApiResultHandler.buildApiResult(200,"查询成功",res);
    }

    @ApiOperation("分页获取指定课程的填空题信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId",value = "课程Id",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/fillQuestion/{courseId}/{page}/{size}")
    public ApiResult findFillQuestion(@PathVariable Integer courseId,
                                      @PathVariable Integer page,
                                      @PathVariable Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<FillQuestion> res = fillQuestionService.findAll(pageable,courseId);
        return ApiResultHandler.buildApiResult(200,"查询成功",res);
    }

    @ApiOperation("添加填空题")
    @ApiImplicitParam(name = "multiQuestion",value = "填空题实体数据",required = true,dataType = "MultiQuestion")
    @PostMapping("/fillQuestion")
    public ApiResult add(@RequestBody FillQuestion fillQuestion) {
        int res = fillQuestionService.add(fillQuestion);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"添加成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"添加失败",res);
    }

    @ApiOperation("修改填空题")
    @ApiImplicitParam(name = "multiQuestion",value = "填空题实体数据",required = true,dataType = "MultiQuestion")
    @PutMapping("/fillQuestion")
    public ApiResult update(@RequestBody FillQuestion fillQuestion) {
        int res = fillQuestionService.update(fillQuestion);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"修改成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"修改失败",res);
    }

    @ApiOperation("删除指定Id的填空题信息")
    @ApiImplicitParam(name = "questionId",value = "填空题Id",required = true,dataType = "Integer")
    @DeleteMapping("/fillQuestion/{questionId}")
    public ApiResult delete(@PathVariable("questionId")Integer questionId) {
        int res = fillQuestionService.delete(questionId);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"删除成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"删除失败",res);
    }
}
