package com.lixxing.controller;

import com.lixxing.model.ApiResult;
import com.lixxing.model.Clazz;
import com.lixxing.model.Profession;
import com.lixxing.service.ClazzService;
import com.lixxing.service.ProfessionService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Api(tags = "班级管理")
@RestController
public class ClazzController {
    @Autowired
    private ClazzService clazzService;
    @Autowired
    private ProfessionService professionService;

    @ApiOperation("分页获取全部班级")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",required = true,dataType = "Integer/PathVariable"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",required = true,dataType = "Integer/PathVariable")
    })
    @GetMapping("/clazzs/{page}/{size}")
    public ApiResult findAll(@PathVariable("page")Integer page,
                             @PathVariable("size")Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        return ApiResultHandler.success(clazzService.findAll(pageable));
    }
    @ApiOperation("获取全部班级")
    @GetMapping("/clazzs")
    public ApiResult findAll(){
        System.out.println("查询全部");
        return ApiResultHandler.success(clazzService.findAll());
    }
    @ApiOperation("获取指定班级Id的班级信息")
    @ApiImplicitParam(name = "clazzId",value = "班级Id",required = true,dataType = "Integer/PathVariable")
    @GetMapping("/clazz/{clazzId}")
    public ApiResult findById(@PathVariable("clazzId") Integer clazzId){
//        System.out.println("根据ID查找");
        return ApiResultHandler.success(clazzService.findById(clazzId));
    }
    @ApiOperation("删除指定班级Id的班级信息")
    @ApiImplicitParam(name = "clazzId",value = "班级Id",required = true,dataType = "Integer/PathVariable")
    @DeleteMapping("/clazz/{clazzId}")
    public ApiResult deleteById(@PathVariable("clazzId") Integer clazzId){
        clazzService.delete(clazzId);
        return ApiResultHandler.success();
    }
    @ApiOperation("修改指定班级Id的班级信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "clazzId",value = "班级Id",required = true,dataType = "Integer/PathVariable"),
            @ApiImplicitParam(name = "clazz",value = "班级实体数据",required = true,dataType = "Clazz")
    })
    @PutMapping("/clazz")
    public ApiResult update(@RequestBody Clazz clazz){
        return ApiResultHandler.success(clazzService.update(clazz));
    }

    @ApiOperation("新增班级")
    @ApiImplicitParam(name = "clazz", value = "Clazz实体数据" ,required = true, dataType = "Clazz")
    @PostMapping("/clazz")
    public ApiResult add(@RequestBody Clazz clazz){
        System.out.println(clazz);
        return ApiResultHandler.success(clazzService.add(clazz));
    }

}
