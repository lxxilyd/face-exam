package com.lixxing.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.extra.mail.MailUtil;
import com.lixxing.model.*;
import com.lixxing.service.*;
import com.lixxing.utils.ApiResultHandler;
import com.lixxing.utils.TokenUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisServer;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Api(tags = "登录")
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private TokenUtil tokenUtil;
    @Autowired
    private RedisService redisService;
    @Autowired
    AdminService adminService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    StudentService studentService;

    @ApiOperation("用户登录")
    @ApiImplicitParam(name = "login",value = "用户实体数据",required = true,dataType = "Login")
    @PostMapping("/login")
    public ApiResult login(@RequestBody Login login) {
        Integer username = login.getUsername();
        String password = login.getPassword();
        String region = login.getRegion();
        String verifyCode = login.getVerifyCode();
        String verifyCodeId = login.getVerifyCodeId();
        if (null == verifyCode)
            return ApiResultHandler.buildApiResult(400, "验证码不能为空", null);
        if (null == redisService.get(verifyCodeId))
            return ApiResultHandler.buildApiResult(400, "验证码已过期", null);
        if (!redisService.get(verifyCodeId).equals(verifyCode))
            return ApiResultHandler.buildApiResult(400, "验证码不正确", null);
        String token;
        switch (region){
            case "管理员":{
                Admin adminRes = loginService.adminLogin(username, password);
                if (adminRes != null) {
                    if (adminRes.getState() == 0)
                        return ApiResultHandler.buildApiResult(400, "用户已禁用", null);
                    adminRes.setPwd(null);
                    token = tokenUtil.getToken(username.toString(),region);
                    adminRes.setToken(token);
                    return ApiResultHandler.buildApiResult(200, "登录成功", adminRes);
                }
                return ApiResultHandler.buildApiResult(400, "用户名或者密码错误", null);
            }
            case "教师":{
                Teacher teacherRes = loginService.teacherLogin(username,password);
                if (teacherRes != null) {
                    if (teacherRes.getState() == 0)
                        return ApiResultHandler.buildApiResult(400, "用户已禁用", null);
                    teacherRes.setPwd(null);
                    token = tokenUtil.getToken(username.toString(),region);
                    teacherRes.setToken(token);
                    return ApiResultHandler.buildApiResult(200, "登录成功", teacherRes);
                }
                return ApiResultHandler.buildApiResult(400, "用户名或者密码错误", null);
            }
            case "学生":{
                Student studentRes = loginService.studentLogin(username,password);
                if (studentRes != null) {
                    if (studentRes.getState() == 0)
                        return ApiResultHandler.buildApiResult(400, "用户已禁用", null);
                    studentRes.setPwd(null);
                    token = tokenUtil.getToken(username.toString(),region);
                    studentRes.setToken(token);
                    return ApiResultHandler.buildApiResult(200, "登录成功", studentRes);
                }
                return ApiResultHandler.buildApiResult(400, "用户名或者密码错误", null);
            }
            default: return ApiResultHandler.buildApiResult(400, "登录失败", null);
        }
    }

    @ApiOperation("密码重置")
    @ApiImplicitParam(name = "reseter",value = "表单数据实体",required = true,dataType = "Reseter")
    @PostMapping("/resetPassword")
    public ApiResult resetPassword(@RequestBody Reseter reseter) {
        Integer username = reseter.getUsername();
        String email = reseter.getEmail();
        String region = reseter.getRegion();
        String verifyCode = reseter.getVerifyCode();
        String verifyCodeId = reseter.getVerifyCodeId();
        String emailVerifyCode = reseter.getEmailVerifyCode();
        String emailVerifyCodeId = reseter.getEmailVerifyCodeId();
        String password = reseter.getPassword();
        String confirmPassword = reseter.getConfirmPassword();

        if (null == username)
            return ApiResultHandler.buildApiResult(400, "用户名不能为空", null);
        if (null == verifyCode)
            return ApiResultHandler.buildApiResult(400, "验证码不能为空", null);
        if (null == redisService.get(verifyCodeId))
            return ApiResultHandler.buildApiResult(400, "验证码已过期", null);
        if (!redisService.get(verifyCodeId).equals(verifyCode))
            return ApiResultHandler.buildApiResult(400, "验证码不正确", null);
        if (null == emailVerifyCode)
            return ApiResultHandler.buildApiResult(400, "邮箱验证码不能为空", null);
        if (null == redisService.get(emailVerifyCodeId))
            return ApiResultHandler.buildApiResult(400, "邮箱验证码已过期", null);
        if (!redisService.get(emailVerifyCodeId).equals(emailVerifyCode))
            return ApiResultHandler.buildApiResult(400, "邮箱验证码不正确", null);
        if (password.isEmpty())
            return ApiResultHandler.buildApiResult(400, "密码不能为空", null);
        if (!password.equals(confirmPassword))
            return ApiResultHandler.buildApiResult(400, "两次密码不一致", null);
        switch (region){
            case "管理员":{
                Admin adminRes = adminService.findByIdAndEmail(username, email);
                if (adminRes != null) {
                    adminRes.setPwd(password);
                    adminService.updatePwd(adminRes);
                    return ApiResultHandler.buildApiResult(200, "密码重置成功", adminRes);
                }
                return ApiResultHandler.buildApiResult(400, "用户或绑定的邮箱不存在", null);
            }
            case "教师":{
                Teacher teacherRes = teacherService.findByIdAndEmail(username,email);
                if (teacherRes != null) {
                    teacherRes.setPwd(password);
                    teacherService.updatePwd(teacherRes);
                    return ApiResultHandler.buildApiResult(200, "密码重置成功", teacherRes);
                }
                return ApiResultHandler.buildApiResult(400, "用户或绑定的邮箱不存在", null);
            }
            case "学生":{
                Student studentRes = studentService.findByIdAndEmail(username,email);
                if (studentRes != null) {
                    studentRes.setPwd(password);
                    studentService.updatePwd(studentRes);
                    return ApiResultHandler.buildApiResult(200, "密码重置成功", studentRes);
                }
                return ApiResultHandler.buildApiResult(400, "用户或绑定的邮箱不存在", null);
            }
            default: return ApiResultHandler.buildApiResult(400, "重置失败", null);
        }
    }

    @ApiOperation("获取验证码")
    @GetMapping(value = "/getVerifyCode")
    public ApiResult getVerifyCode() throws IOException {
        ShearCaptcha captcha = CaptchaUtil
                .createShearCaptcha(200,100,4,4);
        String imageBase64 = captcha.getImageBase64();
        String verifyCodeId = UUID.randomUUID().toString().replace("-", "");
        String code = captcha.getCode();
        redisService.set(verifyCodeId,code,60*5);
        Map<String,Object> verify = new HashMap<>();
        verify.put("verifyCodeId", verifyCodeId);
        verify.put("verifyCodeImg",imageBase64);
        return ApiResultHandler.buildApiResult(200,"获取成功",verify);
    }

    @ApiOperation("获取邮箱验证码")
    @GetMapping(value = "/getEmailVerifyCode/{toEmail}")
    public ApiResult getEmailVerifyCode(@PathVariable String toEmail) {
        ShearCaptcha captcha = CaptchaUtil
                .createShearCaptcha(200,100,4,4);
        String verifyCodeId = UUID.randomUUID().toString().replace("-", "");
        String code = captcha.getCode();
        final String mailSuffix = "\n此验证码五分钟有效，用于进行密码重置。";
        redisService.set(verifyCodeId,code,60*5);
        if (toEmail.isEmpty())
            return ApiResultHandler.buildApiResult(200,"邮箱不能为空",null);
        MailUtil.send(toEmail,"重置密码","验证码："+code+mailSuffix,false,null);
        Map<String,Object> verify = new HashMap<>();
        verify.put("emailVerifyCodeId", verifyCodeId);
        return ApiResultHandler.buildApiResult(200,"发送成功",verify);
    }
}
