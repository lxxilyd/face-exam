package com.lixxing.controller;

import com.lixxing.model.ApiResult;
import com.lixxing.model.Score;
import com.lixxing.service.ScoreService;
import com.lixxing.utils.ApiResultHandler;
import com.lixxing.utils.ExportUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

@Api(tags = "成绩管理")
@RestController
public class ScoreController {
    @Autowired
    private ScoreService scoreService;

    @ApiOperation("查询所有的成绩")
    @GetMapping("/scores")
    public ApiResult findAll() {
        List<Score> res = scoreService.findAll();
        return ApiResultHandler.buildApiResult(200,"查询所有学生成绩",res);
    }

    @ApiOperation("分页查询所有的成绩")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer")
    })
    @GetMapping("/scores/{page}/{size}")
    public ApiResult findAll(@PathVariable("page") Integer page,
                             @PathVariable("size") Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Score> res = scoreService.findAll(pageable);
        return ApiResultHandler.buildApiResult(200, "分页查询成绩", res);
    }

    @ApiOperation("分页查询指定学生的成绩")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "studentId",value = "学生学号",
                    required = true,dataType = "Integer")
    })
    @GetMapping("/scores/{page}/{size}/{studentId}")
    public ApiResult findById(@PathVariable("page") Integer page, @PathVariable("size") Integer size, @PathVariable("studentId") Integer studentId) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Score> res = scoreService.findById(pageable, studentId);
        return ApiResultHandler.buildApiResult(200, "根据ID查询成绩", res);
    }

    @ApiOperation("查询指定Id的成绩")
    @ApiImplicitParam(name = "scoreId",value = "成绩Id",required = true,dataType = "Integer")
    @GetMapping("/score/{scoreId}")
    public ApiResult findById(@PathVariable("scoreId") Integer scoreId) {
        Score score = scoreService.findById(scoreId);
        if (null != score) {
            return ApiResultHandler.buildApiResult(200, "根据ID查询成绩", score);
        } else {
            return ApiResultHandler.buildApiResult(204, "ID不存在", score);
        }
    }

    @ApiOperation("查询指定学生的成绩")
    @ApiImplicitParam(name = "studentId",value = "学生学号",required = true,dataType = "Integer")
    @GetMapping("/scoresByStudent/{studentId}")
    public ApiResult findByStudentId(@PathVariable("studentId") Integer studentId) {
        List<Score> res = scoreService.findByStudentId(studentId);
        if (!res.isEmpty()) {
            return ApiResultHandler.buildApiResult(200, "根据ID查询成绩", res);
        } else {
            return ApiResultHandler.buildApiResult(204, "ID不存在", res);
        }
    }


    @ApiOperation("添加成绩")
    @ApiImplicitParam(name = "score",value = "成绩实体",required = true,dataType = "Score")
    @PostMapping("/score")
    public ApiResult add(@RequestBody Score score) {
        int res = scoreService.add(score);
        if (res == 0) {
            return ApiResultHandler.buildApiResult(204,"成绩添加失败",res);
        }else {
            return ApiResultHandler.buildApiResult(200,"成绩添加成功",res);
        }
    }

    @ApiOperation("根据考试查询所有成绩")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "examCode",value = "考试Id",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/scores/{examCode}")
    public ApiResult findByExamCode(@PathVariable("examCode") Integer examCode) {

        List<Score> scores = scoreService.findByExamCode(examCode);
        return ApiResultHandler.buildApiResult(200,"查询成功",scores);
    }

    @ApiOperation("根据考试分页查询成绩")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "examCode",value = "考试Id",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/score/{examCode}/{page}/{size}")
    public ApiResult findByExamCode(@PathVariable("examCode") Integer examCode,
                                    @PathVariable("page") Integer page,
                                    @PathVariable("size") Integer size) {

        Page<Score> scores = scoreService.findByExamCode(PageRequest.of(page-1,size),examCode);
        return ApiResultHandler.buildApiResult(200,"查询成功",scores);
    }

    @GetMapping("/score/download/{examCode}")
    public void downloadScoreTable(@PathVariable("examCode") Integer examCode
            , HttpServletResponse response) throws IOException {

        List<Score> scores = scoreService.findByExamCode(examCode);
        byte[] exportExcel = ExportUtil.getExportExcel(scores);
        response.setContentType("application/octet-stream");
        response.setHeader("Access-Control-Expose-Headers","Content-Disposition");
        response.setHeader("Content-Disposition",
                "attachment;filename="+ URLEncoder
                        .encode(scores.get(0).getExam().getExamName()+".xls", "utf-8"));
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(exportExcel);
        outputStream.flush();
        outputStream.close();
    }
}
