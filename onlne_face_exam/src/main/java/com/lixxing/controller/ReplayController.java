package com.lixxing.controller;

import com.lixxing.model.ApiResult;
import com.lixxing.model.Message;
import com.lixxing.model.Replay;
import com.lixxing.service.MessageService;
import com.lixxing.service.ReplayService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "回复管理")
@RestController
public class ReplayController {

    @Autowired
    private ReplayService replayService;
    @Autowired
    private MessageService messageService;

    @ApiOperation("添加回复")
    @ApiImplicitParam(name = "replay",value = "回复实体数据",required = true,dataType = "Replay")
    @PostMapping("/replay")
    public ApiResult add(@RequestBody Replay replay) {
        Integer messageId = replay.getMessageId();
        Message message = messageService.findById(messageId);
        List<Replay> replays = message.getReplays();
        replays.add(replay);
        message.setReplays(replays);
        int update = messageService.update(message);
        int data = replayService.add(replay);
        if (data != 0 && update != 0) {
            return ApiResultHandler.buildApiResult(200,"添加成功！",data);
        } else {
            return ApiResultHandler.buildApiResult(400,"添加失败！",null);
        }
    }

    @GetMapping("/replay/{messageId}")
    @ApiOperation("回去指定消息的回复")
    @ApiImplicitParam(name = "messageId",value = "消息Id",required = true,dataType = "Integer")
    public ApiResult findAllById(@PathVariable("messageId") Integer messageId) {
        List<Replay> res = replayService.findAllById(messageId);
        return ApiResultHandler.buildApiResult(200,"根据messageId查询",res);
    }
}
