package com.lixxing.controller;

import com.lixxing.model.ApiResult;
import com.lixxing.model.Message;
import com.lixxing.service.MessageService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "消息管理")
@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;

    @ApiOperation("分页查询所有消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/messages/{page}/{size}")
    public ApiResult<Message> findAll(@PathVariable("page") Integer page, 
                                      @PathVariable("size") Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Message> all = messageService.findAll(pageable);
        return ApiResultHandler.buildApiResult(200,"查询所有消息",all);
    }

    @ApiOperation("分页查询所有消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type",value = "消息类型",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/messages/{type}/{page}/{size}")
    public ApiResult<Message> findAll(@PathVariable("type") Integer type,
                                      @PathVariable("page") Integer page, 
                                      @PathVariable("size") Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Message> all = messageService.findAll(pageable,type);
        return ApiResultHandler.buildApiResult(200,"查询所有消息",all);
    }

    @ApiOperation("分页查询所有消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type",value = "消息类型",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/messages/{type}")
    public ApiResult<Message> findAll(@PathVariable("type") Integer type){
        List<Message> all = messageService.findAll(type);
        return ApiResultHandler.buildApiResult(200,"查询所有消息",all);
    }

    @GetMapping("/message/{id}")
    public ApiResult findById(@PathVariable("id") Integer id) {
        Message res = messageService.findById(id);
        return ApiResultHandler.buildApiResult(200,"根据Id查询",res);
    }

    @DeleteMapping("/message/{id}")
    public int delete(@PathVariable("id") Integer id) {
        int res = messageService.delete(id);
        return res;
    }

    @PutMapping("/message")
    public ApiResult update(@RequestBody Message message) {
        int res = messageService.update(message);
        if (res == 0) {
            return ApiResultHandler.buildApiResult(400,"更新失败",res);
        } else {
            return ApiResultHandler.buildApiResult(200,"更新成功",res);
        }
    }

    @PostMapping("/message")
    public ApiResult add(@RequestBody Message message) {
        System.out.println(message);
        int res = messageService.add(message);
        if (res == 0) {
            return ApiResultHandler.buildApiResult(400,"添加失败",res);
        } else {
            return ApiResultHandler.buildApiResult(200,"添加成功",res);
        }
    }
}
