package com.lixxing.controller;

import com.lixxing.model.ApiResult;
import com.lixxing.model.Profession;
import com.lixxing.service.ProfessionService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Api(tags = "专业管理")
@RestController
public class ProfessionController {
    @Autowired
    private ProfessionService professionService;
    @ApiOperation("分页获取全部专业")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",required = true,dataType = "Integer/PathVariable"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",required = true,dataType = "Integer/PathVariable")
    })
    @GetMapping("/professions/{page}/{size}")
    public ApiResult findAll(@PathVariable("page")Integer page,
                             @PathVariable("size")Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        return ApiResultHandler.success(professionService.findAll(pageable));
    }
    @ApiOperation("获取全部专业")
    @GetMapping("/professions")
    public ApiResult findAll(){
        return ApiResultHandler.success(professionService.findAll());
    }
    @ApiOperation("获取指定专业Id的专业信息")
    @ApiImplicitParam(name = "professionId",value = "专业Id",required = true,dataType = "Integer/PathVariable")
    @GetMapping("/profession/{professionId}")
    public ApiResult findById(@PathVariable("professionId") Integer professionId){
//        System.out.println("根据ID查找");
        return ApiResultHandler.success(professionService.findById(professionId));
    }
    @ApiOperation("删除指定专业Id的专业信息")
    @ApiImplicitParam(name = "professionId",value = "专业Id",required = true,dataType = "Integer/PathVariable")
    @DeleteMapping("/profession/{professionId}")
    public ApiResult deleteById(@PathVariable("professionId") Integer professionId){
        professionService.delete(professionId);
        return ApiResultHandler.success();
    }
    @ApiOperation("修改指定专业Id的专业信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "professionId",value = "专业Id",required = true,dataType = "Integer/PathVariable"),
            @ApiImplicitParam(name = "profession",value = "专业实体数据",required = true,dataType = "Profession")
    })
    @PutMapping("/profession")
    public ApiResult update(@RequestBody Profession profession){
        return ApiResultHandler.success(professionService.update(profession));
    }

    @ApiOperation("新增专业")
    @ApiImplicitParam(name = "profession", value = "Profession实体数据" ,required = true, dataType = "Profession")
    @PostMapping("/profession")
    public ApiResult add(@RequestBody Profession profession){
        System.out.println(profession);
        return ApiResultHandler.success(professionService.add(profession));
    }

}
