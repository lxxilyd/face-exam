package com.lixxing.controller;

import com.lixxing.model.ApiResult;
import com.lixxing.model.Exam;
import com.lixxing.model.Student;
import com.lixxing.model.StudentExamStatus;
import com.lixxing.service.StudentExamStatusService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "状态管理")
@RestController
public class StatusController {
    @Autowired
    private StudentExamStatusService statusService;

    @ApiOperation("查询所有的状态")
    @GetMapping("/status")
    public ApiResult findAll() {
        List<StudentExamStatus> res = statusService.findAll();
        return ApiResultHandler.buildApiResult(200,"查询所有学生状态",res);
    }

    @ApiOperation("分页查询所有的状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer")
    })
    @GetMapping("/status/{page}/{size}")
    public ApiResult findAll(@PathVariable("page") Integer page,
                             @PathVariable("size") Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<StudentExamStatus> res = statusService.findAll(pageable);
        return ApiResultHandler.buildApiResult(200, "根据ID查询状态", res);
    }

    @ApiOperation("查询指定学生的考试状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentId",value = "学生学号",required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "examCode",value = "考试编号",required = true,dataType = "Integer")
    })
    @GetMapping("/getStatus/{studentId}/{examCode}")
    public ApiResult findById(@PathVariable("studentId") Integer studentId,
                              @PathVariable("examCode") Integer examCode) {
        StudentExamStatus status = statusService.findByStudentAndExam(new Student(studentId), new Exam(examCode));
        if (null != status) {
            return ApiResultHandler.buildApiResult(200, "根据ID查询状态", status);
        } else {
            return ApiResultHandler.buildApiResult(204, "ID不存在", status);
        }
    }

    @ApiOperation("添加状态")
    @ApiImplicitParam(name = "status",value = "状态实体",required = true,dataType = "StudentExamStatus")
    @PostMapping("/status")
    public ApiResult add(@RequestBody StudentExamStatus status) {
        int res = statusService.add(status);
        if (res == 0) {
            return ApiResultHandler.buildApiResult(400,"状态添加失败",res);
        }else {
            return ApiResultHandler.buildApiResult(200,"状态添加成功",res);
        }
    }

    @ApiOperation("修改状态")
    @ApiImplicitParam(name = "status",value = "状态实体",required = true,dataType = "StudentExamStatus")
    @PutMapping("/status")
    public ApiResult update(@RequestBody StudentExamStatus status) {
        int res = statusService.add(status);
        if (res == 0) {
            return ApiResultHandler.buildApiResult(400,"状态修改失败",res);
        }else {
            return ApiResultHandler.buildApiResult(200,"状态修改成功",res);
        }
    }

}
