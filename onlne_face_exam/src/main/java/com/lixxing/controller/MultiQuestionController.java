package com.lixxing.controller;

import com.lixxing.model.ApiResult;
import com.lixxing.model.MultiQuestion;
import com.lixxing.service.MultiQuestionService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "选择题管理")
@RestController
public class MultiQuestionController {

    @Autowired
    private MultiQuestionService multiQuestionService;

    @ApiOperation("查询最后一条记录id")
    @GetMapping("/multiQuestionId")
    public ApiResult findOnlyQuestion() {
        MultiQuestion res = multiQuestionService.findOnlyQuestionId();
        return ApiResultHandler.buildApiResult(200,"查询成功",res);
    }

    @ApiOperation("查询指定Id的选择题信息")
    @ApiImplicitParam(name = "questionId",value = "选择题Id",required = true,dataType = "Integer")
    @GetMapping("/multiQuestion/{questionId}")
    public ApiResult findByQuestionId(@PathVariable("questionId")Integer questionId) {
        MultiQuestion res = multiQuestionService.findByQuestionId(questionId);
        return ApiResultHandler.buildApiResult(200,"查询成功",res);
    }

    @ApiOperation("添加选择题")
    @ApiImplicitParam(name = "multiQuestion",value = "选择题实体数据",required = true,dataType = "MultiQuestion")
    @PostMapping("/multiQuestion")
    public ApiResult add(@RequestBody MultiQuestion multiQuestion) {
        int res = multiQuestionService.add(multiQuestion);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"添加成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"添加失败",res);
    }
    @ApiOperation("修改选择题")
    @ApiImplicitParam(name = "multiQuestion",value = "选择题实体数据",required = true,dataType = "MultiQuestion")
    @PutMapping("/multiQuestion")
    public ApiResult update(@RequestBody MultiQuestion multiQuestion) {
        int res = multiQuestionService.update(multiQuestion);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"更新成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"更新失败",res);
    }
    @ApiOperation("删除指定Id的选择题信息")
    @ApiImplicitParam(name = "questionId",value = "选择题Id",required = true,dataType = "Integer")
    @DeleteMapping("/multiQuestion/{questionId}")
    public ApiResult delete(@PathVariable("questionId") Integer questionId) {
        System.out.println(questionId);
        int res = multiQuestionService.delete(questionId);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"删除成功",res);
        }
        return ApiResultHandler.buildApiResult(400,"删除失败",res);
    }

    @ApiOperation("分页获取选择题信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/multiQuestion/{page}/{size}")
    public ApiResult findMulitQuestion(@PathVariable Integer page, @PathVariable Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<MultiQuestion> res = multiQuestionService.findAll(pageable);
        return ApiResultHandler.buildApiResult(200,"查询成功",res);
    }

    @ApiOperation("分页获取指定课程选择题信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId",value = "课程Id",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "page",value = "第几页",
                    required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",
                    required = true,dataType = "Integer"),
    })
    @GetMapping("/multiQuestion/{courseId}/{page}/{size}")
    public ApiResult findMulitQuestion(@PathVariable Integer courseId,
                                       @PathVariable Integer page,
                                       @PathVariable Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        Page<MultiQuestion> res = multiQuestionService.findAll(pageable,courseId);
        return ApiResultHandler.buildApiResult(200,"查询成功",res);
    }

    @ApiOperation("获取全部选择题信息")
    @GetMapping("/multiQuestions")
    public ApiResult findMulitQuestion() {
        List<MultiQuestion> res = multiQuestionService.findAll();
        return ApiResultHandler.buildApiResult(200,"查询成功",res);
    }
}
