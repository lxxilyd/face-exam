package com.lixxing.controller;

import com.lixxing.model.ApiResult;
import com.lixxing.model.Teacher;
import com.lixxing.service.TeacherService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Api(tags = "教师管理")
@RestController
public class TeacherController {
    @Autowired
    private TeacherService teacherService;
    @ApiOperation("分页查询所有教师")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",required = true,dataType = "Integer")
    })
    @GetMapping("/teachers/{page}/{size}")
    public ApiResult findAll(@PathVariable Integer page, @PathVariable Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Teacher> teacherPage = teacherService.findAll(pageable);
        return ApiResultHandler.buildApiResult(200,"查询所有教师",teacherPage);
    }
    @ApiOperation("获取指定教师Id的教师信息")
    @ApiImplicitParam(name = "teacherId",value = "教师Id",required = true,dataType = "Integer")
    @GetMapping("/teacher/{teacherId}")
    public ApiResult findById(@PathVariable("teacherId") Integer teacherId){
        return ApiResultHandler.success(teacherService.findById(teacherId));
    }
    @ApiOperation("删除指定教师Id的教师信息")
    @ApiImplicitParam(name = "teacherId",value = "教师Id",required = true,dataType = "Integer")
    @DeleteMapping("/teacher/{teacherId}")
    public ApiResult deleteById(@PathVariable("teacherId") Integer teacherId){
        return ApiResultHandler.success(teacherService.deleteById(teacherId));
    }
    @ApiOperation("修改教师密码")
    @ApiImplicitParam(name = "teacher",value = "教师实体数据",required = true,dataType = "Teacher")
    @PutMapping("/teacherPWD")
    public ApiResult updatePwd(@RequestBody Teacher teacher) {
        int res = teacherService.updatePwd(teacher);
        if (res != 0) {
            return ApiResultHandler.buildApiResult(200,"密码更新成功",null);
        }
        return ApiResultHandler.buildApiResult(200,"密码更新失败",null);
    }
    @ApiOperation("修改教师信息")
    @ApiImplicitParam(name = "teacher",value = "教师实体数据",required = true,dataType = "Teacher")
    @PutMapping("/teacher")
    public ApiResult update(@RequestBody Teacher teacher){
        return ApiResultHandler.success(teacherService.update(teacher));
    }
    @ApiOperation("添加教师信息")
    @ApiImplicitParam(name = "teacher",value = "教师实体数据",required = true,dataType = "Teacher")
    @PostMapping("/teacher")
    public ApiResult add(@RequestBody Teacher teacher){
        System.out.println(teacher);
        return ApiResultHandler.success(teacherService.add(teacher));
    }
}
