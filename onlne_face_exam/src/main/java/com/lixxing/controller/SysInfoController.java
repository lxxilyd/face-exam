package com.lixxing.controller;

import com.lixxing.model.Admin;
import com.lixxing.model.ApiResult;
import com.lixxing.service.AdminService;
import com.lixxing.utils.ApiResultHandler;
import com.lixxing.utils.SysInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "获取系统信息")
@RestController
public class SysInfoController {

    private SysInfo sysInfo = new SysInfo();

    @ApiOperation("获取系统信息")
    @GetMapping("/sysInfo")
    public ApiResult getSysInfo(){
        return ApiResultHandler.success(sysInfo.getSystemInfo());
    }
}
