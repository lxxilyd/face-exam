package com.lixxing.controller;

import com.lixxing.model.Admin;
import com.lixxing.model.ApiResult;
import com.lixxing.service.AdminService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "管理员管理")
@RestController
public class AdminController {

    @Autowired
    private AdminService adminService;

    @GetMapping("/admins")
    @ApiOperation("查询全部")
    public ApiResult findAll(){
        return ApiResultHandler.success(adminService.findAll());
    }

    @GetMapping("/admin/{adminId}")
    @ApiOperation("查询指定Id的管理员")
    @ApiImplicitParam(name = "adminId",value = "管理员Id",required = true,dataType = "Integer")
    public ApiResult findById(@PathVariable("adminId") Integer adminId){
        System.out.println("根据ID查找");
        return ApiResultHandler.success(adminService.findById(adminId));
    }

    @DeleteMapping("/admin/{adminId}")
    @ApiOperation("删除指定Id的管理员")
    @ApiImplicitParam(name = "adminId",value = "管理员Id",required = true,dataType = "Integer")
    public ApiResult deleteById(@PathVariable("adminId") Integer adminId){
        adminService.deleteById(adminId);
        return ApiResultHandler.success();
    }

    @PutMapping("/admin")
    @ApiOperation("修改管理员")
    @ApiImplicitParam(name = "adminId",value = "管理员实体数据",required = true,dataType = "Admin")
    public ApiResult update(@RequestBody Admin admin){
        return ApiResultHandler.success(adminService.update(admin));
    }

    @PostMapping("/admin")
    @ApiOperation("添加管理员")
    @ApiImplicitParam(name = "adminId",value = "管理员实体数据",required = true,dataType = "Admin")
    public ApiResult add(@RequestBody Admin admin){
        return ApiResultHandler.success(adminService.add(admin));
    }
}
