package com.lixxing.controller;

import cn.hutool.core.date.DateTime;
import com.lixxing.model.ApiResult;
import com.lixxing.model.Clazz;
import com.lixxing.model.Exam;
import com.lixxing.service.ExamService;
import com.lixxing.utils.ApiResultHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Api(tags = "考试管理")
@RestController
public class ExamController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ExamService examService;

    @ApiOperation("查询所有考试")
    @GetMapping("/exams")
    public ApiResult findAll(){
        System.out.println("不分页查询所有试卷");
        ApiResult apiResult;
        apiResult = ApiResultHandler.buildApiResult(200, "请求成功！", examService.findAll());
        return apiResult;
    }

    @ApiOperation("分页查询所有考试")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",required = true,dataType = "Integer/PathVariable"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",required = true,dataType = "Integer/PathVariable")
    })
    @GetMapping("/exams/{page}/{size}")
    public ApiResult findAll(@PathVariable("page") Integer page, @PathVariable("size") Integer size){
        logger.info("分页查询所有考试");
        ApiResult apiResult;
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Exam> all = examService.findAll(pageable);
        apiResult = ApiResultHandler.buildApiResult(200, "请求成功！", all);
        return apiResult;
    }

    @ApiOperation("分页查询指定班级的考试")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",required = true,dataType = "Integer/PathVariable"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",required = true,dataType = "Integer/PathVariable"),
            @ApiImplicitParam(name = "clazzs",value = "班级列表",required = true,dataType = "Clazz")
    })
    @PostMapping("/examsClazz/{page}/{size}")
    public ApiResult findAll(@PathVariable("page") Integer page,
                             @PathVariable("size") Integer size,
                             @RequestBody Clazz clazz){
        ApiResult apiResult;
        Set<Clazz> clazzs = new HashSet<>();
        clazzs.add(clazz);
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Exam> all = examService.findAllByClazzs(pageable,clazzs);
        apiResult = ApiResultHandler.buildApiResult(200, "请求成功！", all);
        return apiResult;
    }

    @ApiOperation("分页查询指定班级的指定类型考试")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",required = true,dataType = "Integer/PathVariable"),
            @ApiImplicitParam(name = "size",value = "每页多少条数据",required = true,dataType = "Integer/PathVariable"),
            @ApiImplicitParam(name = "clazzs",value = "班级列表",required = true,dataType = "Clazz"),
            @ApiImplicitParam(name = "type",value = "类型",required = true,dataType = "Integer")
    })
    @PostMapping("/examsClazz/{type}/{page}/{size}")
    public ApiResult findAll(@PathVariable("page") Integer page,
                             @PathVariable("size") Integer size,
                             @PathVariable("type") Integer type,
                             @RequestBody Clazz clazz){
        ApiResult apiResult;
        Set<Clazz> clazzs = new HashSet<>();
        clazzs.add(clazz);
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Exam> all = examService.findAllByTypeAndClazzs(pageable,clazzs,type);
        apiResult = ApiResultHandler.buildApiResult(200, "请求成功！", all);
        return apiResult;
    }

    @ApiOperation("查询指定班级的考试")
    @ApiImplicitParam(name = "clazz",value = "班级实体",required = true,dataType = "Clazz")
    @PostMapping("/examsClazz")
    public ApiResult findAll(@RequestBody Clazz clazz){
        ApiResult apiResult;
        Set<Clazz> clazzs = new HashSet<>();
        clazzs.add(clazz);
        List<Exam> all = examService.findAllByClazzs(clazzs);
        apiResult = ApiResultHandler.buildApiResult(200, "请求成功！", all);
        return apiResult;
    }

    @ApiOperation("查询指定考试编号的考试")
    @ApiImplicitParam(name = "examCode",value = "考试编号",required = true,dataType = "Integer")
    @GetMapping("/exam/{examCode}")
    public ApiResult findById(@PathVariable("examCode") Integer examCode){
        Exam res = examService.findById(examCode);
        if(res == null) {
            return ApiResultHandler.buildApiResult(10000,"考试编号不存在",null);
        }
        return ApiResultHandler.buildApiResult(200,"请求成功！",res);
    }

    @ApiOperation("查询指定考试类型与编号的考试")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type",value = "考试类型",required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "examCode",value = "考试编号",required = true,dataType = "Integer")
    })
    @GetMapping("/exam/{type}/{examCode}")
    public ApiResult findByTypeAndId(@PathVariable("type") Integer type,
                                     @PathVariable("examCode") Integer examCode){
        Exam res = examService.findByTypeAndId(type,examCode);
        if(res == null) {
            return ApiResultHandler.buildApiResult(10000,"考试编号不存在",null);
        }
        return ApiResultHandler.buildApiResult(200,"请求成功！",res);
    }

    @ApiOperation("获取系统时间")
    @GetMapping("/systemDate")
    public ApiResult getSystemDate(){
        long timeMillis = System.currentTimeMillis();
        return ApiResultHandler.buildApiResult(200,"请求成功！",timeMillis);
    }

    @ApiOperation("删除指定考试编号的考试")
    @ApiImplicitParam(name = "examCode",value = "考试编号",required = true,dataType = "Integer")
    @DeleteMapping("/exam/{examCode}")
    public ApiResult deleteById(@PathVariable("examCode") Integer examCode){
        int res = examService.delete(examCode);
        return ApiResultHandler.buildApiResult(200,"删除成功",res);
    }

    @ApiOperation("修改考试")
    @ApiImplicitParam(name = "exam",value = "考试实体数据",required = true,dataType = "Exam")
    @PutMapping("/exam")
    public ApiResult update(@RequestBody Exam exam){
        System.out.println(exam);
        int res = examService.update(exam);
        if (res == 0) {
            return ApiResultHandler.buildApiResult(400,"请求参数错误",null);
        }
        return ApiResultHandler.buildApiResult(200,"更新成功",res);
    }

    @ApiOperation("添加考试")
    @ApiImplicitParam(name = "exam",value = "考试实体数据",required = true,dataType = "Exam")
    @PostMapping("/exam")
    public ApiResult add(@RequestBody Exam exam){
        System.out.println(exam);
        int res = examService.add(exam);
        if (res ==1) {
            return ApiResultHandler.buildApiResult(200, "添加成功", res);
        } else {
            return  ApiResultHandler.buildApiResult(400,"添加失败",res);
        }
    }
}
