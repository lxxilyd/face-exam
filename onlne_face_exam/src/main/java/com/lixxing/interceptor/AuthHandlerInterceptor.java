package com.lixxing.interceptor;

import cn.hutool.core.util.StrUtil;
import com.lixxing.exception.RolesException;
import com.lixxing.exception.TokenAuthExpiredException;
import com.lixxing.model.Admin;
import com.lixxing.service.AdminService;
import com.lixxing.utils.TokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component
public class AuthHandlerInterceptor implements HandlerInterceptor {

    @Autowired
    private TokenUtil tokenUtil;
    @Value("${token.privateKey}")
    private String privateKey;

    private Logger logger = LoggerFactory.getLogger(AuthHandlerInterceptor.class);

//    @Autowired
//    private AdminService adminService;

    /**
     * 权限认证的拦截操作.
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
        // 如果不是映射到方法直接通过,可以访问资源.
        if (!(object instanceof HandlerMethod)) {
            return true;
        }
        //为空就返回错误
        String token = request.getHeader("authorization");
        if (null == token || "".equals(token.trim())) {
            throw new TokenAuthExpiredException();
        }
        Map<String, String> map = tokenUtil.parseToken(token);
        String userId = map.get("userId");
        String userRole = map.get("userRole");
        long expriedTime = Long.parseLong(map.get("expriedTime"));
        long timeMillis = System.currentTimeMillis() /1000L;
        long timeOfUse = expriedTime - timeMillis;
//        logger.info("timeOfUse: "+timeOfUse);
        response.setHeader("Access-Control-Expose-Headers","Authorization");
        if (timeOfUse > 0 && timeOfUse < 600) {//老年 token 就刷新 token
            response.setHeader("Authorization", tokenUtil.getToken(userId, userRole));
        } else if (timeOfUse > 600){

        }
        else {
            throw new TokenAuthExpiredException();
        }
        //2.角色匹配.
//        if ("user".equals(userRole)) {
//            log.debug("========user账户============");
//            return true;
//        }
//        if ("admin".equals(userRole)) {
//            log.debug("========admin账户============");
//            return true;
//        }
//        return false;
        if(StrUtil.isEmpty(userRole)){
            throw new RolesException();
        }
//        currentAdmin=adminService.getAdminById(new Integer(userId));
//        log.info(currentAdmin.toString());
        return true;
    }
}