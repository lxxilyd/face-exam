package com.lixxing;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@EnableSwaggerBootstrapUI
@EnableCaching
@SpringBootApplication
//@EnableAdminServer
public class OnlneFaceExamApplication {

    public static void main(String[] args)  {
        SpringApplication.run(OnlneFaceExamApplication.class, args);
    }
}
