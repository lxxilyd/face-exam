package com.lixxing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Set;

/**
 * 考试实体类
 */
@Entity
@ApiModel
public class Exam extends BaseModel implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty("考试编号")
    private Integer examCode;
    @ApiModelProperty("考试名称")
    private String examName;
    @ApiModelProperty("考试简介")
    private String introduction;
    @ApiModelProperty("考试类型")
    private Integer type;
    @ApiModelProperty("考试状态")
    private Integer status;
    @ApiModelProperty("考生提示")
    private String tip;
    @ApiModelProperty("考试开始时间")
    private Timestamp examStart;
    @ApiModelProperty("考试时长")
    private Integer examDuration;
    // 一对一关系
    @ApiModelProperty("试卷实体")
    @OneToOne(fetch = FetchType.EAGER)
    private Paper paper;
    // 多对多关系
    @ManyToMany(fetch = FetchType.EAGER)
    @ApiModelProperty("考试班级")
    private Set<Clazz> clazzs;

    @ApiModelProperty("考试课程")
    @OneToOne(fetch = FetchType.EAGER)
    private Course course;

    public Exam() {
    }

    public Exam(Integer examCode) {
        this.examCode = examCode;
    }

    public Exam(String examName, String introduction, Integer type, Integer status,
                String tip, Timestamp examStart, Integer examDuration, Paper paper,
                Set<Clazz> clazzs, Course course) {
        this.examName = examName;
        this.introduction = introduction;
        this.type = type;
        this.status = status;
        this.tip = tip;
        this.examStart = examStart;
        this.examDuration = examDuration;
        this.paper = paper;
        this.clazzs = clazzs;
        this.course = course;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getExamCode() {
        return examCode;
    }

    public void setExamCode(Integer examCode) {
        this.examCode = examCode;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Timestamp getExamStart() {
        return examStart;
    }

    public void setExamStart(Timestamp examStart) {
        this.examStart = examStart;
    }

    public Integer getExamDuration() {
        return examDuration;
    }

    public void setExamDuration(Integer examDuration) {
        this.examDuration = examDuration;
    }

    public Paper getPaper() {
        return paper;
    }

    public void setPaper(Paper paper) {
        this.paper = paper;
    }

    public Set<Clazz> getClazzs() {
        return clazzs;
    }

    public void setClazzs(Set<Clazz> clazzs) {
        this.clazzs = clazzs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exam exam = (Exam) o;
        return Objects.equals(examCode, exam.examCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(examCode);
    }

    @Override
    public String toString() {
        return "Exam{" +
                "examCode=" + examCode +
                ", examName='" + examName + '\'' +
                ", introduction='" + introduction + '\'' +
                ", type=" + type +
                ", status=" + status +
                ", tip='" + tip + '\'' +
                ", examStart=" + examStart +
                ", examDuration=" + examDuration +
                ", paper=" + paper +
                ", clazzs=" + clazzs +
                ", course=" + course +
                '}';
    }
}
