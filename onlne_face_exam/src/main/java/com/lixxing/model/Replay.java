package com.lixxing.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@ApiModel
@Entity
public class Replay extends BaseModel implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;
    @ApiModelProperty("回复对应的消息Id")
    private Integer messageId;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty("回复Id")
    private Integer replayId;
    @ApiModelProperty("回复")
    private String replay;
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @ApiModelProperty("回复时间")
    private Date replayTime;
    @OneToOne(fetch = FetchType.EAGER)
    @ApiModelProperty("回复学生")
    Student studentResponder;
    @ApiModelProperty("回复教师")
    @OneToOne(fetch = FetchType.EAGER)
    Teacher teacherResponder;

    public Replay() {
    }

    public Replay(Integer messageId, Integer replayId, String replay, Date replayTime) {
        this.messageId = messageId;
        this.replayId = replayId;
        this.replay = replay;
        this.replayTime = replayTime;
    }

    public Student getStudentResponder() {
        return studentResponder;
    }

    public void setStudentResponder(Student studentResponder) {
        this.studentResponder = studentResponder;
    }

    public Teacher getTeacherResponder() {
        return teacherResponder;
    }

    public void setTeacherResponder(Teacher teacherResponder) {
        this.teacherResponder = teacherResponder;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public Integer getReplayId() {
        return replayId;
    }

    public void setReplayId(Integer replayId) {
        this.replayId = replayId;
    }

    public String getReplay() {
        return replay;
    }

    public void setReplay(String replay) {
        this.replay = replay;
    }

    public Date getReplayTime() {
        return replayTime;
    }

    public void setReplayTime(Date replayTime) {
        this.replayTime = replayTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Replay replay = (Replay) o;
        return Objects.equals(replayId, replay.replayId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(replayId);
    }

    @Override
    public String toString() {
        return "Replay{" +
                "messageId=" + messageId +
                ", replayId=" + replayId +
                ", replay='" + replay + '\'' +
                ", replayTime=" + replayTime +
                ", studentResponder=" + studentResponder +
                ", teacherResponder=" + teacherResponder +
                '}';
    }
}