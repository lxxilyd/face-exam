package com.lixxing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class Reseter {
    @ApiModelProperty("用户名")
    private Integer username;
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty("图形验证码")
    private String verifyCode;
    @ApiModelProperty("图形验证码Id")
    private String verifyCodeId;
    @ApiModelProperty("邮箱验证码")
    private String emailVerifyCode;
    @ApiModelProperty("邮箱验证码Id")
    private String emailVerifyCodeId;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("确认密码")
    private String confirmPassword;
    @ApiModelProperty("角色名")
    private String region;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailVerifyCode() {
        return emailVerifyCode;
    }

    public void setEmailVerifyCode(String emailVerifyCode) {
        this.emailVerifyCode = emailVerifyCode;
    }

    public String getEmailVerifyCodeId() {
        return emailVerifyCodeId;
    }

    public void setEmailVerifyCodeId(String emailVerifyCodeId) {
        this.emailVerifyCodeId = emailVerifyCodeId;
    }

    public String getVerifyCodeId() {
        return verifyCodeId;
    }

    public void setVerifyCodeId(String verifyCodeId) {
        this.verifyCodeId = verifyCodeId;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public Integer getUsername() {
        return username;
    }

    public void setUsername(Integer username) {
        this.username = username;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return "Reseter{" +
                "username=" + username +
                ", email='" + email + '\'' +
                ", verifyCode='" + verifyCode + '\'' +
                ", verifyCodeId='" + verifyCodeId + '\'' +
                ", emailVerifyCode='" + emailVerifyCode + '\'' +
                ", emailVerifyCodeId='" + emailVerifyCodeId + '\'' +
                ", password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                ", region='" + region + '\'' +
                '}';
    }
}
