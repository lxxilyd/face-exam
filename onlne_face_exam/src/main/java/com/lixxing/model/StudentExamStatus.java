package com.lixxing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 学生考试状态，记录学生是否已作答并提交
 */
@ApiModel
@Entity
public class StudentExamStatus extends BaseModel implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;

    @ApiModelProperty("状态Id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private StatusId statusId;

    @ApiModelProperty("学生")
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(insertable = false,updatable = false)
    private Student student;

    @ApiModelProperty("考试")
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(insertable = false,updatable = false)
    private Exam exam;

    @ApiModelProperty("状态码")
    Integer statusCode;

    public StudentExamStatus() {
    }

    public StudentExamStatus(StatusId statusId) {
        this.statusId = statusId;
    }

    public StudentExamStatus(Student student, Exam exam, Integer statusCode) {
        this.student = student;
        this.exam = exam;
        this.statusCode = statusCode;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public StatusId getStatusId() {
        return statusId;
    }

    public void setStatusId(StatusId statusId) {
        this.statusId = statusId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "StudentExamStstus{" +
                "statusId=" + statusId +
                ", student=" + student +
                ", exam=" + exam +
                ", statusCode=" + statusCode +
                '}';
    }
}
