package com.lixxing.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@ApiModel
public class Message extends BaseModel implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty("消息Id")
    private Integer messageId;
    @ApiModelProperty("消息临时Id")
    private Integer temp_id;//解决id为null创建的一个临时id
    @ApiModelProperty("公告消息发布者")
    @OneToOne(fetch=FetchType.EAGER)
    private Admin adminAnnouncer;
    @ApiModelProperty("讨论消息发布者")
    @OneToOne(fetch=FetchType.EAGER)
    private Student studentAnnouncer;
    @ApiModelProperty("消息标题")
    private String title;
    @ApiModelProperty("消息类型")
    private Integer type;
    @ApiModelProperty("消息正文")
    private String content;
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @ApiModelProperty("发布日期")
    private Date time;
    @ApiModelProperty("评论信息")
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    List<Replay> replays;   //一对多关系，评论信息

    public Message() {
    }

    public Message(Integer messageId, Integer temp_id, String title, String content, Date time, List<Replay> replays) {
        this.messageId = messageId;
        this.temp_id = temp_id;
        this.title = title;
        this.content = content;
        this.time = time;
        this.replays = replays;
    }

    public Admin getAdminAnnouncer() {
        return adminAnnouncer;
    }

    public void setAdminAnnouncer(Admin adminAnnouncer) {
        this.adminAnnouncer = adminAnnouncer;
    }

    public Student getStudentAnnouncer() {
        return studentAnnouncer;
    }

    public void setStudentAnnouncer(Student studentAnnouncer) {
        this.studentAnnouncer = studentAnnouncer;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer id) {
        this.messageId = id;
    }

    public Integer getTemp_id() {
        return temp_id;
    }

    public void setTemp_id(Integer temp_id) {
        this.temp_id = temp_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public List<Replay> getReplays() {
        return replays;
    }

    public void setReplays(List<Replay> replays) {
        this.replays = replays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(messageId, message.messageId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageId);
    }

    @Override
    public String toString() {
        return "Message{" +
                "messageId=" + messageId +
                ", temp_id=" + temp_id +
                ", adminAnnouncer=" + adminAnnouncer +
                ", studentAnnouncer=" + studentAnnouncer +
                ", title='" + title + '\'' +
                ", type=" + type +
                ", content='" + content + '\'' +
                ", time=" + time +
                ", replays=" + replays +
                '}';
    }
}