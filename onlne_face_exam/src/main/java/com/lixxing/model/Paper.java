package com.lixxing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * 试卷实体
 */
@ApiModel
@Entity
public class Paper extends BaseModel implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty("试卷编号")
    private Integer paperId;
    @ApiModelProperty("试卷名称")
    private String paperName;

    @ApiModelProperty("所属课程")
    @ManyToOne(fetch = FetchType.EAGER)
    private Course course;

    // 多对多关系
    @ApiModelProperty("选择题列表")
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<MultiQuestion> multiQuestions;
    // 多对多关系
    @ApiModelProperty("判断题列表")
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<JudgeQuestion> judgeQuestions;
    // 多对多关系
    @ApiModelProperty("填空题列表")
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<FillQuestion> fillQuestions;
    @ApiModelProperty("试卷题目数")
    private Integer questionNum;
    @ApiModelProperty("试卷总分")
    private Integer totilScore;

    public Paper() {
    }

    public Paper(Integer paperId) {
        this.paperId = paperId;
    }

    public Paper(Integer paperId, String paperName, Set<MultiQuestion> multiQuestions, Set<JudgeQuestion> judgeQuestions, Set<FillQuestion> fillQuestions) {
        this.paperId = paperId;
        this.paperName = paperName;
        this.multiQuestions = multiQuestions;
        this.judgeQuestions = judgeQuestions;
        this.fillQuestions = fillQuestions;
    }

    public Integer getQuestionNum() {
        return questionNum;
    }

    public void setQuestionNum(Integer questionNum) {
        this.questionNum = questionNum;
    }

    public Integer getTotilScore() {
        return totilScore;
    }

    public void setTotilScore(Integer totilScore) {
        this.totilScore = totilScore;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getPaperId() {
        return paperId;
    }

    public void setPaperId(Integer paperId) {
        this.paperId = paperId;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public Set<MultiQuestion> getMultiQuestions() {
        return multiQuestions;
    }

    public void setMultiQuestions(Set<MultiQuestion> multiQuestions) {
        this.multiQuestions = multiQuestions;
    }

    public Set<JudgeQuestion> getJudgeQuestions() {
        return judgeQuestions;
    }

    public void setJudgeQuestions(Set<JudgeQuestion> judgeQuestions) {
        this.judgeQuestions = judgeQuestions;
    }

    public Set<FillQuestion> getFillQuestions() {
        return fillQuestions;
    }

    public void setFillQuestions(Set<FillQuestion> fillQuestions) {
        this.fillQuestions = fillQuestions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Paper paper = (Paper) o;
        return Objects.equals(paperId, paper.paperId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(paperId);
    }

    @Override
    public String toString() {
        return "Paper{" +
                "paperId=" + paperId +
                ", paperName='" + paperName + '\'' +
                ", multiQuestions=" + multiQuestions +
                ", judgeQuestions=" + judgeQuestions +
                ", fillQuestions=" + fillQuestions +
                ", questionNum=" + questionNum +
                ", totilScore=" + totilScore +
                '}';
    }
}
