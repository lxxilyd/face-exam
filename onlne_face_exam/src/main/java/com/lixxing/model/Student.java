package com.lixxing.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * 学生实体类
 */
@ApiModel
@Entity
public class Student extends BaseModel implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;
    @Id
    @ApiModelProperty("学生Id(学号)")
    private Integer studentId;
    @ApiModelProperty("面部信息Id")
    private String FaceId;
    @ApiModelProperty("学生姓名")
    private String studentName;
    @ApiModelProperty("电话号码")
    private String tel;
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty("密码")
    private String pwd;
    @ApiModelProperty("身份证号码")
    private String cardId;
    @ApiModelProperty("性别")
    private String sex;
    @ApiModelProperty("角色名称")
    private String role;
    // transient排出序列化
    @ApiModelProperty("验证Token")
    private transient String token;
    @ApiModelProperty("班级")
    @ManyToOne(fetch = FetchType.EAGER)
    private Clazz clazz;
    @ApiModelProperty("所学课程")
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Course> courses;

    public Student() {
    }

    public Student(Integer studentId) {
        this.studentId = studentId;
    }

    public Student(Integer studentId, String studentName, Clazz clazz, String tel, String email, String pwd, String cardId, String sex, String role) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.clazz = clazz;
        this.tel = tel;
        this.email = email;
        this.pwd = pwd;
        this.cardId = cardId;
        this.sex = sex;
        this.role = role;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getFaceId() {
        return FaceId;
    }

    public void setFaceId(String faceId) {
        FaceId = faceId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Clazz getClazz() {
        return clazz;
    }

    public void setClazz(Clazz clazz) {
        this.clazz = clazz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(studentId, student.studentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentId);
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", FaceId='" + FaceId + '\'' +
                ", studentName='" + studentName + '\'' +
                ", tel='" + tel + '\'' +
                ", email='" + email + '\'' +
                ", pwd='" + pwd + '\'' +
                ", cardId='" + cardId + '\'' +
                ", sex='" + sex + '\'' +
                ", role='" + role + '\'' +
                ", token='" + token + '\'' +
                ", clazz=" + clazz +
                ", courses=" + courses +
                '}';
    }
}