package com.lixxing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@ApiModel
public class Answer extends BaseModel implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;

    @ApiModelProperty("作答Id")
    @Id
    private Long answerId;

    @ApiModelProperty("作答考试")
    @ManyToOne(fetch = FetchType.EAGER)
    private Exam exam;

    @ApiModelProperty("作答学生")
    @OneToOne(fetch = FetchType.EAGER)
    private Student student;

    @ApiModelProperty("题目Id")
    private Integer questionId;
    // 题目类型
    @ApiModelProperty("题目类型")
    private Integer questionType;

    // 问题
    @ApiModelProperty("问题")
    private String question;
    // 答案
    @ApiModelProperty("答案")
    private String answer;
    // 分值
    @ApiModelProperty("分值")
    private Integer score;
    // 难度等级
    @ApiModelProperty("难度等级")
    private Integer level;
    // 所属课程
    @ApiModelProperty("所属课程")
    @ManyToOne(fetch = FetchType.EAGER)
    private Course course;
    //题目解析
    @ApiModelProperty("题目解析")
    private String analysis;

    @ApiModelProperty("作答答案")
    private String answ;

    //答案A
    @ApiModelProperty("答案A")
    private String answerA;
    //答案B
    @ApiModelProperty("答案B")
    private String answerB;
    //答案C
    @ApiModelProperty("答案C")
    private String answerC;
    //答案D
    @ApiModelProperty("答案D")
    private String answerD;

    //是否已做
    @ApiModelProperty("是否已做")
    private Boolean click;

    public Answer() {
    }

    public Answer(Long answerId) {
        this.answerId = answerId;
    }

    public Answer(Long answerId, Exam exam, Student student, Integer questionId,
                  Integer questionType, String question, String answer, Integer score,
                  Integer level, Course course, String analysis, String answ, String answerA,
                  String answerB, String answerC, String answerD, Boolean click) {
        this.answerId = answerId;
        this.exam = exam;
        this.student = student;
        this.questionId = questionId;
        this.questionType = questionType;
        this.question = question;
        this.answer = answer;
        this.score = score;
        this.level = level;
        this.course = course;
        this.analysis = analysis;
        this.answ = answ;
        this.answerA = answerA;
        this.answerB = answerB;
        this.answerC = answerC;
        this.answerD = answerD;
        this.click = click;
    }

    public Boolean getClick() {
        return click;
    }

    public void setClick(Boolean click) {
        this.click = click;
    }

    public String getAnswerA() {
        return answerA;
    }

    public void setAnswerA(String answerA) {
        this.answerA = answerA;
    }

    public String getAnswerB() {
        return answerB;
    }

    public void setAnswerB(String answerB) {
        this.answerB = answerB;
    }

    public String getAnswerC() {
        return answerC;
    }

    public void setAnswerC(String answerC) {
        this.answerC = answerC;
    }

    public String getAnswerD() {
        return answerD;
    }

    public void setAnswerD(String answerD) {
        this.answerD = answerD;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getAnalysis() {
        return analysis;
    }

    public void setAnalysis(String analysis) {
        this.analysis = analysis;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Long answerId) {
        this.answerId = answerId;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public Integer getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Integer questionType) {
        this.questionType = questionType;
    }

    public String getAnsw() {
        return answ;
    }

    public void setAnsw(String answ) {
        this.answ = answ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return Objects.equals(answerId, answer.answerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(answerId);
    }

    @Override
    public String toString() {
        return "Answer{" +
                "answerId=" + answerId +
                ", exam=" + exam +
                ", student=" + student +
                ", questionId=" + questionId +
                ", questionType=" + questionType +
                ", question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", score=" + score +
                ", level='" + level + '\'' +
                ", course=" + course +
                ", analysis='" + analysis + '\'' +
                ", answ='" + answ + '\'' +
                ", answerA='" + answerA + '\'' +
                ", answerB='" + answerB + '\'' +
                ", answerC='" + answerC + '\'' +
                ", answerD='" + answerD + '\'' +
                ", click=" + click +
                '}';
    }
}
