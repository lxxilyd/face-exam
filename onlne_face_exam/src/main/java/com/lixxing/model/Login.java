package com.lixxing.model;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class Login {
    @ApiModelProperty("用户名")
    private Integer username;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("图形验证码")
    private String verifyCode;
    @ApiModelProperty("图形验证码Id")
    private String verifyCodeId;
    @ApiModelProperty("角色名")
    private String region;

    public String getVerifyCodeId() {
        return verifyCodeId;
    }

    public void setVerifyCodeId(String verifyCodeId) {
        this.verifyCodeId = verifyCodeId;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public Integer getUsername() {
        return username;
    }

    public void setUsername(Integer username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return "Login{" +
                "username=" + username +
                ", password='" + password + '\'' +
                ", verifyCode='" + verifyCode + '\'' +
                ", verifyCodeId='" + verifyCodeId + '\'' +
                ", region='" + region + '\'' +
                '}';
    }
}
