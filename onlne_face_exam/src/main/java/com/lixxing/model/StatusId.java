package com.lixxing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.Objects;

/**
 * 学生考试状态联合主键
 */
@ApiModel
@Embeddable
public class StatusId implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;

    @OneToOne
    @ApiModelProperty("学生")
    private Student student;

    @OneToOne
    @ApiModelProperty("考试")
    private Exam exam;

    public StatusId() {
    }

    public StatusId(Student student, Exam exam) {
        this.student = student;
        this.exam = exam;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatusId statusId = (StatusId) o;
        return Objects.equals(student, statusId.student) &&
                Objects.equals(exam, statusId.exam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(student, exam);
    }

    @Override
    public String toString() {
        return "StatusId{" +
                "student=" + student +
                ", exam=" + exam +
                '}';
    }
}
