package com.lixxing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

// 选择题实体
@ApiModel
@Entity
public class MultiQuestion extends BaseModel implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty("题目Id")
    private Integer questionId;
    // 题目类型
    @ApiModelProperty("题目类型")
    private Integer questionType;
    // 问题
    @ApiModelProperty("问题")
    private String question;
    // 答案
    @ApiModelProperty("答案")
    private String answer;
    // 分值
    @ApiModelProperty("分值")
    private Integer score;
    // 难度等级
    @ApiModelProperty("难度等级")
    private Integer level;
    // 科目
    @ApiModelProperty("科目")
    @ManyToOne(fetch = FetchType.EAGER)
    private Course course;
    //答案A
    @ApiModelProperty("答案A")
    private String answerA;
    //答案B
    @ApiModelProperty("答案B")
    private String answerB;
    //答案C
    @ApiModelProperty("答案C")
    private String answerC;
    //答案D
    @ApiModelProperty("答案D")
    private String answerD;
    //题目解析
    @ApiModelProperty("题目解析")
    private String analysis;

    public MultiQuestion() {
    }

    public MultiQuestion(Integer questionId) {
        this.questionId = questionId;
    }

    public MultiQuestion(Integer questionType, String question, String answer,
                         Integer score, Integer level, Course course, String answerA,
                         String answerB, String answerC, String answerD, String analysis) {
        this.questionType = questionType;
        this.question = question;
        this.answer = answer;
        this.score = score;
        this.level = level;
        this.course = course;
        this.answerA = answerA;
        this.answerB = answerB;
        this.answerC = answerC;
        this.answerD = answerD;
        this.analysis = analysis;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Integer getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Integer questionType) {
        this.questionType = questionType;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getAnswerA() {
        return answerA;
    }

    public void setAnswerA(String answerA) {
        this.answerA = answerA;
    }

    public String getAnswerB() {
        return answerB;
    }

    public void setAnswerB(String answerB) {
        this.answerB = answerB;
    }

    public String getAnswerC() {
        return answerC;
    }

    public void setAnswerC(String answerC) {
        this.answerC = answerC;
    }

    public String getAnswerD() {
        return answerD;
    }

    public void setAnswerD(String answerD) {
        this.answerD = answerD;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getAnalysis() {
        return analysis;
    }

    public void setAnalysis(String analysis) {
        this.analysis = analysis;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MultiQuestion that = (MultiQuestion) o;
        return Objects.equals(questionId, that.questionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionId);
    }

    @Override
    public String toString() {
        return "MultiQuestion{" +
                "questionId=" + questionId +
                ", questionType=" + questionType +
                ", question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", score=" + score +
                ", level='" + level + '\'' +
                ", course=" + course +
                ", answerA='" + answerA + '\'' +
                ", answerB='" + answerB + '\'' +
                ", answerC='" + answerC + '\'' +
                ", answerD='" + answerD + '\'' +
                ", analysis='" + analysis + '\'' +
                '}';
    }
}