package com.lixxing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@ApiModel
@Entity
public class Score extends BaseModel implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;

    @ApiModelProperty("成绩Id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer scoreId;

    @ApiModelProperty("学生实体")
    @OneToOne(fetch = FetchType.EAGER)
    private Student student;

    @ApiModelProperty("考试实体")
    @OneToOne(fetch = FetchType.EAGER)
    private Exam exam;

    @ApiModelProperty("作答结果")
//    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @OneToMany(fetch = FetchType.EAGER)
    private List<Answer> answers;

    @ApiModelProperty("成绩")
    private Double score;

    @ApiModelProperty("考试课程")
    @OneToOne(fetch = FetchType.EAGER)
    private Course course;

    @ApiModelProperty("开始作答时间")
    private Timestamp startTime;

    @ApiModelProperty("交卷时间")
    private Timestamp endTime;

    public Score() {
    }

    public Score(Integer scoreId) {
        this.scoreId = scoreId;
    }

    public Score(Student student, Exam exam, List<Answer> answers, Double score,
                 Course course, Timestamp startTime, Timestamp endTime) {
        this.student = student;
        this.exam = exam;
        this.answers = answers;
        this.score = score;
        this.course = course;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getScoreId() {
        return scoreId;
    }

    public void setScoreId(Integer scoreId) {
        this.scoreId = scoreId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "Score{" +
                "scoreId=" + scoreId +
                ", student=" + student +
                ", exam=" + exam +
                ", answers=" + answers +
                ", score=" + score +
                ", course=" + course +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}