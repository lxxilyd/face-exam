package com.lixxing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * 班级实体类
 */
@ApiModel
@Entity
public class Clazz extends BaseModel implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;

    @Id
    @ApiModelProperty("班级Id")
    private Integer clazzId;
    @ApiModelProperty("班级名称")
    private String clazzName;
    @ApiModelProperty("年级")
    private Integer grade;
    @OneToOne(fetch = FetchType.EAGER)
    @ApiModelProperty("专业")
    private Profession profession;

    public Clazz() {
    }

    public Clazz(Integer clazzId) {
        this.clazzId = clazzId;
    }

    public Clazz(Integer clazzId, String clazzName, Integer grade) {
        this.clazzId = clazzId;
        this.clazzName = clazzName;
        this.grade = grade;
    }

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getClazzId() {
        return clazzId;
    }

    public void setClazzId(Integer clazzId) {
        this.clazzId = clazzId;
    }

    public String getClazzName() {
        return clazzName;
    }

    public void setClazzName(String clazzName) {
        this.clazzName = clazzName;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Clazz clazz = (Clazz) o;
        return Objects.equals(clazzId, clazz.clazzId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clazzId);
    }

    @Override
    public String toString() {
        return "Clazz{" +
                "clazzId=" + clazzId +
                ", clazzName='" + clazzName + '\'' +
                ", grade=" + grade +
                ", profession=" + profession +
                '}';
    }
}
