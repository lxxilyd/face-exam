package com.lixxing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

//判断题实体类
@ApiModel
@Entity
public class JudgeQuestion extends BaseModel implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty("题目Id")
    private Integer questionId;
    // 题目类型
    @ApiModelProperty("题目类型")
    private Integer questionType;
    // 问题
    @ApiModelProperty("问题")
    private String question;
    // 答案
    @ApiModelProperty("答案")
    private String answer;
    // 分值
    @ApiModelProperty("分值")
    private Integer score;
    // 难度等级
    @ApiModelProperty("难度等级")
    private Integer level;
    // 科目
    @ApiModelProperty("科目")
    @ManyToOne(fetch = FetchType.EAGER)
    private Course course;
    //题目解析
    @ApiModelProperty("题目解析")
    private String analysis;
    public JudgeQuestion() {
    }

    public JudgeQuestion(Integer questionId) {
        this.questionId = questionId;
    }

    public JudgeQuestion(Integer questionType, String question, String answer,
                         Integer score, Integer level, Course course, String analysis) {
        this.questionType = questionType;
        this.question = question;
        this.answer = answer;
        this.score = score;
        this.level = level;
        this.course = course;
        this.analysis = analysis;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Integer getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Integer questionType) {
        this.questionType = questionType;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getAnalysis() {
        return analysis;
    }

    public void setAnalysis(String analysis) {
        this.analysis = analysis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JudgeQuestion that = (JudgeQuestion) o;
        return Objects.equals(questionId, that.questionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionId);
    }

    @Override
    public String toString() {
        return "JudgeQuestion{" +
                "questionId=" + questionId +
                ", questionType=" + questionType +
                ", question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", score=" + score +
                ", level='" + level + '\'' +
                ", course=" + course +
                ", analysis='" + analysis + '\'' +
                '}';
    }
}