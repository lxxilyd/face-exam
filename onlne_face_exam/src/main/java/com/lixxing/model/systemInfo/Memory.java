package com.lixxing.model.systemInfo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("内存实体")
public class Memory {
    @ApiModelProperty("内存总数(GB)")
    Double phyTotal;
    @ApiModelProperty("已使用的内存数(GB)")
    Double phyUsed;
    @ApiModelProperty("未使用的内存数(GB)")
    Double phyFree;
    @ApiModelProperty("内存使用率")
    Double phyUsage;
    @ApiModelProperty("SWAP交换区总数(GB)")
    Double swapTotal;
    @ApiModelProperty("SWAP交换区已使用数(GB)")
    Double swapUsed;
    @ApiModelProperty("SWAP交换区未使用数(GB)")
    Double swapFree;
    @ApiModelProperty("交换区使用率")
    Double swapUsage;

    public Memory() {
    }

    public Double getPhyTotal() {
        return phyTotal;
    }

    public void setPhyTotal(Double phyTotal) {
        this.phyTotal = phyTotal;
    }

    public Double getPhyUsed() {
        return phyUsed;
    }

    public void setPhyUsed(Double phyUsed) {
        this.phyUsed = phyUsed;
    }

    public Double getPhyFree() {
        return phyFree;
    }

    public void setPhyFree(Double phyFree) {
        this.phyFree = phyFree;
    }

    public Double getPhyUsage() {
        return phyUsage;
    }

    public void setPhyUsage(Double phyUsage) {
        this.phyUsage = phyUsage;
    }

    public Double getSwapTotal() {
        return swapTotal;
    }

    public void setSwapTotal(Double swapTotal) {
        this.swapTotal = swapTotal;
    }

    public Double getSwapUsed() {
        return swapUsed;
    }

    public void setSwapUsed(Double swapUsed) {
        this.swapUsed = swapUsed;
    }

    public Double getSwapFree() {
        return swapFree;
    }

    public void setSwapFree(Double swapFree) {
        this.swapFree = swapFree;
    }

    public Double getSwapUsage() {
        return swapUsage;
    }

    public void setSwapUsage(Double swapUsage) {
        this.swapUsage = swapUsage;
    }

    @Override
    public String toString() {
        return "Memory{" +
                "phyTotal=" + phyTotal +
                ", phyUsed=" + phyUsed +
                ", phyFree=" + phyFree +
                ", phyUsage=" + phyUsage +
                ", swapTotal=" + swapTotal +
                ", swapUsed=" + swapUsed +
                ", swapFree=" + swapFree +
                ", swapUsage=" + swapUsage +
                '}';
    }
}
