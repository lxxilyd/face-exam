package com.lixxing.model.systemInfo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("CPU实体")
public class CPU {
    @ApiModelProperty("CPU核心数")
    Integer cores;
    @ApiModelProperty("CPU信息")
    String model;
    @ApiModelProperty("CPU制造商")
    String maker;
    @ApiModelProperty("CPU使用率")
    Double cpuUsage;
    @ApiModelProperty("CPU空闲率")
    Double cpuFreeage;

    public CPU() {
    }

    public CPU(Integer cores, String model, String maker) {
        this.cores = cores;
        this.model = model;
        this.maker = maker;
    }

    public Double getCpuUsage() {
        return cpuUsage;
    }

    public void setCpuUsage(Double cpuUsage) {
        this.cpuUsage = cpuUsage;
    }

    public Double getCpuFreeage() {
        return cpuFreeage;
    }

    public void setCpuFreeage(Double cpuFreeage) {
        this.cpuFreeage = cpuFreeage;
    }

    public Integer getCores() {
        return cores;
    }

    public void setCores(Integer cores) {
        this.cores = cores;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    @Override
    public String toString() {
        return "Cpu{" +
                "cores=" + cores +
                ", model='" + model + '\'' +
                ", maker='" + maker + '\'' +
                ", cpuUsage=" + cpuUsage +
                ", cpuFreeage=" + cpuFreeage +
                '}';
    }
}
