package com.lixxing.model.systemInfo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

@ApiModel("磁盘实体")
public class Disk {
    @ApiModelProperty("盘符名")
    String diskName;
    @ApiModelProperty("文件系统类型")
    String diskFileSysType;
    @ApiModelProperty("磁盘容量(GB)")
    Long diskTotal;
    @ApiModelProperty("磁盘已使用数(GB)")
    Long diskUsed;
    @ApiModelProperty("磁盘未使用数(GB)")
    Long diskFree;
    @ApiModelProperty("磁盘使用率")
    Double diskUsage;

    public Disk() {
    }

    public String getDiskName() {
        return diskName;
    }

    public void setDiskName(String diskName) {
        this.diskName = diskName;
    }

    public String getDiskFileSysType() {
        return diskFileSysType;
    }

    public void setDiskFileSysType(String diskFileSysType) {
        this.diskFileSysType = diskFileSysType;
    }

    public Long getDiskTotal() {
        return diskTotal;
    }

    public void setDiskTotal(Long diskTotal) {
        this.diskTotal = diskTotal;
    }

    public Long getDiskUsed() {
        return diskUsed;
    }

    public void setDiskUsed(Long diskUsed) {
        this.diskUsed = diskUsed;
    }

    public Long getDiskFree() {
        return diskFree;
    }

    public void setDiskFree(Long diskFree) {
        this.diskFree = diskFree;
    }

    public Double getDiskUsage() {
        return diskUsage;
    }

    public void setDiskUsage(Double diskUsage) {
        this.diskUsage = diskUsage;
    }

    @Override
    public String toString() {
        return "Disk{" +
                "diskName='" + diskName + '\'' +
                ", diskFileSysType='" + diskFileSysType + '\'' +
                ", diskTotal=" + diskTotal +
                ", diskUsed=" + diskUsed +
                ", diskFree=" + diskFree +
                ", diskUsage=" + diskUsage +
                '}';
    }
}
