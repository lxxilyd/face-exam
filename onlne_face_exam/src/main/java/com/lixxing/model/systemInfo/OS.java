package com.lixxing.model.systemInfo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("操作系统实体")
public class OS {
    @ApiModelProperty("操作系统名称")
    String osName;
    @ApiModelProperty("系统制造商")
    String osMaker;
    @ApiModelProperty("系统架构")
    String osArch;
    @ApiModelProperty("系统版本")
    String osVersion;

    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    public String getOsMaker() {
        return osMaker;
    }

    public void setOsMaker(String osMaker) {
        this.osMaker = osMaker;
    }

    public String getOsArch() {
        return osArch;
    }

    public void setOsArch(String osArch) {
        this.osArch = osArch;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    @Override
    public String toString() {
        return "OS{" +
                "osName='" + osName + '\'' +
                ", osMaker='" + osMaker + '\'' +
                ", osArch='" + osArch + '\'' +
                ", osVersion='" + osVersion + '\'' +
                '}';
    }
}
