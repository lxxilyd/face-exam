package com.lixxing.model.systemInfo;

import com.lixxing.model.systemInfo.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel("系统信息")
public class SystemInfo {
    @ApiModelProperty("CPU信息")
    private List<CPU> cpus;
    @ApiModelProperty("内存信息")
    private Memory memory;
    @ApiModelProperty("磁盘信息")
    private List<Disk> disks;
    @ApiModelProperty("操作系统信息")
    private OS os;
    @ApiModelProperty("网卡信息")
    private NetWork netWork;

    public SystemInfo() {
    }

    public List<CPU> getCPU() {
        return cpus;
    }

    public void setCPU(List<CPU> cpus) {
        this.cpus = cpus;
    }

    public Memory getMemory() {
        return memory;
    }

    public void setMemory(Memory memory) {
        this.memory = memory;
    }

    public List<Disk> getDisk() {
        return disks;
    }

    public void setDisk(List<Disk> disks) {
        this.disks = disks;
    }

    public OS getOs() {
        return os;
    }

    public void setOs(OS os) {
        this.os = os;
    }

    public NetWork getNetWork() {
        return netWork;
    }

    public void setNetWork(NetWork netWork) {
        this.netWork = netWork;
    }

    @Override
    public String toString() {
        return "SystemInfo{" +
                "cpu=" + cpus +
                ", memory=" + memory +
                ", disk=" + disks +
                ", os=" + os +
                ", netWork=" + netWork +
                '}';
    }
}
