package com.lixxing.model.systemInfo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("网卡实体")
public class NetWork {
    @ApiModelProperty("网卡名")
    String netFaceName;
    @ApiModelProperty("出口流量(byte)")
    Long TxFlow;
    @ApiModelProperty("入口流量(byte)")
    Long RxFlow;
    @ApiModelProperty("接收网速(bps)")
    Long rxbps;
    @ApiModelProperty("发送网速(bps)")
    Long txbps;

    public Long getRxbps() {
        return rxbps;
    }

    public void setRxbps(Long rxbps) {
        this.rxbps = rxbps;
    }

    public Long getTxbps() {
        return txbps;
    }

    public void setTxbps(Long txbps) {
        this.txbps = txbps;
    }

    public String getNetFaceName() {
        return netFaceName;
    }

    public void setNetFaceName(String netFaceName) {
        this.netFaceName = netFaceName;
    }

    public Long getTxFlow() {
        return TxFlow;
    }

    public void setTxFlow(Long txFlow) {
        TxFlow = txFlow;
    }

    public Long getRxFlow() {
        return RxFlow;
    }

    public void setRxFlow(Long rxFlow) {
        RxFlow = rxFlow;
    }

    @Override
    public String toString() {
        return "NetWork{" +
                "netFaceName='" + netFaceName + '\'' +
                ", TxFlow=" + TxFlow +
                ", RxFlow=" + RxFlow +
                ", rxbps=" + rxbps +
                ", txbps=" + txbps +
                '}';
    }
}
