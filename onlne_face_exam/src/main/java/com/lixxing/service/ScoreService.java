package com.lixxing.service;

import com.lixxing.model.Score;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ScoreService {
    int add(Score score);

    List<Score> findAll();

    Page<Score> findAll(Pageable pageable);

    Page<Score> findById(Pageable pageable, Integer studentId);

    List<Score> findByStudentId(Integer studentId);

    Score findById(Integer scoreId);

    List<Score> findByExamCode(Integer examCode);

    Page<Score> findByExamCode(Pageable pageable,Integer examCode);
}
