package com.lixxing.service;

import com.lixxing.model.Paper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PaperService {

    public int add(Paper paper);

    public int delete(Integer paperId);

    public int update(Paper paper);

    public Paper findById(Integer paperId);

    public Page<Paper> findAll(Pageable pageable);

    public List<Paper> findAll();
}
