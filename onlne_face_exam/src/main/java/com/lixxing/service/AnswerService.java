package com.lixxing.service;


import com.lixxing.model.Answer;
import com.lixxing.model.Exam;
import com.lixxing.model.Score;
import com.lixxing.model.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AnswerService {
    Page<Answer> findAll(Pageable pageable);

    List<Answer> findAll();

    List<Answer> findAllByStudentAndExam(Student student, Exam exam);

    Answer findById(Long answerId);

    Answer findByStudentAndExamAndId(Long answerId, Student student, Exam exam);

    int delete(Long answerId);

    int update(Answer answer);

    int add(Answer answer);

    Score getScore(List<Answer> answers, Long startTime, Long endTime);
}
