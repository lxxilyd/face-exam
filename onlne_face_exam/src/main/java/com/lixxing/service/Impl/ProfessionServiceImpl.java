package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.ProfessionDao;
import com.lixxing.model.Profession;
import com.lixxing.model.Profession;
import com.lixxing.service.ProfessionService;
import com.lixxing.service.ProfessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ProfessionServiceImpl implements ProfessionService {

    @Autowired
    ProfessionDao professionDao;

    @Override
    @CacheEvict(value = "professions",allEntries = true)
    public int add(Profession profession) {
        professionDao.saveAndFlush(profession);
        return 1;
    }

    @Override
    @CacheEvict(value = "professions",allEntries = true)
    public int delete(Integer professionId) {
        return professionDao.removeByProfessionId(professionId);
    }

    @Override
    @CacheEvict(value = "professions",allEntries = true)
    public int update(Profession profession) {
        try {
            Profession oldProfession = professionDao.findByProfessionId(profession.getProfessionId());
            BeanUtil.copyProperties(profession,oldProfession,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            professionDao.saveAndFlush(oldProfession);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @Cacheable(value = "professions")
    public Profession findById(Integer professionId) {
        return professionDao.findByProfessionId(professionId);
    }

    @Override
    @Cacheable(value = "professions")
    public Page<Profession> findAll(Pageable pageable) {
        return professionDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "professions")
    public List<Profession> findAll() {
        return professionDao.findAll();
    }
}
