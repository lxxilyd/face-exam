package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.FillQuestionDao;
import com.lixxing.model.Course;
import com.lixxing.model.FillQuestion;
import com.lixxing.service.FillQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class FillQuestionServiceImpl implements FillQuestionService {

    @Autowired
    private FillQuestionDao fillQuestionDao;

    @Override
    @Cacheable(value = "fillQuestions")
    public List<FillQuestion> findByIdAndType(Integer paperId) {
        return fillQuestionDao.findAll(paperId);
    }

    @Override
    @Cacheable(value = "fillQuestions")
    public Page<FillQuestion> findAll(Pageable pageable) {
        return fillQuestionDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "fillQuestions")
    public Page<FillQuestion> findAll(Pageable pageable, Integer courseId) {
        return fillQuestionDao.findAllByCourse(pageable,new Course(courseId));
    }

    @Override
    @Cacheable(value = "fillQuestions")
    public FillQuestion findOnlyQuestionId() {
        return fillQuestionDao.findTopByOrderByQuestionIdDesc();
    }

    @Override
    @Cacheable(value = "fillQuestions")
    public FillQuestion findByQuestionId(Integer questionId) {
        return fillQuestionDao.findByQuestionId(questionId);
    }

    @Override
    @CacheEvict(value = "fillQuestions",allEntries = true)
    public int add(FillQuestion fillQuestion) {
        fillQuestionDao.saveAndFlush(fillQuestion);
        return 1;
    }

    @Override
    @CacheEvict(value = "fillQuestions",allEntries = true)
    public int update(FillQuestion fillQuestion) {
        try {
            FillQuestion oldFillQuestion = fillQuestionDao.findByQuestionId(fillQuestion.getQuestionId());
            BeanUtil.copyProperties(fillQuestion,oldFillQuestion,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            fillQuestionDao.saveAndFlush(oldFillQuestion);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "fillQuestions",allEntries = true)
    public int delete(Integer questionId) {
        return fillQuestionDao.removeByQuestionId(questionId);
    }

    @Override
    public List<Integer> findBySubject(String subject, Integer pageNo) {
        return fillQuestionDao.findBySubject(subject,pageNo);
    }
}
