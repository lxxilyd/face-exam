package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.ClazzDao;
import com.lixxing.model.Clazz;
import com.lixxing.service.ClazzService;
import com.lixxing.service.ClazzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class ClazzServiceImpl implements ClazzService {

    @Autowired
    ClazzDao clazzDao;

    @Override
    @CacheEvict(value = "clazzs",allEntries = true)
    public int add(Clazz clazz) {
        clazzDao.saveAndFlush(clazz);
        return 1;
    }

    @Override
    @CacheEvict(value = "clazzs",allEntries = true)
    public int delete(Integer clazzId) {
        return clazzDao.removeByClazzId(clazzId);
    }

    @Override
    @CacheEvict(value = "clazzs",allEntries = true)
    public int update(Clazz clazz) {
        try {
            Clazz oldClazz = clazzDao.findByClazzId(clazz.getClazzId());
            BeanUtil.copyProperties(clazz,oldClazz,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            clazzDao.saveAndFlush(oldClazz);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @Cacheable(value = "clazzs")
    public Clazz findById(Integer clazzId) {
        return clazzDao.findByClazzId(clazzId);
    }

    @Override
    @Cacheable(value = "clazzs")
    public Page<Clazz> findAll(Pageable pageable) {
        return clazzDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "clazzs")
    public List<Clazz> findAll() {
        return clazzDao.findAll();
    }
}
