package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.AnswerDao;
import com.lixxing.dao.FillQuestionDao;
import com.lixxing.dao.JudgeQuestionDao;
import com.lixxing.dao.MultiQuestionDao;
import com.lixxing.model.*;
import com.lixxing.service.AnswerService;
import com.lixxing.utils.CloneUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    private AnswerDao answerDao;
    @Autowired
    private MultiQuestionDao multiQuestionDao;
    @Autowired
    FillQuestionDao fillQuestionDao;
    @Autowired
    JudgeQuestionDao judgeQuestionDao;

    @Override
    @Cacheable(value = "answers")
    public Page<Answer> findAll(Pageable pageable) {
        return answerDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "answers")
    public List<Answer> findAll() {
        return answerDao.findAll();
    }

    @Override
//    @Cacheable(value = "answers")
    public List<Answer> findAllByStudentAndExam(Student student, Exam exam) {
        List<Answer> allByStudentAndExam = answerDao.findAllByStudentAndExam(student, exam);
        for (Answer answer : allByStudentAndExam) {
            Exam exam1 = answer.getExam();
            // 深克隆避免影响数据库数据
            Exam exam2 = null;
            try {
                exam2 = CloneUtil.deepClone(exam1);
            }catch (Exception e){
                e.printStackTrace();
            }
            Paper paper = exam2.getPaper();
            Set<JudgeQuestion> judgeQuestions = paper.getJudgeQuestions();
            Set<FillQuestion> fillQuestions = paper.getFillQuestions();
            Set<MultiQuestion> multiQuestions = paper.getMultiQuestions();
            // 判断考试类型，若不是练习则去除答案
            if (exam2.getType() == 1 || exam2.getType() == 0){
                for (JudgeQuestion judgeQuestion : judgeQuestions) {
                    judgeQuestion.setAnswer(null);
                    judgeQuestion.setAnalysis(null);
                }
                for (FillQuestion fillQuestion : fillQuestions) {
                    fillQuestion.setAnswer(null);
                    fillQuestion.setAnalysis(null);
                }
                for (MultiQuestion multiQuestion : multiQuestions) {
                    multiQuestion.setAnswer(null);
                    multiQuestion.setAnalysis(null);
                }
                paper.setJudgeQuestions(judgeQuestions);
                paper.setFillQuestions(fillQuestions);
                paper.setMultiQuestions(multiQuestions);
                exam2.setPaper(paper);
                answer.setExam(exam2);
            }
        }
        return allByStudentAndExam;
    }

    @Override
    @Cacheable(value = "answers")
    public Answer findById(Long answerId) {
        return answerDao.findByAnswerId(answerId);
    }

    @Override
    @Cacheable(value = "answers")
    public Answer findByStudentAndExamAndId(Long answerId, Student student, Exam exam) {
        return answerDao.findByAnswerIdAndStudentAndExam(answerId,student,exam);
    }

    @Override
    @CacheEvict(value = "answers",allEntries = true)
    public int delete(Long answerId) {
        answerDao.removeByAnswerId(answerId);
        return 1;
    }

    @Override
    @CacheEvict(value = "answers",allEntries = true)
    public int update(Answer answer) {
        try {
            Answer oldAnswer = answerDao.findByAnswerId(answer.getAnswerId());
            BeanUtil.copyProperties(answer,oldAnswer,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            answerDao.saveAndFlush(oldAnswer);
            return 1;
        }catch (Exception e){
            answerDao.saveAndFlush(answer);
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "answers",allEntries = true)
    public int add(Answer answer) {
        answerDao.saveAndFlush(answer);
        return 1;
    }

    @Override
    public Score getScore(List<Answer> answers,Long startTime,Long endTime) {
        Exam exam = answers.get(0).getExam();
        Student student = answers.get(0).getStudent();
        // 保存最终作答结果
        Long answerId = System.currentTimeMillis();
        for (Answer answer : answers) {
            if (null != answer){
                if (null == answer.getAnswerId()) answer.setAnswerId(answerId++);
                if (answer.getQuestionType() == 1){
                    MultiQuestion byId = multiQuestionDao.getById(answer.getQuestionId());
                    answer.setAnswer(byId.getAnswer());
                    answer.setAnalysis(byId.getAnalysis());
                }
                answerDao.saveAndFlush(answer);
            }
        }
        // 计算成绩
        Double totalScore = 0.0;
        if (exam.getType() == 2){
            if (null != answers){
                for (Answer answer : answers) {
                    if (answer.getAnsw() == null) continue;
                    if (answer.getExam().getType() == 2 && answer.getAnsw().equals(answer.getAnswer())){
                        totalScore += answer.getScore();
                    }
                }
            }
        }else {
            if (null != answers){
                for (Answer answer : answers) {
                    // 选择题
                    if (answer.getQuestionType() == 1){
                        MultiQuestion multiQuestion = multiQuestionDao.findByQuestionIdAndAnswer(
                                answer.getQuestionId(), answer.getAnsw());
                        if (null != multiQuestion) {
                            totalScore += answer.getScore();
                            // 将正确答案保存到作答结果中
                            answer.setAnalysis(multiQuestion.getAnalysis());
                            answer.setAnswer(multiQuestion.getAnswer());
                            answerDao.saveAndFlush(answer);
                        }
                    }
                    // 选择题
                    if (answer.getQuestionType() == 2){
                        FillQuestion fillQuestion = fillQuestionDao.findByQuestionIdAndAnswer(
                                answer.getQuestionId(), answer.getAnsw());
                        if (null != fillQuestion) {
                            totalScore += answer.getScore();
                            // 将正确答案保存到作答结果中
                            answer.setAnalysis(fillQuestion.getAnalysis());
                            answer.setAnswer(fillQuestion.getAnswer());
                            answerDao.saveAndFlush(answer);
                        }
                    }
                    // 选择题
                    if (answer.getQuestionType() == 3){
                        JudgeQuestion judgeQuestion = judgeQuestionDao.findByQuestionIdAndAnswer(
                                answer.getQuestionId(), answer.getAnsw());
                        if (null != judgeQuestion) {
                            totalScore += answer.getScore();
                            // 将正确答案保存到作答结果中
                            answer.setAnalysis(judgeQuestion.getAnalysis());
                            answer.setAnswer(judgeQuestion.getAnswer());
                            answerDao.saveAndFlush(answer);
                        }
                    }
                }
            }
        }
        // 构造并保存成绩对象
        Score score = new Score();
        score.setAnswers(answers);
        score.setExam(exam);
        score.setStudent(student);
        score.setStartTime(new Timestamp(startTime));
        score.setEndTime(new Timestamp(endTime));
        score.setScore(totalScore);
        return score;
    }
}
