package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.MultiQuestionDao;
import com.lixxing.model.Course;
import com.lixxing.model.MultiQuestion;
import com.lixxing.service.MultiQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class MultiQuestionServiceImpl implements MultiQuestionService {

    @Autowired
    private MultiQuestionDao multiQuestionDao;

    @Override
    @Cacheable(value = "multiQuestions")
    public List<MultiQuestion> findByIdAndType(Integer PaperId) {
        return multiQuestionDao.findByIdAndType(PaperId);
    }

    @Override
    @Cacheable(value = "multiQuestions")
    public List<MultiQuestion> findAll() {
        return multiQuestionDao.findAll();
    }

    @Override
    @Cacheable(value = "multiQuestions")
    public Page<MultiQuestion> findAll(Pageable pageable) {
        return multiQuestionDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "multiQuestions")
    public Page<MultiQuestion> findAll(Pageable pageable, Integer courseId) {
        return multiQuestionDao.findAllByCourse(pageable,new Course(courseId));
    }

    @Override
    @Cacheable(value = "multiQuestions")
    public MultiQuestion findOnlyQuestionId() {
        return multiQuestionDao.findTopByOrderByQuestionIdDesc();
    }

    @Override
    @Cacheable(value = "multiQuestions")
    public MultiQuestion findByQuestionId(Integer QuestionId) {
        return multiQuestionDao.findByQuestionId(QuestionId);
    }

    @Override
    @CacheEvict(value = "multiQuestions",allEntries = true)
    public int add(MultiQuestion multiQuestion) {
        multiQuestionDao.saveAndFlush(multiQuestion);
        return 1;
    }

    @Override
    @CacheEvict(value = "multiQuestions",allEntries = true)
    public int update(MultiQuestion multiQuestion) {
        try {
            MultiQuestion oldMultiQuestion = multiQuestionDao.findByQuestionId(multiQuestion.getQuestionId());
            BeanUtil.copyProperties(multiQuestion,oldMultiQuestion,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            multiQuestionDao.saveAndFlush(oldMultiQuestion);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "multiQuestions",allEntries = true)
    public int delete(Integer questionId) {
        multiQuestionDao.removeByQuestionId(questionId);
        return 1;
    }

    @Override
    @Cacheable(value = "multiQuestions")
    public List<Integer> findBySubject(String subject, Integer pageNo) {
        return multiQuestionDao.findBySubject(subject,pageNo);
    }
}
