package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.CourseDao;
import com.lixxing.model.Course;
import com.lixxing.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CourseServiceImpl implements CourseService {

    @Autowired
    CourseDao courseDao;

    @Override
    @CacheEvict(value = "courses",allEntries = true)
    public int add(Course course) {
        courseDao.saveAndFlush(course);
        return 1;
    }

    @Override
    @CacheEvict(value = "courses",allEntries = true)
    public int delete(Integer courseId) {
        return courseDao.removeByCourseId(courseId);
    }

    @Override
    @CacheEvict(value = "courses",allEntries = true)
    public int update(Course course) {
        try {
            Course oldCourse = courseDao.findByCourseId(course.getCourseId());
            BeanUtil.copyProperties(course,oldCourse,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            courseDao.saveAndFlush(oldCourse);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @Cacheable(value = "courses")
    public Course findById(Integer courseId) {
        return courseDao.findByCourseId(courseId);
    }

    @Override
    @Cacheable(value = "courses")
    public Page<Course> findAll(Pageable pageable) {
        return courseDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "courses")
    public List<Course> findAll() {
        return courseDao.findAll();
    }
}
