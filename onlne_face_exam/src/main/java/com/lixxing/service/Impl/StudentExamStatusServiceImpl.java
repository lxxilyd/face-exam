package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.StudentExamStatusDao;
import com.lixxing.model.*;
import com.lixxing.service.StudentExamStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class StudentExamStatusServiceImpl implements StudentExamStatusService {

    @Autowired
    private StudentExamStatusDao statusDao;

    @Override
    @Cacheable(value = "student_exam_status")
    public Page<StudentExamStatus> findAll(Pageable pageable) {
        return statusDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "student_exam_status")
    public List<StudentExamStatus> findAll() {
        return statusDao.findAll();
    }

    @Override
    @Cacheable(value = "student_exam_status")
    public List<StudentExamStatus> findAllByStudentAndExam(Student student, Exam exam) {
        return statusDao.findAllByStudentAndExam(student, exam);
    }

    @Override
    @Cacheable(value = "student_exam_status")
    public StudentExamStatus findById(StatusId statusId) {
        return statusDao.findByStatusId(statusId);
    }

    @Override
    @Cacheable(value = "student_exam_status")
    public StudentExamStatus findByStudentAndExam(Student student, Exam exam) {
        return statusDao.findByStudentAndExam(student,exam);
    }

    @Override
    @Cacheable(value = "student_exam_status")
    public StudentExamStatus findByStudentAndExamAndId(StatusId statusId, Student student, Exam exam) {
        return statusDao.findByStatusIdAndStudentAndExam(statusId,student,exam);
    }

    @Override
    @CacheEvict(value = "student_exam_status",allEntries = true)
    public int delete(StatusId statusId) {
        statusDao.removeByStatusId(statusId);
        return 1;
    }

    @Override
    @CacheEvict(value = "student_exam_status",allEntries = true)
    public int update(StudentExamStatus status) {
        try {
            StudentExamStatus oldStudentExamStatus = statusDao.findByStatusId(status.getStatusId());
            BeanUtil.copyProperties(status,oldStudentExamStatus,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            statusDao.saveAndFlush(oldStudentExamStatus);
            return 1;
        }catch (Exception e){
            statusDao.saveAndFlush(status);
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "student_exam_status",allEntries = true)
    public int add(StudentExamStatus status) {
        statusDao.saveAndFlush(status);
        return 1;
    }
}
