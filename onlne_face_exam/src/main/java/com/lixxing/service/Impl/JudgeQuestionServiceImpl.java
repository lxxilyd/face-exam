package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.JudgeQuestionDao;
import com.lixxing.model.Course;
import com.lixxing.model.JudgeQuestion;
import com.lixxing.service.JudgeQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class JudgeQuestionServiceImpl implements JudgeQuestionService {

    @Autowired
    private JudgeQuestionDao judgeQuestionDao;

    @Override
    @Cacheable(value = "judgeQuestions")
    public List<JudgeQuestion> findByIdAndType(Integer paperId) {
        return judgeQuestionDao.findByIdAndType(paperId);
    }

    @Override
    @Cacheable(value = "judgeQuestions")
    public Page<JudgeQuestion> findAll(Pageable pageable) {
        return judgeQuestionDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "judgeQuestions")
    public Page<JudgeQuestion> findAll(Pageable pageable, Integer courseId) {
        return judgeQuestionDao.findAllByCourse(pageable,new Course(courseId));
    }

    @Override
    @Cacheable(value = "judgeQuestions")
    public JudgeQuestion findOnlyQuestionId() {
        return judgeQuestionDao.findTopByOrderByQuestionIdDesc();
    }

    @Override
    @CacheEvict(value = "judgeQuestions",allEntries = true)
    public int add(JudgeQuestion judgeQuestion) {
        judgeQuestionDao.saveAndFlush(judgeQuestion);
        return 1;
    }

    @Override
    @CacheEvict(value = "judgeQuestions",allEntries = true)
    public int update(JudgeQuestion judgeQuestion) {
        try {
            JudgeQuestion oldJudgeQuestion = judgeQuestionDao.findByQuestionId(judgeQuestion.getQuestionId());
            BeanUtil.copyProperties(judgeQuestion,oldJudgeQuestion,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            judgeQuestionDao.saveAndFlush(oldJudgeQuestion);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "judgeQuestions",allEntries = true)
    public int delete(Integer questionId) {
        try {
            return judgeQuestionDao.removeByQuestionId(questionId);
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    @Cacheable(value = "judgeQuestions")
    public JudgeQuestion findByQuestionId(Integer questionId) {
        return judgeQuestionDao.findByQuestionId(questionId);
    }

    @Override
    @Cacheable(value = "judgeQuestions")
    public List<Integer> findBySubject(String subject, Integer pageNo) {
        return judgeQuestionDao.findBySubject(subject,pageNo);
    }
}
