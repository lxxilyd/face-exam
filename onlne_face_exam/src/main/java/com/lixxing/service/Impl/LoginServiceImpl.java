package com.lixxing.service.Impl;

import cn.hutool.crypto.SecureUtil;
import com.lixxing.dao.LoginDao;
import com.lixxing.model.Admin;
import com.lixxing.model.Student;
import com.lixxing.model.Teacher;
import com.lixxing.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginDao loginDao;

    @Override
    public Admin adminLogin(Integer username, String password) {
        password = SecureUtil.md5(password+username);
        return loginDao.adminLogin(username,password);
    }

    @Override
    public Teacher teacherLogin(Integer username, String password) {
        password = SecureUtil.md5(password+username);
        return loginDao.teacherLogin(username,password);
    }

    @Override
    public Student studentLogin(Integer username, String password) {
        password = SecureUtil.md5(password+username);
        return loginDao.studentLogin(username,password);
    }
}
