package com.lixxing.service.Impl;

import com.lixxing.dao.ScoreDao;
import com.lixxing.model.Exam;
import com.lixxing.model.Score;
import com.lixxing.model.Student;
import com.lixxing.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ScoreServiceImpl implements ScoreService {

    @Autowired
    private ScoreDao scoreDao;

    @Override
    @CacheEvict(value = "scores",allEntries = true)
    public int add(Score score) {
        scoreDao.saveAndFlush(score);
        return 1;
    }

    @Override
    @Cacheable(value = "scores")
    public List<Score> findAll() {
        return scoreDao.findAll();
    }

    @Override
    @Cacheable(value = "scores")
    public Page<Score> findAll(Pageable pageable) {
        return scoreDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "scores")
    public Page<Score> findById(Pageable pageable, Integer studentId) {
        return scoreDao.findAllByStudent(pageable,new Student(studentId));
    }

    @Override
    @Cacheable(value = "scores")
    public List<Score> findByStudentId(Integer studentId) {
        return scoreDao.findAllByStudent(new Student(studentId));
    }

    @Override
    @Cacheable(value = "scores")
    public Score findById(Integer scoreId) {
        return scoreDao.findByScoreId(scoreId);
    }

    @Override
    @Cacheable(value = "scores")
    public List<Score> findByExamCode(Integer examCode) {
        return scoreDao.findAllByExam(new Exam(examCode));
    }

    @Override
    @Cacheable(value = "scores")
    public Page<Score> findByExamCode(Pageable pageable,Integer examCode) {
        return scoreDao.findAllByExam(pageable,new Exam(examCode));
    }
}
