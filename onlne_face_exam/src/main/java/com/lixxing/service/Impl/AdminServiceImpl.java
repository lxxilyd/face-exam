package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.crypto.SecureUtil;
import com.lixxing.dao.AdminDao;
import com.lixxing.model.Admin;
import com.lixxing.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminDao adminDao;

    @Override
    @Cacheable(value = "admins")
    public List<Admin> findAll() {
        return adminDao.findAll();
    }

    @Override
    @Cacheable(value = "admins")
    public Admin findById(Integer adminId) {
        return adminDao.findByAdminId(adminId);
    }

    @Override
    @Cacheable(value = "admins")
    public Admin findByIdAndEmail(Integer adminId, String email) {
        return adminDao.findByAdminIdAndEmail(adminId,email);
    }

    @Override
    @CacheEvict(value = "admins",allEntries = true)
    public int deleteById(int adminId) {
        return adminDao.removeByAdminId(adminId);
    }

    @Override
    @CacheEvict(value = "admins",allEntries = true)
    public int update(Admin admin) {
        String pwd = admin.getPwd();
        Integer adminId = admin.getAdminId();
        admin.setPwd(SecureUtil.md5(pwd+adminId));
        try {
            Admin oldAdmin = adminDao.findById(admin.getAdminId()).get();
            BeanUtil.copyProperties(admin,oldAdmin,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            adminDao.saveAndFlush(oldAdmin);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "admins",allEntries = true)
    public int updatePwd(Admin admin) {
        String pwd = admin.getPwd();
        Integer adminId = admin.getAdminId();
        // 使用Id进行加盐
        admin.setPwd(SecureUtil.md5(pwd+adminId));
        try {
            Admin oldAdmin = adminDao.findByAdminId(admin.getAdminId());
            BeanUtil.copyProperties(admin,oldAdmin,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            adminDao.saveAndFlush(oldAdmin);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "admins",allEntries = true)
    public int add(Admin admin) {
        String pwd = admin.getCardId();
        pwd = pwd.substring(pwd.length()-6).toUpperCase();
        Integer adminId = admin.getAdminId();
        admin.setPwd(SecureUtil.md5(pwd+adminId));
        adminDao.saveAndFlush(admin);
        return 1;
    }
}
