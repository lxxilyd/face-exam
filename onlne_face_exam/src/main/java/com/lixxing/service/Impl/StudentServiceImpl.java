package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.crypto.SecureUtil;
import com.lixxing.dao.StudentDao;
import com.lixxing.model.Student;
import com.lixxing.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentDao studentDao;

    @Override
    @Cacheable(value = "students")
    public Page<Student> findAll(Pageable pageable) {
        return studentDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "students")
    public List<Student> findAll() {
        return studentDao.findAll();
    }

    @Override
    @Cacheable(value = "students#3#3")
    public Student findById(Integer studentId) {
        return studentDao.findByStudentId(studentId);
    }

    @Override
    @Cacheable(value = "students")
    public Student findByIdAndEmail(Integer studentId, String email) {
        return studentDao.findByStudentIdAndEmail(studentId,email);
    }

    @Override
    @CacheEvict(value = "students",allEntries = true)
    public int deleteById(Integer studentId) {
        return studentDao.removeByStudentId(studentId);
    }

    @Override
    @CacheEvict(value = "students",allEntries = true)
    public int update(Student student) {
        try {
            Student oldStudent = studentDao.findByStudentId(student.getStudentId());
            BeanUtil.copyProperties(student,oldStudent,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            studentDao.saveAndFlush(oldStudent);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "students",allEntries = true)
    public int updatePwd(Student student) {
        String pwd = student.getPwd();
        Integer studentId = student.getStudentId();
        student.setPwd(SecureUtil.md5(pwd+studentId));
        try {
            Student oldStudent = studentDao.findByStudentId(student.getStudentId());
            BeanUtil.copyProperties(student,oldStudent,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            studentDao.saveAndFlush(oldStudent);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "students",allEntries = true)
    public int add(Student student) {
        String pwd = student.getCardId();
        pwd = pwd.substring(pwd.length()-6).toUpperCase();
        Integer studentId = student.getStudentId();
        student.setPwd(SecureUtil.md5(pwd+studentId));
        studentDao.saveAndFlush(student);
        return 1;
    }
}
