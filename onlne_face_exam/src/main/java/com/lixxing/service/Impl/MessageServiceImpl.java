package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.MessageDao;
import com.lixxing.model.Message;
import com.lixxing.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageDao messageDao;

    @Override
    @Cacheable(value = "messages")
    public Page<Message> findAll(Pageable pageable) {
        return messageDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "messages")
    public List<Message> findAll(Integer type) {
        return messageDao.findAllByType(type);
    }

    @Override
    @Cacheable(value = "messages")
    public Page<Message> findAll(Pageable pageable, Integer type) {
        return messageDao.findAllByType(pageable,type);
    }

    @Override
    @Cacheable(value = "messages")
    public Message findById(Integer id) {
        return messageDao.findByMessageId(id);
    }

    @Override
    @CacheEvict(value = "messages",allEntries = true)
    public int delete(Integer id) {
        return messageDao.removeByMessageId(id);
    }

    @Override
    @CacheEvict(value = "messages",allEntries = true)
    public int update(Message message) {
        try {
            Message oldMessage = messageDao.findByMessageId(message.getMessageId());
            BeanUtil.copyProperties(message,oldMessage,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            messageDao.saveAndFlush(oldMessage);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "messages",allEntries = true)
    public int add(Message message) {
        messageDao.saveAndFlush(message);
        return 1;
    }
}
