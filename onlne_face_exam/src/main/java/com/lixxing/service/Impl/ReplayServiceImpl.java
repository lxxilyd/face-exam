package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.ReplayDao;
import com.lixxing.model.Replay;
import com.lixxing.service.ReplayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ReplayServiceImpl implements ReplayService {

    @Autowired
    private ReplayDao replayDao;

    @Override
    @Cacheable(value = "replays")
    public List<Replay> findAll() {
        return replayDao.findAll();
    }

    @Override
    @Cacheable(value = "replays")
    public List<Replay> findAllById(Integer messageId) {
        return replayDao.findAllByMessageId(messageId);
    }

    @Override
    @Cacheable(value = "replays")
    public Replay findById(Integer replayId) {
        return replayDao.findByReplayId(replayId);
    }

    @Override
    @CacheEvict(value = "replays",allEntries = true)
    public int delete(Integer replayId) {
        return replayDao.removeByMessageId(replayId);
    }

    @Override
    @CacheEvict(value = "replays",allEntries = true)
    public int update(Replay replay) {
        try {
            Replay oldReplay = replayDao.findByReplayId(replay.getReplayId());
            BeanUtil.copyProperties(replay,oldReplay,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            replayDao.saveAndFlush(oldReplay);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "replays",allEntries = true)
    public int add(Replay replay) {
        replayDao.saveAndFlush(replay);
        return 1;
    }
}
