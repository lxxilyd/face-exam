package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.FillQuestionDao;
import com.lixxing.dao.JudgeQuestionDao;
import com.lixxing.dao.MultiQuestionDao;
import com.lixxing.dao.PaperDao;
import com.lixxing.model.FillQuestion;
import com.lixxing.model.JudgeQuestion;
import com.lixxing.model.MultiQuestion;
import com.lixxing.model.Paper;
import com.lixxing.service.PaperService;
import com.lixxing.service.PaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class PaperServiceImpl implements PaperService {

    @Autowired
    PaperDao paperDao;
    @Autowired
    MultiQuestionDao multiQuestionDao;
    @Autowired
    FillQuestionDao fillQuestionDao;
    @Autowired
    JudgeQuestionDao judgeQuestionDao;

    @Override
    @CacheEvict(value = "papers",allEntries = true)
    public int add(Paper paper) {
        Integer totilScore = 0;
        // 计算题目数量
        paper.setQuestionNum(paper.getFillQuestions().size()+
                paper.getJudgeQuestions().size()+
                paper.getMultiQuestions().size());
        // 计算总分
        if(paper.getMultiQuestions().size() > 0){
            for (MultiQuestion multiQuestion : paper.getMultiQuestions()) {
                multiQuestion = multiQuestionDao.findByQuestionId(multiQuestion.getQuestionId());
                totilScore += multiQuestion.getScore();
            }
        }
        if(paper.getFillQuestions().size() > 0){
            for (FillQuestion fillQuestion : paper.getFillQuestions()) {
                fillQuestion = fillQuestionDao.getById(fillQuestion.getQuestionId());
                totilScore += fillQuestion.getScore();
            }
        }
        if(paper.getJudgeQuestions().size() > 0){
            for (JudgeQuestion judgeQuestion : paper.getJudgeQuestions()) {
                judgeQuestion = judgeQuestionDao.getById(judgeQuestion.getQuestionId());
                totilScore += judgeQuestion.getScore();
            }
        }
        paper.setTotilScore(totilScore);
        paperDao.saveAndFlush(paper);
        return 1;
    }

    @Override
    @CacheEvict(value = "papers",allEntries = true)
    public int delete(Integer paperId) {
        return paperDao.removeByPaperId(paperId);
    }

    @Override
    @CacheEvict(value = "papers",allEntries = true)
    public int update(Paper paper) {
        try {
            Paper oldPaper = paperDao.findByPaperId(paper.getPaperId());
            BeanUtil.copyProperties(paper,oldPaper,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            Integer totilScore = 0;
            // 计算题目数量
            oldPaper.setQuestionNum(paper.getFillQuestions().size()+
                    paper.getJudgeQuestions().size()+
                    paper.getMultiQuestions().size());
            // 计算总分
            if(paper.getMultiQuestions().size() > 0){
                for (MultiQuestion multiQuestion : paper.getMultiQuestions()) {
                    multiQuestion = multiQuestionDao.findByQuestionId(multiQuestion.getQuestionId());
                    totilScore += multiQuestion.getScore();
                }
            }
            if(paper.getFillQuestions().size() > 0){
                for (FillQuestion fillQuestion : paper.getFillQuestions()) {
                    fillQuestion = fillQuestionDao.getById(fillQuestion.getQuestionId());
                    totilScore += fillQuestion.getScore();
                }
            }
            if(paper.getJudgeQuestions().size() > 0){
                for (JudgeQuestion judgeQuestion : paper.getJudgeQuestions()) {
                    judgeQuestion = judgeQuestionDao.getById(judgeQuestion.getQuestionId());
                    totilScore += judgeQuestion.getScore();
                }
            }
            oldPaper.setTotilScore(totilScore);
            paperDao.saveAndFlush(oldPaper);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @Cacheable(value = "papers")
    public Paper findById(Integer paperId) {
        return paperDao.findByPaperId(paperId);
    }

    @Override
    @Cacheable(value = "papers")
    public Page<Paper> findAll(Pageable pageable) {
        return paperDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "papers")
    public List<Paper> findAll() {
        return paperDao.findAll();
    }
}
