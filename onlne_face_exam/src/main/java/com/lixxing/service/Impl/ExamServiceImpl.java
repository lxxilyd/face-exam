package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.ExamDao;
import com.lixxing.model.*;
import com.lixxing.service.ExamService;
import com.lixxing.utils.CloneUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Transactional
@Service
public class ExamServiceImpl implements ExamService {

    @Autowired
    ExamDao examDao;

    @Override
    @CacheEvict(value = "exams",allEntries = true)
    public int add(Exam exam) {
        examDao.saveAndFlush(exam);
        return 1;
    }

    @Override
    @CacheEvict(value = "exams",allEntries = true)
    public int delete(Integer examId) {
        return examDao.removeByExamCode(examId);
    }

    @Override
    @CacheEvict(value = "exams",allEntries = true)
    public int update(Exam exam) {
        try {
            Exam oldExam = examDao.findByExamCode(exam.getExamCode());
            BeanUtil.copyProperties(exam,oldExam,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            examDao.saveAndFlush(oldExam);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @Cacheable(value = "exams")
    public Exam findById(Integer examId) {
        return setExamStatus(examDao.findByExamCode(examId));
    }

    @Override
//    @Cacheable(value = "exams")
    public Exam findByTypeAndId(Integer type, Integer examId) {
        Exam exam = setExamStatus(examDao.findByExamCode(examId));
        // 深克隆避免影响数据库数据
        Exam exam1 = null;
        try {
             exam1 = CloneUtil.deepClone(exam);
        }catch (Exception e){
            e.printStackTrace();
        }
        long timeMillis = System.currentTimeMillis();
        long time = exam1.getExamStart().getTime();
        int usedTime = (int) ((timeMillis-time) / 1000L / 60);
        if (time > timeMillis){
            exam1.setExamDuration(exam1.getExamDuration() - usedTime);
        }else {
            exam1.setExamDuration(exam1.getExamDuration() - usedTime);
        }
        Paper paper = exam1.getPaper();
        Set<JudgeQuestion> judgeQuestions = paper.getJudgeQuestions();
        Set<FillQuestion> fillQuestions = paper.getFillQuestions();
        Set<MultiQuestion> multiQuestions = paper.getMultiQuestions();
        // 判断考试类型，若不是练习则去除答案
        if (type == 1 || type == 0){
            for (JudgeQuestion judgeQuestion : judgeQuestions) {
                judgeQuestion.setAnswer(null);
                judgeQuestion.setAnalysis(null);
            }
            for (FillQuestion fillQuestion : fillQuestions) {
                fillQuestion.setAnswer(null);
                fillQuestion.setAnalysis(null);
            }
            for (MultiQuestion multiQuestion : multiQuestions) {
                multiQuestion.setAnswer(null);
                multiQuestion.setAnalysis(null);
            }
            paper.setJudgeQuestions(judgeQuestions);
            paper.setFillQuestions(fillQuestions);
            paper.setMultiQuestions(multiQuestions);
            exam1.setPaper(paper);
        }
        return exam1;
    }

    @Override
    @Cacheable(value = "exams")
    public Page<Exam> findAll(Pageable pageable) {
        return setExamStatus(examDao.findAll(pageable),pageable);
    }

    @Override
    @Cacheable(value = "exams")
    public List<Exam> findAll() {
        return setExamStatus(examDao.findAll());
    }

    @Override
//    @Cacheable(value = "exams")
    public Page<Exam> findAllByClazzs(Pageable pageable, Set<Clazz> clazzs) {
        return setExamStatus(examDao.findAllByClazzsIn(pageable,clazzs),pageable);
    }

    @Override
    @Cacheable(value = "exams")
    public Page<Exam> findAllByTypeAndClazzs(Pageable pageable, Set<Clazz> clazzs,Integer type) {
        return setExamStatus(examDao.findAllByTypeAndClazzsIn(pageable,type,clazzs),
                pageable);
    }

    @Override
//    @Cacheable(value = "exams")
    public List<Exam> findAllByClazzs(Set<Clazz> clazzs) {
        return setExamStatus(examDao.findAllByClazzsIn(clazzs));
    }

    /**
     * 设置考试状态
     * @param all
     * @param pageable
     * @return
     */
    private Page<Exam> setExamStatus(Page<Exam> all,Pageable pageable) {
        List<Exam> exams = all.getContent();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        for (Exam exam : exams) {
            if (timestamp.before(exam.getExamStart())){
                exam.setStatus(0);
            }
            else if (timestamp.before(new Timestamp(
                    exam.getExamStart().getTime()+(exam.getExamDuration()*60*1000)))){
                exam.setStatus(1);
            }
            else {
                exam.setStatus(2);
            }
        }
        return new PageImpl<>(exams,pageable,all.getSize());
    }

    /**
     * 设置考试状态
     * @param all
     * @return
     */
    private List<Exam> setExamStatus(List<Exam> all) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        for (Exam exam : all) {
            if (timestamp.before(exam.getExamStart())){
                exam.setStatus(0);
            }
            else if (timestamp.before(new Timestamp(
                    exam.getExamStart().getTime()+(exam.getExamDuration()*60*1000)))){
                exam.setStatus(1);
            }
            else {
                exam.setStatus(2);
            }
        }
        return all;
    }

    /**
     * 设置考试状态
     * @param exam
     * @return
     */
    private Exam setExamStatus(Exam exam) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        if (timestamp.before(exam.getExamStart())){
            exam.setStatus(0);
        }
        else if (timestamp.before(new Timestamp(
                exam.getExamStart().getTime()+(exam.getExamDuration()*60*1000)))){
            exam.setStatus(1);
        }
        else {
            exam.setStatus(2);
        }
        return exam;
    }
}
