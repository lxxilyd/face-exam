package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.crypto.SecureUtil;
import com.lixxing.dao.TeacherDao;
import com.lixxing.model.Teacher;
import com.lixxing.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    private TeacherDao teacherDao;

    @Override
    @Cacheable(value = "teachers")
    public Page<Teacher> findAll(Pageable pageable) {
        return teacherDao.findAll(pageable);
    }

    @Override
    @Cacheable(value = "teachers")
    public List<Teacher> findAll() {
        return teacherDao.findAll();
    }

    @Override
    @Cacheable(value = "teachers")
    public Teacher findById(Integer teacherId) {
        return teacherDao.findByTeacherId(teacherId);
    }

    @Override
    @Cacheable(value = "teachers")
    public Teacher findByIdAndEmail(Integer teacherId, String email) {
        return teacherDao.findByTeacherIdAndEmail(teacherId,email);
    }

    @Override
    @CacheEvict(value = "teachers",allEntries = true)
    public int deleteById(Integer teacherId) {
        return teacherDao.removeByTeacherId(teacherId);
    }

    @Override
    @CacheEvict(value = "teachers",allEntries = true)
    public int update(Teacher teacher) {
        try {
            Teacher oldTeacher = teacherDao.findByTeacherId(teacher.getTeacherId());
            BeanUtil.copyProperties(teacher,oldTeacher,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            teacherDao.saveAndFlush(oldTeacher);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "teachers",allEntries = true)
    public int updatePwd(Teacher teacher) {
        String pwd = teacher.getPwd();
        Integer teacherId = teacher.getTeacherId();
        // 使用Id进行加盐
        teacher.setPwd(SecureUtil.md5(pwd+teacherId));
        try {
            Teacher oldTeacher = teacherDao.findByTeacherId(teacher.getTeacherId());
            BeanUtil.copyProperties(teacher,oldTeacher,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            teacherDao.saveAndFlush(oldTeacher);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @CacheEvict(value = "teachers",allEntries = true)
    public int add(Teacher teacher) {
        String pwd = teacher.getCardId();
        pwd = pwd.substring(pwd.length()-6).toUpperCase();
        Integer teacherId = teacher.getTeacherId();
        // 使用Id进行加盐
        teacher.setPwd(SecureUtil.md5(pwd+teacherId));
        teacherDao.saveAndFlush(teacher);
        return 1;
    }
}
