package com.lixxing.service;

import com.lixxing.model.FillQuestion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface FillQuestionService {

    List<FillQuestion> findByIdAndType(Integer paperId);

    Page<FillQuestion> findAll(Pageable pageable);

    Page<FillQuestion> findAll(Pageable pageable,Integer courseId);

    FillQuestion findOnlyQuestionId();

    FillQuestion findByQuestionId(Integer questionId);

    int add(FillQuestion fillQuestion);

    int update(FillQuestion fillQuestion);

    int delete(Integer questionId);

    List<Integer> findBySubject(String subject, Integer pageNo);
}
