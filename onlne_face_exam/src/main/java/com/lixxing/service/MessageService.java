package com.lixxing.service;

import com.lixxing.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MessageService {
    Page<Message> findAll(Pageable pageable);

    Page<Message> findAll(Pageable pageable,Integer type);

    List<Message> findAll(Integer type);

    Message findById(Integer id);

    int delete(Integer id);

    int update(Message message);

    int add(Message message);
}
