package com.lixxing.service;

import com.lixxing.model.Admin;

import java.util.List;

public interface AdminService{

    public List<Admin> findAll();

    public Admin findById(Integer adminId);

    public Admin findByIdAndEmail(Integer adminId,String email);

    public int deleteById(int adminId);

    public int update(Admin admin);

    public int updatePwd(Admin admin);

    public int add(Admin admin);
}
