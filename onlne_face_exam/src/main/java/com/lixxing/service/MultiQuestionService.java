package com.lixxing.service;

import com.lixxing.model.MultiQuestion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MultiQuestionService {

    List<MultiQuestion> findByIdAndType(Integer PaperId);

    List<MultiQuestion> findAll();

    Page<MultiQuestion> findAll(Pageable pageable);

    Page<MultiQuestion> findAll(Pageable pageable,Integer courseId);

    MultiQuestion findOnlyQuestionId();

    MultiQuestion findByQuestionId(Integer PaperId);

    int add(MultiQuestion multiQuestion);

    int update(MultiQuestion multiQuestion);

    int delete(Integer questionId);

    List<Integer> findBySubject(String subject, Integer pageNo);
}
