package com.lixxing.service;

import com.lixxing.model.Clazz;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ClazzService {

    public int add(Clazz clazz);

    public int delete(Integer clazzId);

    public int update(Clazz clazz);

    public Clazz findById(Integer clazzId);

    public Page<Clazz> findAll(Pageable pageable);

    public List<Clazz> findAll();
}
