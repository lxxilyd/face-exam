package com.lixxing.service;

import com.lixxing.model.Teacher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TeacherService {

    Page<Teacher> findAll(Pageable pageable);

    public List<Teacher> findAll();

    public Teacher findById(Integer teacherId);

    public Teacher findByIdAndEmail(Integer teacherId,String email);

    public int deleteById(Integer teacherId);

    public int update(Teacher teacher);

    public int updatePwd(Teacher teacher);

    public int add(Teacher teacher);
}
