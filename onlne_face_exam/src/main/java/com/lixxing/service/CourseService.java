package com.lixxing.service;

import com.lixxing.model.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CourseService {

    public int add(Course course);

    public int delete(Integer courseId);

    public int update(Course course);

    public Course findById(Integer courseId);

    public Page<Course> findAll(Pageable pageable);

    public List<Course> findAll();
}
