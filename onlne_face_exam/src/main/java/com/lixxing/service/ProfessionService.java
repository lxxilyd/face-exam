package com.lixxing.service;

import com.lixxing.model.Profession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProfessionService {

    public int add(Profession profession);

    public int delete(Integer professionId);

    public int update(Profession profession);

    public Profession findById(Integer professionId);

    public Page<Profession> findAll(Pageable pageable);

    public List<Profession> findAll();
}
