package com.lixxing.service;


import com.lixxing.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface StudentExamStatusService {
    Page<StudentExamStatus> findAll(Pageable pageable);

    List<StudentExamStatus> findAll();

    List<StudentExamStatus> findAllByStudentAndExam(Student student, Exam exam);

    StudentExamStatus findById(StatusId statusId);

    StudentExamStatus findByStudentAndExam(Student student, Exam exam);

    StudentExamStatus findByStudentAndExamAndId(StatusId statusId, Student student, Exam exam);

    int delete(StatusId statusId);

    int update(StudentExamStatus status);

    int add(StudentExamStatus status);
    
}
