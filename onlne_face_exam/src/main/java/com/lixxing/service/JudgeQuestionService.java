package com.lixxing.service;


import com.lixxing.model.JudgeQuestion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface JudgeQuestionService {

    List<JudgeQuestion> findByIdAndType(Integer paperId);

    Page<JudgeQuestion> findAll(Pageable pageable);

    Page<JudgeQuestion> findAll(Pageable pageable,Integer courseId);

    JudgeQuestion findOnlyQuestionId();

    int add(JudgeQuestion judgeQuestion);

    int update(JudgeQuestion judgeQuestion);

    int delete(Integer questionId);

    JudgeQuestion findByQuestionId(Integer questionId);

    List<Integer> findBySubject(String subject, Integer pageNo);
}
