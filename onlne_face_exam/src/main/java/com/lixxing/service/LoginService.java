package com.lixxing.service;

import com.lixxing.model.Admin;
import com.lixxing.model.Student;
import com.lixxing.model.Teacher;

public interface LoginService {

    public Admin adminLogin(Integer username, String password);

    public Teacher teacherLogin(Integer username, String password);

    public Student studentLogin(Integer username, String password);
}
