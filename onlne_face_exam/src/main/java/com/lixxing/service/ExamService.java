package com.lixxing.service;

import com.lixxing.model.Clazz;
import com.lixxing.model.Exam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface ExamService {

    public int add(Exam exam);

    public int delete(Integer examId);

    public int update(Exam exam);

    public Exam findById(Integer examId);

    public Exam findByTypeAndId(Integer type,Integer examId);

    public Page<Exam> findAll(Pageable pageable);

    public List<Exam> findAll();

    public Page<Exam> findAllByClazzs(Pageable pageable, Set<Clazz> clazzs);

    public Page<Exam> findAllByTypeAndClazzs(Pageable pageable, Set<Clazz> clazzs,Integer type);

    public List<Exam> findAllByClazzs(Set<Clazz> clazzs);
}
