package com.lixxing.advice;

import com.lixxing.exception.RolesException;
import com.lixxing.exception.TokenAuthExpiredException;
import com.lixxing.model.ApiResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestControllerAdvice
public class GlobalControllerAdvice {
    private static final String BAD_REQUEST_MSG = "客户端请求参数错误";
    private Logger log = LoggerFactory.getLogger(GlobalControllerAdvice.class);
    @ExceptionHandler(TokenAuthExpiredException.class)
    public ApiResult tokenAuthExpiredExceptionHandler(TokenAuthExpiredException e) {
        return new ApiResult(401, "登录信息已失效，请重新登录", null);
    }
    @ExceptionHandler(RolesException.class)
    public ApiResult rolesAuthExpiredExceptionHandler(RolesException e) {
        return new ApiResult(402, "该用户无角色权限", null);
    }
}