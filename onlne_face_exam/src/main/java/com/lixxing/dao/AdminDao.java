package com.lixxing.dao;

import com.lixxing.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdminDao extends JpaRepository<Admin,Integer> {

    public List<Admin> findAll();

    public Admin findByAdminId(Integer adminId);

    public Admin findByAdminIdAndEmail(Integer adminId,String email);

    public int removeByAdminId(int adminId);

    public Admin findByAdminIdAndPwd(Integer adminId,String pwd);
}
