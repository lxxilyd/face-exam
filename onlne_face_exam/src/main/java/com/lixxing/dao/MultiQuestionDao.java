package com.lixxing.dao;

import com.lixxing.model.Course;
import com.lixxing.model.MultiQuestion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

//选择题
@Repository
public interface MultiQuestionDao extends JpaRepository<MultiQuestion,Integer> {

    @Query(nativeQuery = true,value = "select * from multi_question where question_id in (select question_id from paper_manage where question_type = 1 and paper_id = :paperId)")
    List<MultiQuestion> findByIdAndType(@Param("paperId") Integer PaperId);

    Page<MultiQuestion> findAll(Pageable pageable);

    Page<MultiQuestion> findAllByCourse(Pageable pageable, Course course);

    MultiQuestion findByQuestionId(Integer questionId);

    MultiQuestion findByQuestionIdAndAnswer(Integer questionId,String answer);

    int removeByQuestionId(Integer questionId);

    /**
     * 查询最后一条记录的questionId
     * @return MultiQuestion
     */
    MultiQuestion findTopByOrderByQuestionIdDesc();

    @Query(nativeQuery = true,value = "select question_id from multi_question  where subject =#{subject} order by rand() desc limit #{pageNo}")
    List<Integer> findBySubject(String subject, Integer pageNo);
}
