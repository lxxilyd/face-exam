package com.lixxing.dao;

import com.lixxing.model.Clazz;
import com.lixxing.model.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClazzDao extends JpaRepository<Clazz,Integer> {

    public Clazz findByClazzId(Integer clazzId);

    public Page<Clazz> findAll(Pageable pageable);

    public List<Clazz> findAll();

    public int removeByClazzId(Integer clazzId);
}
