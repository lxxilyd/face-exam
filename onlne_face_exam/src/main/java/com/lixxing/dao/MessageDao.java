package com.lixxing.dao;


import com.lixxing.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MessageDao extends JpaRepository<Message,Integer> {
    Page<Message> findAll(Pageable page);

    Page<Message> findAllByType(Pageable page,Integer type);

    List<Message> findAllByType(Integer type);

    Message findByMessageId(Integer id);

    int removeByMessageId(Integer id);
}
