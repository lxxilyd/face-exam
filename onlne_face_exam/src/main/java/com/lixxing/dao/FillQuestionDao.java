package com.lixxing.dao;

import com.lixxing.model.Course;
import com.lixxing.model.FillQuestion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

//填空题
@Repository
public interface FillQuestionDao extends JpaRepository<FillQuestion,Integer> {

    @Query(nativeQuery = true,value = "select * from fill_question where question_id in (select question_id from paper_manage where question_type = 2 and paper_id = :paperId)")
    List<FillQuestion> findAll(@Param("paperId") Integer paperId);

    Page<FillQuestion> findAll(Pageable pageable);

    Page<FillQuestion> findAllByCourse(Pageable pageable, Course course);

    /**
     * 查询最后一条questionId
     * @return FillQuestion
     */
    FillQuestion findTopByOrderByQuestionIdDesc();

    FillQuestion findByQuestionId(Integer questionId);

    FillQuestion findByQuestionIdAndAnswer(Integer questionId,String answer);

    int removeByQuestionId(Integer questionId);

    @Query(nativeQuery = true,value = "select questionId from fill_question where subject = #{subject} order by rand() desc limit #{pageNo}")
    List<Integer> findBySubject(String subject, Integer pageNo);
}
