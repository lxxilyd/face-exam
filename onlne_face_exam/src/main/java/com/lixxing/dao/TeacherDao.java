package com.lixxing.dao;

import com.lixxing.model.Teacher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherDao extends JpaRepository<Teacher,Integer> {

    Page<Teacher> findAll(Pageable pageable);

    public List<Teacher> findAll();

    public Teacher findByTeacherId(Integer teacherId);

    public Teacher findByTeacherIdAndPwd(Integer teacherId,String pwd);

    public Teacher findByTeacherIdAndEmail(Integer teacherId,String email);

    public int removeByTeacherId(Integer teacherId);

}
