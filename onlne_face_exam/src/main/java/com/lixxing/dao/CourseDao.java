package com.lixxing.dao;

import com.lixxing.model.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseDao extends JpaRepository<Course,Integer> {

    public Course findByCourseId(Integer courseId);

    public Page<Course> findAll(Pageable pageable);

    public List<Course> findAll();

    public int removeByCourseId(Integer courseId);
}
