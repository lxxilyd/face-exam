package com.lixxing.dao;

import com.lixxing.model.Answer;
import com.lixxing.model.Exam;
import com.lixxing.model.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerDao extends JpaRepository<Answer,Integer> {

    Page<Answer> findAll(Pageable pageable);
//
    @Override
    List<Answer> findAll();

    List<Answer> findAllByStudentAndExam(Student student,Exam exam);

    Answer findByAnswerId(Long answerId);

    int removeByAnswerId(Long answerId);

    Answer findByAnswerIdAndStudentAndExam(Long answerId, Student student, Exam exam);

}
