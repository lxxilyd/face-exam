package com.lixxing.dao;

import com.lixxing.model.Paper;
import com.lixxing.model.Paper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaperDao extends JpaRepository<Paper,Integer> {

    public Paper findByPaperId(Integer paperId);

    public Page<Paper> findAll(Pageable pageable);

    public List<Paper> findAll();

    public int removeByPaperId(Integer paperId);
}
