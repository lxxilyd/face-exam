package com.lixxing.dao;

import com.lixxing.model.Replay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReplayDao extends JpaRepository<Replay,Integer> {

    List<Replay> findAll();

    List<Replay> findAllByMessageId(Integer messageId);

    Replay findByMessageId(Integer messageId);

    Replay findByReplayId(Integer replayId);

    int removeByMessageId(Integer replayId);
}
