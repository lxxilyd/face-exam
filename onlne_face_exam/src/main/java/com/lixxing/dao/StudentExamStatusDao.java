package com.lixxing.dao;

import com.lixxing.model.StatusId;
import com.lixxing.model.StudentExamStatus;
import com.lixxing.model.Exam;
import com.lixxing.model.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentExamStatusDao extends JpaRepository<StudentExamStatus,Integer> {

    Page<StudentExamStatus> findAll(Pageable pageable);
//
    @Override
    List<StudentExamStatus> findAll();

    List<StudentExamStatus> findAllByStudentAndExam(Student student, Exam exam);

    StudentExamStatus findByStatusId(StatusId statusId);

    int removeByStatusId(StatusId statusId);

    StudentExamStatus findByStatusIdAndStudentAndExam(StatusId statusId, Student student, Exam exam);

    StudentExamStatus findByStudentAndExam(Student student, Exam exam);

}
