package com.lixxing.dao;

import com.lixxing.model.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface StudentDao extends JpaRepository<Student,Integer> {

    /**
     * 分页查询所有学生
     * @param pageable
     * @return List<Student>
     */
    public Page<Student> findAll(Pageable pageable);

    public List<Student> findAll();

    public Student findByStudentId(Integer studentId);

    public Student findByStudentIdAndPwd(Integer studentId,String pwd);
    public Student findByStudentIdAndEmail(Integer studentId,String email);

    public int removeByStudentId(Integer studentId);

}
