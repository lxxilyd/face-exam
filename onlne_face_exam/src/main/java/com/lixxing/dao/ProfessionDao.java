package com.lixxing.dao;

import com.lixxing.model.Profession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProfessionDao extends JpaRepository<Profession,Integer> {

    public Profession findByProfessionId(Integer professionId);

    public Page<Profession> findAll(Pageable pageable);

    public List<Profession> findAll();

    public int removeByProfessionId(Integer professionId);
}
