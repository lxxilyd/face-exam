package com.lixxing.dao;

import com.lixxing.model.Course;
import com.lixxing.model.JudgeQuestion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

//判断题
@Repository
public interface JudgeQuestionDao extends JpaRepository<JudgeQuestion,Integer> {

    @Query(nativeQuery = true,value = "select * from judge_question where question_id in (select question_id from paper_manage where question_type = 3 and paper_id = :paperId)")
    List<JudgeQuestion> findByIdAndType(@Param("paperId") Integer paperId);

    Page<JudgeQuestion> findAll(Pageable page);

    Page<JudgeQuestion> findAllByCourse(Pageable page, Course course);

    JudgeQuestion findByQuestionId(Integer questionId);

    JudgeQuestion findByQuestionIdAndAnswer(Integer questionId,String answer);

    int removeByQuestionId(Integer questionId);

    /**
     * 查询最后一条记录的questionId
     * @return JudgeQuestion
     */
    JudgeQuestion findTopByOrderByQuestionIdDesc();

    @Query(nativeQuery = true,value = "select questionId from judge_question  where subject=#{subject}  order by rand() desc limit #{pageNo}")
    List<Integer> findBySubject(String subject, Integer pageNo);
}
