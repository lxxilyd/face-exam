package com.lixxing.dao;

import com.lixxing.model.Clazz;
import com.lixxing.model.Exam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ExamDao extends JpaRepository<Exam,Integer> {

    List<Exam> findAll();

    Page<Exam> findAll(Pageable pageable);

    List<Exam> findAllByClazzsIn(Set<Clazz> clazzs);

    Page<Exam> findAllByClazzsIn(Pageable pageable,Set<Clazz> clazzs);

    Page<Exam> findAllByTypeAndClazzsIn(Pageable pageable,Integer type,Set<Clazz> clazzs);

    Exam findByExamCode(Integer examCode);

    int removeByExamCode(Integer examCode);


}
