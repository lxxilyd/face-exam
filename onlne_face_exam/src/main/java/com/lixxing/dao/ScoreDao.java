package com.lixxing.dao;

import com.lixxing.model.Exam;
import com.lixxing.model.Score;
import com.lixxing.model.Student;
import com.lixxing.utils.ExportUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ScoreDao extends JpaRepository<Score,Integer> {

    List<Score> findAll();

    Page<Score> findAll(Pageable pageable);

    // 分页
    Page<Score> findAllByStudent(Pageable pageable, Student student);

    // 不分页
    List<Score> findAllByStudent(Student student);

    List<Score> findAllByExam(Exam exam);

    Page<Score> findAllByExam(Pageable pageable, Exam exam);

    Score findByScoreId(Integer scoreId);
}
