package com.lixxing.dao;

import com.lixxing.model.Admin;
import com.lixxing.model.Student;
import com.lixxing.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LoginDao {

    @Autowired
    private AdminDao adminDao;
    @Autowired
    private TeacherDao teacherDao;
    @Autowired
    private StudentDao studentDao;

    public Admin adminLogin (Integer adminId, String password){
        return adminDao.findByAdminIdAndPwd(adminId,password);
    }

    public Teacher teacherLogin(Integer username, String password){
        return teacherDao.findByTeacherIdAndPwd(username,password);
    }

    public Student studentLogin(Integer username, String password){
        return studentDao.findByStudentIdAndPwd(username,password);
    }
}
