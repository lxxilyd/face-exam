package com.lixxing.utils;

import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.deepoove.poi.XWPFTemplate;
import com.lixxing.model.*;
import org.springframework.core.io.ClassPathResource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExportUtil {
    public static byte[] getExportWord(Paper paper) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        try {
            inputStream = new ClassPathResource("templates/template2.docx").getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        XWPFTemplate template = XWPFTemplate.compile(inputStream);
        Map<String,Object> model = new HashMap<>();
        model.put("title",paper.getPaperName());
        model.put("total",paper.getQuestionNum());
        model.put("totalScore",paper.getTotilScore());
        // 全局题号
        int index = 1;
        // 填充选择题数据
        List<Map<String,Object>> multiQuestions = new ArrayList<>();
        for (MultiQuestion multiQuestion : paper.getMultiQuestions()) {
            // Map集合保存当前题目信息
            Map<String,Object> multiMap = new HashMap<>();
            multiMap.put("index",index++);
            multiMap.put("question",multiQuestion.getQuestion());
            multiMap.put("score",multiQuestion.getScore());
            multiMap.put("answerA",multiQuestion.getAnswerA());
            multiMap.put("answerB",multiQuestion.getAnswerB());
            multiMap.put("answerC",multiQuestion.getAnswerC());
            multiMap.put("answerD",multiQuestion.getAnswerD());
            multiQuestions.add(multiMap);
        }
        // 填充填空题数据
        List<Map<String,Object>> fillQuestions = new ArrayList<>();
        for (FillQuestion fillQuestion : paper.getFillQuestions()) {
            Map<String,Object> fillMap = new HashMap<>();
            fillMap.put("index",index++);
            fillMap.put("question",fillQuestion.getQuestion());
            fillMap.put("score",fillQuestion.getScore());
            fillQuestions.add(fillMap);
        }
        // 填充填空题数据
        List<Map<String,Object>> judgeQuestions = new ArrayList<>();
        for (JudgeQuestion judgeQuestion : paper.getJudgeQuestions()) {
            Map<String,Object> judgeMap = new HashMap<>();
            judgeMap.put("index",index++);
            judgeMap.put("question",judgeQuestion.getQuestion());
            judgeMap.put("score",judgeQuestion.getScore());
            judgeQuestions.add(judgeMap);
        }
        model.put("multiQuestions",multiQuestions);
        model.put("fillQuestions",fillQuestions);
        model.put("judgeQuestions",judgeQuestions);
        template.render(model);
        try {
            template.writeAndClose(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] paperExport = outputStream.toByteArray();
        return  paperExport;
    }

    public static byte[] getExportExcel(List<Score> scores) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ExcelWriter excelWriter = ExcelUtil.getWriter();
        List<Map<String,Object>> excel = new ArrayList<>();
        for (Score score : scores) {
            Map<String,Object> model = new HashMap<>();
            model.put("学号",score.getStudent().getStudentId());
            model.put("姓名",score.getStudent().getStudentName());
            model.put("班级",score.getStudent().getClazz().getClazzName());
            model.put("考试名称",score.getExam().getExamName());
            model.put("成绩",score.getScore());
            model.put("开始时间",score.getStartTime());
            model.put("结束时间",score.getEndTime());
            excel.add(model);
        }
        excelWriter.autoSizeColumnAll();
        excelWriter.write(excel,true);
        excelWriter.flush(outputStream);
        byte[] paperExport = outputStream.toByteArray();
        return paperExport;
    }
}
