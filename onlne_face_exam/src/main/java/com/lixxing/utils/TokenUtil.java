package com.lixxing.utils;

import cn.hutool.jwt.JWT;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.jwt.JWTPayload;
import cn.hutool.jwt.JWTUtil;
import com.lixxing.exception.TokenAuthExpiredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.Map;

@Component
public class TokenUtil {
    @Value("${token.privateKey}")
    private String privateKey;
    @Value("${token.aliveTime}")
    private Integer aliveTime;

    private Logger log = LoggerFactory.getLogger(TokenUtil.class);

    /**
     * 加密token
     */
    public String getToken(String userId, String userRole) {
        DateTime now = DateTime.now();
        DateTime newTime = now.offsetNew(DateField.MINUTE, aliveTime);
        Map<String,Object> payload = new HashMap<>();
        //签发时间
        payload.put(JWTPayload.ISSUED_AT, now);
        //过期时间
        payload.put(JWTPayload.EXPIRES_AT, newTime);
        //生效时间
        payload.put(JWTPayload.NOT_BEFORE, now);
        payload.put("userId" ,userId);
        payload.put("userRole" ,userRole);
        String token = JWTUtil.createToken(payload,privateKey.getBytes());
        return token;
    }

    /**
     * 解析token.
     */
    public Map<String, String> parseToken(String token)  {
        if(token==null){
            throw new TokenAuthExpiredException();
        }
        HashMap<String, String> map = new HashMap<>();
        try {
            JWT jwt = JWTUtil.parseToken(token);
            JWTPayload payload = jwt.getPayload();
            String userId = payload.getClaim("userId").toString();
            String userRole = payload.getClaim("userRole").toString();
            String expriedTime = payload.getClaim(JWTPayload.EXPIRES_AT).toString();
            String startTime = payload.getClaim(JWTPayload.NOT_BEFORE).toString();
            map.put("userId", userId);
            map.put("userRole", userRole);
            map.put("startTime",startTime);
            map.put("expriedTime",expriedTime);
        } catch (Exception e) {
            e.printStackTrace();
            throw new TokenAuthExpiredException();
        }
        return map;
    }
}
