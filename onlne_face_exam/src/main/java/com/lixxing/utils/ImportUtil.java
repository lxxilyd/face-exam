package com.lixxing.utils;

import com.lixxing.model.ApiResult;
import com.lixxing.model.MultiQuestion;
import com.lixxing.model.Paper;
import com.lixxing.model.systemInfo.SystemInfo;
import org.apache.poi.ooxml.extractor.POIXMLTextExtractor;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;

public class ImportUtil {
    public static void ImportWord(MultipartFile multipartFile) throws IOException, OpenXML4JException {
//        multipartFile.transferTo(new File("F:/test.docx"));
        String filename = multipartFile.getOriginalFilename();
        byte[] bytes = multipartFile.getBytes();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        if (filename.endsWith(".docx")) {
            XWPFDocument document = new XWPFDocument(inputStream);
            POIXMLTextExtractor extractor = new XWPFWordExtractor(document);
            String text = extractor.getText();
            extractor.close();
            String[] texts = text.split("选择题|填空题");
//            System.out.println(texts[1]);
            Set<MultiQuestion> multiQuestions = new HashSet<>();
            Paper paper = new Paper();
            // 试卷名称
            paper.setPaperName(filename.replace(".docx",""));
            // 匹配选择题
            String[] split = texts[1].split("\n");
            for (int i = 0; i < split.length/7; i++) {
                MultiQuestion multiQuestion = new MultiQuestion();
                multiQuestion.setQuestionId(i);
                multiQuestion.setQuestionType(1);
                multiQuestion.setQuestion(split[(i*7)+1].replace("\\d.",""));
                multiQuestion.setScore(Integer.parseInt(split[(i*7)+2].replace("分","")));
                multiQuestion.setAnswerA(split[(i*7)+3].replace("A.",""));
                multiQuestion.setAnswerB(split[(i*7)+4].replace("B.",""));
                multiQuestion.setAnswerC(split[(i*7)+5].replace("C.",""));
                multiQuestion.setAnswerD(split[(i*7)+6].replace("D.",""));
                multiQuestion.setAnalysis(split[(i*7)+7].replace("解析:",""));
                multiQuestions.add(multiQuestion);
            }
            paper.setMultiQuestions(multiQuestions);
            System.out.println(multiQuestions);
        }
        System.out.println(filename);
    }
}
