package com.lixxing.utils;

import com.lixxing.model.systemInfo.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hyperic.sigar.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@ApiModel("系统信息")
public class SysInfo {
    @ApiModelProperty("CPU信息")
    private List<CPU> cpus = new ArrayList<>();
    @ApiModelProperty("内存信息")
    private Memory memory = new Memory();
    @ApiModelProperty("磁盘信息")
    private List<Disk> disks = new ArrayList<>();
    @ApiModelProperty("操作系统信息")
    private OS os = new OS();
    @ApiModelProperty("网卡信息")
    private NetWork netWork = new NetWork();
    @ApiModelProperty("系统信息")
    SystemInfo systemInfo = new SystemInfo();

    public SystemInfo getSystemInfo() {
        try {
            setCpuInfo();
            setMemoryInfo();
            setFileSystemInfo();
            setOSInfo();
            setNetWorkInfo();
            systemInfo.setCPU(cpus);
            systemInfo.setDisk(disks);
            systemInfo.setMemory(memory);
            systemInfo.setNetWork(netWork);
            systemInfo.setOs(os);
            return systemInfo;
        } catch (Exception e){
            e.printStackTrace();
            return systemInfo;
        }
    }

    /**
     * 1.CPU资源信息
     */

    // a)CPU数量（单位：个）
    private int getCpuCount() throws SigarException {
        Sigar sigar = new Sigar();
        try {
            return sigar.getCpuInfoList().length;
        } finally {
            sigar.close();
        }
    }
    // b)CPU的总量（单位：HZ）及CPU的相关信息
    private void setCpuInfo() {
        Sigar sigar = new Sigar();
        CpuInfo[] infos;
        CpuPerc[] cpuList = null;
        DecimalFormat df = new DecimalFormat("0.00");
        cpus.clear();
        try {
            infos = sigar.getCpuInfoList();
            cpuList = sigar.getCpuPercList();
            for (int i = 0; i < cpuList.length; i++) {// 不管是单块CPU还是多CPU都适用
                CPU cpu = new CPU();
                cpu.setCores(getCpuCount());
                CpuInfo info = infos[i];
                cpu.setMaker(info.getVendor());
                cpu.setModel(info.getModel());
                cpu.setCpuUsage(Double.parseDouble(df.format(cpuList[i].getCombined()*100)));
                cpu.setCpuFreeage(Double.parseDouble(df.format(cpuList[i].getIdle()*100)));
                cpus.add(cpu);
            }
        } catch (SigarException e) {
            e.printStackTrace();
        }
    }
    /**
     * 2.内存资源信息
     *
     */
    private void setMemoryInfo() {
        // a)物理内存信息
        DecimalFormat df = new DecimalFormat("#0.00");
        Sigar sigar = new Sigar();
        Mem mem;
        try {
            mem = sigar.getMem();
            // 内存总量
            memory.setPhyTotal(Double.parseDouble(df.format((float)mem.getTotal() / 1024/1024/1024)));
            // 当前内存使用量
            memory.setPhyUsed(Double.parseDouble(df.format((float)mem.getUsed() / 1024/1024/1024)));
            // 当前内存剩余量
            memory.setPhyFree(Double.parseDouble(df.format((float)mem.getFree() / 1024/1024/1024)));
            memory.setPhyUsage(Double.parseDouble(df.format(memory.getPhyUsed()/memory.getPhyTotal()*100)));
            // b)系统页面文件交换区信息
            Swap swap = sigar.getSwap();
            // 交换区总量
            memory.setSwapTotal(Double.parseDouble(df.format((float)swap.getTotal() / 1024/1024/1024)));
            // 当前交换区使用量
            memory.setSwapUsed(Double.parseDouble(df.format((float)swap.getUsed() / 1024/1024/1024)));
            // 当前交换区剩余量
            memory.setSwapFree(Double.parseDouble(df.format((float)swap.getFree() / 1024/1024/1024)));
            memory.setSwapUsage(Double.parseDouble(df.format((memory.getSwapUsed()/memory.getSwapTotal()*100))));
        } catch (SigarException e) {
            e.printStackTrace();
        }
    }
    /**
     * 3.操作系统信息
     *
     */
    //取当前操作系统的信息
    private void setOSInfo() {
        OperatingSystem OS = OperatingSystem.getInstance();
        Properties properties = System.getProperties();
        os.setOsArch(properties.getProperty("os.arch"));
        os.setOsName(properties.getProperty("os.name"));
        os.setOsVersion(properties.getProperty("os.version"));
        os.setOsMaker(OS.getVendor());
    }
    // 4.资源信息（主要是硬盘）
    // a)取硬盘已有的分区及其详细信息（通过sigar.getFileSystemList()来获得FileSystem列表对象，然后对其进行编历）：
    private void setFileSystemInfo() throws Exception {
        Sigar sigar = new Sigar();
        disks.clear();
        FileSystem fslist[] = sigar.getFileSystemList();
        DecimalFormat df = new DecimalFormat("#0.00");
        for (int i = 0; i < fslist.length; i++) {
            Disk disk = new Disk();
            FileSystem fs = fslist[i];
            // 分区的盘符名称
            disk.setDiskName(fs.getDirName());
            // 文件系统类型，比如 FAT32、NTFS
            disk.setDiskFileSysType(fs.getSysTypeName());
            // 文件系统类型
            FileSystemUsage usage = null;
            try {
                usage = sigar.getFileSystemUsage(fs.getDirName());
            } catch (SigarException e) {
                if (fs.getType() == 2)
                    throw e;
                continue;
            }
            if (fs.getType() == 2) {
                // 文件系统总大小
                disk.setDiskTotal(usage.getTotal()/1024/1024);
                // 文件系统剩余大小
                disk.setDiskFree(usage.getFree()/1024/1024);
                // 文件系统已经使用量
                disk.setDiskUsed(usage.getUsed()/1024/1024);
                // 文件系统资源的利用率
                disk.setDiskUsage(Double.parseDouble(df.format(usage.getUsePercent()*100)));
            }
            if (null == disk.getDiskTotal()) continue;
            disks.add(disk);
        }
        return;
    }
    // d)获取网络流量等信息
    private void setNetWorkInfo() throws Exception {
        Sigar sigar = new Sigar();
        String ifNames[] = sigar.getNetInterfaceList();
        for (int i = 0; i < ifNames.length; i++) {
            String name = ifNames[i];
            NetInterfaceConfig ifconfig = sigar.getNetInterfaceConfig(name);
            if ("0.0.0.0".equals(ifconfig.getAddress())) continue;
            if ((ifconfig.getFlags() & 1L) <= 0L) continue;
            try {
                NetInterfaceStat ifstat = sigar.getNetInterfaceStat(name);
                if (ifstat.getRxBytes() == 0) continue;
                long start = System.currentTimeMillis();
                NetInterfaceStat statStart = sigar.getNetInterfaceStat(name);
                long rxBytesStart = statStart.getRxBytes();
                long txBytesStart = statStart.getTxBytes();
                Thread.sleep(100);
                long end = System.currentTimeMillis();
                NetInterfaceStat statEnd = sigar.getNetInterfaceStat(name);
                long rxBytesEnd = statEnd.getRxBytes();
                long txBytesEnd = statEnd.getTxBytes();
                long rxbps = (rxBytesEnd - rxBytesStart)*8/(end-start)*1000;
                long txbps = (txBytesEnd - txBytesStart)*8/(end-start)*1000;
                netWork.setNetFaceName(ifconfig.getName());
                netWork.setRxbps(rxbps);
                netWork.setTxbps(txbps);
                netWork.setRxFlow(rxBytesEnd);
                netWork.setTxFlow(txBytesEnd);
            } catch (SigarNotImplementedException e) {
            } catch (SigarException e) {
                e.printStackTrace();
            }
        }
    }
}