package com.lixxing.config;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

//AuditorAware 实现类，指定当前操作用户信息
@Component
public class UserAuditorConfig implements AuditorAware {

    @Override
    public Optional getCurrentAuditor() {
        return Optional.of("admin");
    }
}
