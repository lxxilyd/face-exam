package com.lixxing.config;

import com.lixxing.interceptor.AuthHandlerInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AuthWebMvcConfigurer implements WebMvcConfigurer {
    @Autowired
    private AuthHandlerInterceptor authHandlerInterceptor;

    /**
     * 配置拦截器,拦截转向到 authHandlerInterceptor
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        String[] swaggerExcludePath = { "/doc.html/**","/swagger-resources/**","/webjars/**","/v2/**"};
        registry.addInterceptor(authHandlerInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/login","/resetPassword")
                .excludePathPatterns("/getVerifyCode","/getEmailVerifyCode/**")
                .excludePathPatterns("/view/**")
                .excludePathPatterns(swaggerExcludePath);
    }

    @Bean
    public AuthHandlerInterceptor authenticationInterceptor() {
        return new AuthHandlerInterceptor();
    }

}