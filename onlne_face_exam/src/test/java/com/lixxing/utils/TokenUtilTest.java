package com.lixxing.utils;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class TokenUtilTest {

    @Autowired
    TokenUtil tokenUtil;
    @Test
    public void getToken() {
        System.out.println(tokenUtil.getToken("10001","管理员"));
    }

    @Test
    public void parseToken() {
        String token = tokenUtil.getToken("10001","管理员");
        System.out.println(token);
//        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0aW1lU3RhbXAiOjE2NDYzODM0MzQyNDUsIm5iZiI6MTY0NjM4MzQzNCwiZXhwIjoxNjQ2MzgzNDk0LCJ1c2VyUm9sZSI6IueuoeeQhuWRmCIsImlhdCI6MTY0NjM4MzQzNCwidXNlcklkIjoiMTAwMDEifQ.xwNLuOvCZIFjhqjFquB7jm7PgScboGu4_RSGTeuQuCM";

        try {
            Map<String,String> paseToken = tokenUtil.parseToken(token);
            System.out.println(paseToken);
        }catch (Exception e){
            e.printStackTrace();
        }
//        ;
    }
    @Test
    public void dateTime(){
        DateTime now = DateTime.now();
        Long timestamp = System.currentTimeMillis() / 1000L;
        System.out.println(timestamp);
        DateTime newTime = now.offsetNew(DateField.MINUTE, 10);

        System.out.println(newTime);
    }
    @Test
    public void veridyToken(){
        String token1 = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0aW1lU3RhbXAiOjE2NDYzODM0MzQyNDUsIm5iZiI6MTY0NjM4MzQzNCwiZXhwIjoxNjQ2MzgzNDk0LCJ1c2VyUm9sZSI6IueuoeeQhuWRmCIsImlhdCI6MTY0NjM4MzQzNCwidXNlcklkIjoiMTAwMDEifQ.xwNLuOvCZIFjhqjFquB7jm7PgScboGu4_RSGTeuQuCM";

//        String token = tokenUtil.getToken("1001","学生");

        JWT jwt = JWTUtil.parseToken(token1);
        System.out.println(jwt.validate(0));
//        System.out.println(tokenUtil.verifyTokenDate(token));
    }
}