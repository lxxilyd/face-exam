package com.lixxing.dao;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.model.Student;
import com.lixxing.model.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class StudentDaoTest {
    @Autowired
    StudentDao studentDao;

    @Test
    public void testFindAll(){
        System.out.println(studentDao.findAll());
    }

    @Test
    public void testFindById(){
        System.out.println(studentDao.findByStudentId(102));
    }

    @Test
    public void testAdd(){
        Student student = new Student();
        student.setStudentId(102);
        student.setStudentName("stu1");
        student.setPwd("123");
        studentDao.saveAndFlush(student);
        System.out.println(studentDao.findAll());
    }

    @Test
    public void testUpdate(){
        Student student = new Student();
        student.setStudentId(101);
        student.setRole("3");
        try {
            Student oldStudent = studentDao.findById(student.getStudentId()).get();
            BeanUtil.copyProperties(student,oldStudent,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            studentDao.saveAndFlush(oldStudent);
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println(studentDao.findAll());
    }

    @Test
    public void testDelete(){
//        Student student = new Student(102);
        studentDao.removeByStudentId(102);
//        studentDao.delete(student);
        System.out.println(studentDao.findAll());
    }
    @Test
    public void testFindByPage(){
        /**
         * 分页，页数从0开始。使用getContent()获取对象，默认返回总页数与查询的页数
         */
        PageRequest pageable = PageRequest.of(0,6);
        System.out.println(studentDao.findAll(pageable).getContent()+" "+studentDao.findAll(pageable));
    }
}