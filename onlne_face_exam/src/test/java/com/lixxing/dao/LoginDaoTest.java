package com.lixxing.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class LoginDaoTest {
    @Autowired
    LoginDao loginDao;

    @Test
    public void adminLogin() {
        System.out.println(loginDao.adminLogin(10003,"123"));
    }

    @Test
    public void teacherLogin() {
        System.out.println(loginDao.teacherLogin(1001,"111"));
    }

    @Test
    public void studentLogin() {
        System.out.println(loginDao.teacherLogin(1201,"111"));
    }
}