package com.lixxing.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class MessageDaoTest {
    @Autowired
    MessageDao messageDao;

    @Test
    public void testFindAll(){
        System.out.println(messageDao.findAll());
    }

    @Test
    public void testFindById(){
        System.out.println(messageDao.findByMessageId(1));
    }

    @Test
    public void testAdd(){
//        MessageManage messageManage = new MessageManage();
//        messageManage.setMessageManageId(1002);
//        messageManage.setMessageManageName("tech1");
//        messageManage.setPwd("1234");
//        messageDao.saveAndFlush(messageManage);
//        System.out.println(messageDao.findAll());
    }

    @Test
    public void testUpdate(){
//        MessageManage messageManage = new MessageManage();
//        messageManage.setMessageManageId(1001);
//        messageManage.setRole("2");
//        try {
//            MessageManage oldMessageManage = messageDao.findById(messageManage.getMessageManageId()).get();
//            BeanUtil.copyProperties(messageManage,oldMessageManage,
//                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
//            messageDao.saveAndFlush(oldMessageManage);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        System.out.println(messageDao.findAll());
    }

    @Test
    public void testDelete(){
//        MessageManage messageManage = new MessageManage(1002);
//        messageDao.delete(messageManage);
//        System.out.println(messageDao.findAll());
    }
    @Test
    public void testFindByPage(){
        /**
         * 分页，页数从0开始。使用getContent()获取对象，默认返回总页数与查询的页数
         */
        PageRequest pageable = PageRequest.of(0,1);
        System.out.println(messageDao.findAll(pageable).getContent()+" "+messageDao.findAll(pageable));
    }
}