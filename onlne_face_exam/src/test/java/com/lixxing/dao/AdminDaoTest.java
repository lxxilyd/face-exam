package com.lixxing.dao;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.model.Admin;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class AdminDaoTest {
    @Autowired
    AdminDao adminDao;

    @Test
    public void testFindAll(){
        System.out.println(adminDao.findAll());
    }

    @Test
    public void testFindById(){
        System.out.println(adminDao.findById(10003).get());
    }

    @Test
    public void testAdd(){
        Admin admin = new Admin();
        admin.setAdminId(10001);
        admin.setAdminName("admin");
        admin.setPwd("123");
        adminDao.saveAndFlush(admin);
        System.out.println(adminDao.findAll());
    }

    @Test
    public void testUpdate(){
        Admin admin = new Admin();
        admin.setAdminId(10001);
//        admin.setAdminName("admin");
//        admin.setPwd("1234");
        admin.setRole("1");
        try {
            Admin oldAdmin = adminDao.findById(admin.getAdminId()).get();
            BeanUtil.copyProperties(admin,oldAdmin,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            adminDao.saveAndFlush(oldAdmin);
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println(adminDao.findAll());
    }

    @Test
    public void testDelete(){
        Admin admin = new Admin(10002);
        adminDao.delete(admin);
        System.out.println(adminDao.findAll());
    }
    @Test
    public void testFindByPage(){
        /**
         * 分页，页数从0开始。使用getContent()获取对象，默认返回总页数与查询的页数
         */
        PageRequest pageable = PageRequest.of(0,1);
        System.out.println(adminDao.findAll(pageable).getContent()+" "+adminDao.findAll(pageable));
    }
}