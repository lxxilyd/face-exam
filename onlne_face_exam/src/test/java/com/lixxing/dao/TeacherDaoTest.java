package com.lixxing.dao;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.model.Admin;
import com.lixxing.model.Teacher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class TeacherDaoTest {
    @Autowired
    TeacherDao teacherDao;

    @Test
    public void testFindAll(){
        System.out.println(teacherDao.findAll());
    }

    @Test
    public void testFindById(){
        System.out.println(teacherDao.findByTeacherId(1002));
    }

    @Test
    public void testAdd(){
        Teacher teacher = new Teacher();
        teacher.setTeacherId(1002);
        teacher.setTeacherName("tech1");
        teacher.setPwd("1234");
        teacherDao.saveAndFlush(teacher);
        System.out.println(teacherDao.findAll());
    }

    @Test
    public void testUpdate(){
        Teacher teacher = new Teacher();
        teacher.setTeacherId(1001);
        teacher.setRole("2");
        try {
            Teacher oldTeacher = teacherDao.findById(teacher.getTeacherId()).get();
            BeanUtil.copyProperties(teacher,oldTeacher,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            teacherDao.saveAndFlush(oldTeacher);
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println(teacherDao.findAll());
    }

    @Test
    public void testDelete(){
        Teacher teacher = new Teacher(1002);
        teacherDao.delete(teacher);
        System.out.println(teacherDao.findAll());
    }
    @Test
    public void testFindByPage(){
        /**
         * 分页，页数从0开始。使用getContent()获取对象，默认返回总页数与查询的页数
         */
        PageRequest pageable = PageRequest.of(0,1);
        System.out.println(teacherDao.findAll(pageable).getContent()+" "+teacherDao.findAll(pageable));
    }
}