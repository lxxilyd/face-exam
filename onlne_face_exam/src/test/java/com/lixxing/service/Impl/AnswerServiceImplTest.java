package com.lixxing.service.Impl;

import com.lixxing.model.Exam;
import com.lixxing.model.Student;
import com.lixxing.service.AnswerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class AnswerServiceImplTest {

    @Autowired
    AnswerService answerService;
    @Test
    public void findAll() {
        PageRequest pageRequest = PageRequest.of(0, 6);
        System.out.println(answerService.findAll(pageRequest).getContent());
    }

    @Test
    public void findAll1() {
        System.out.println(answerService.findAll());
    }

    @Test
    public void findAll2() {
    }

    @Test
    public void findById() {
        System.out.println(answerService.findById(5L));
    }

    @Test
    public void delete() {
        System.out.println("65893x".toUpperCase());
    }

    @Test
    public void update() {
    }

    @Test
    public void add() {
    }

    @Test
    public void findByStudentAndExamAnd() {
        Student student = new Student(101);
        Exam exam = new Exam(2);
        System.out.println(answerService.findAllByStudentAndExam(student,exam));
    }
}