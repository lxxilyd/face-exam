package com.lixxing.service.Impl;

import com.lixxing.service.MultiQuestionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class MultiQuessionServiceImplTest {

    @Autowired
    MultiQuestionService multiQuestionService;

    @Test
    public void findAll() {
        Pageable pageable = PageRequest.of(0,6);
        System.out.println(multiQuestionService.findAll(pageable)+" "+multiQuestionService.findAll(pageable).getContent());
    }

    @Test
    public void findById() {
    }

    @Test
    public void deleteById() {
        multiQuestionService.delete(2);
        Pageable pageable = PageRequest.of(0,6);
        System.out.println(multiQuestionService.findAll(pageable));
    }

    @Test
    public void update() {
    }

    @Test
    public void updatePwd() {
    }

    @Test
    public void add() {
    }
}