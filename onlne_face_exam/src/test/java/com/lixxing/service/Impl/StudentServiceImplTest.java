package com.lixxing.service.Impl;

import com.lixxing.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class StudentServiceImplTest {

    @Autowired
    StudentService studentService;

    @Test
    public void findAll() {
        Pageable pageable = PageRequest.of(0,6);
        System.out.println(studentService.findAll(pageable)+" "+studentService.findAll(pageable).getContent());
    }

    @Test
    public void findById() {
    }

    @Test
    public void deleteById() {
        studentService.deleteById(102);
        System.out.println(studentService.findAll());
    }

    @Test
    public void update() {
    }

    @Test
    public void updatePwd() {
    }

    @Test
    public void add() {
    }
}