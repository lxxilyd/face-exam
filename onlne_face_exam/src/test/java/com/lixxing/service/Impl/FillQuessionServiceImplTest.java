package com.lixxing.service.Impl;

import com.lixxing.model.Course;
import com.lixxing.service.FillQuestionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class FillQuessionServiceImplTest {

    @Autowired
    FillQuestionService fillQuestionService;

    @Test
    public void findAll() {
        Pageable pageable = PageRequest.of(0,6);
        System.out.println(fillQuestionService.findAll(pageable).getContent());
    }

    @Test
    public void findAllByCourse() {
        Pageable pageable = PageRequest.of(0,6);
        System.out.println(fillQuestionService.findAll(pageable,204).getContent().size());
    }

    @Test
    public void findById() {
    }

    @Test
    public void deleteById() {
        fillQuestionService.delete(2);
        Pageable pageable = PageRequest.of(0,6);
        System.out.println(fillQuestionService.findAll(pageable));
    }

    @Test
    public void update() {
    }

    @Test
    public void updatePwd() {
    }

    @Test
    public void add() {
    }
}