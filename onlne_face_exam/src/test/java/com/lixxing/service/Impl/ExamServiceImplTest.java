package com.lixxing.service.Impl;

import com.lixxing.model.Exam;
import com.lixxing.service.ExamService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class ExamServiceImplTest {

    @Autowired
    ExamService examService;

    @Test
    public void findByTypeAndId() {
        System.out.println(examService.findByTypeAndId(0,2));
    }

    @Test
    public void findAll() {
        Pageable pageable = PageRequest.of(0,6);
        Page<Exam> all = examService.findAll(pageable);
        System.out.println(all.getTotalElements());
        System.out.println(all.getNumber());
    }
}