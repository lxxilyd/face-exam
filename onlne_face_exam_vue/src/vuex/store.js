import VUE from 'vue'
import VUEX from 'vuex'

VUE.use(VUEX)

const state = {
  isPractice: false, //练习模式标志
  flag: false, //菜单栏左右滑动标志
  userInfo: null,
  menu: [
    {
      index: '1',
      title: '系统管理',
      icon: 'el-icon-setting',
      content:[{item1:'系统监控',path:'/systemInfo'}],
    },
    {
      index: '2',
      title: '消息管理',
      icon: 'el-icon-s-comment',
      content:[
        {item1:'添加公告',path:'/addMessage'},
        {item2:'教务公告',path:'/techMessage'},
        {item3:'论坛消息',path:'/forumMessage'},
      ],
    },
    {
      index: '3',
      title: '试卷管理',
      icon: 'icon-tiku',
      content:[{item1:'所有试卷',path:'/allPaper'},{item2:'添加试卷',path:'/addPaper'}],
    },
    {
      index: '4',
      title: '题目管理',
      icon: 'el-icon-edit-outline',
      content:[
        {item1:'添加题目',path:'/addQuestion'},
        {item2:'选择题',path:'/allMultiQuestion'},
        {item3:'填空题',path:'/allFillQuestion'},
        {item4:'判断题',path:'/allJudjeQuestion'},
      ],
    },
    {
      index: '5',
      title: '成绩查询',
      icon: 'icon-performance',
      content:[{item1:'学生成绩查询',path:'/allStudentsGrade'},{path: '/grade'},{item2: '成绩分段查询',path: '/selectExamToPart'},{path: '/scorePart'}],
    },
  ],
}
const mutations = {
  practice(state,status) {
    state.isPractice = status
  },
  toggle(state) {
    state.flag = !state.flag
  },
  changeUserInfo(state,info) {
    state.userInfo = info
  }
}
const getters = {

}
const actions = {
  getUserInfo(context,info) {
    context.commit('changeUserInfo',info)
  },
  getPractice(context,status) {
    context.commit('practice',status)
  }
}
export default new VUEX.Store({
  state,
  mutations,
  getters,
  actions,
  // store
})
