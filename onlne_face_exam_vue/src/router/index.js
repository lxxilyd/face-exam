import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

// export default new Router({
 const routes = [
    {
      path: '/',
      name: 'index', //登录界面
      component: () => import('@/components/common/hello')
    },
    {
      path: '/login',
      name: 'login', //登录界面
      component: () => import('@/components/common/login')
    },
    {
      path: '/resetPassword',
      name: 'resetPassword', //密码重置界面
      component: () => import('@/components/common/resetPassword')
    },
    {
      path: '/index', //教师主页
      component: () => import('@/components/admin/index'),
      children: [
        {
          path: '/', //首页默认路由
          component: () => import('@/components/common/hello')
        },
        {
          path:'/grade', //学生成绩折线图
          component: () => import('@/components/charts/grade')
        },
        {
          path:'/answerDetail', //学生作答详情
          component: () => import('@/components/charts/answerDetail')
        },
        {
          path:'/getScoreTable', //成绩单
          component: () => import('@/components/charts/getScoreTable')
        },
        {
          path: '/selectExamToPart', //学生分数段
          component: () => import('@/components/teacher/selectExamToPart')
        },
        {
          path: '/scorePart',
          component: () => import('@/components/charts/scorePart')
        },
        {
          path: '/allStudentsGrade', //所有学生成绩统计
          component: () => import('@/components/teacher/allStudentsGrade')
        },
        {
          path: '/allExam', //查询所有考试
          component: () => import('@/components/teacher/allExam')
        },
        {
          path: '/addExam', //添加考试
          component: () => import('@/components/teacher/addExam')
        },
        {
          path: '/allMultiQuestion', //查询所有选择题
          component: () => import('@/components/question/allMultiQuestion')
        },
        {
          path: '/allJudjeQuestion', //查询所有判断题
          component: () => import('@/components/question/allJudgeQuestion')
        },
        {
          path: '/allFillQuestion', //查询所有填空题
          component: () => import('@/components/question/allFillQuestion')
        },
        {
          path: '/addQuestion', //添加试题
          component: () => import('@/components/teacher/addQuestion')
        },
        {
          path: '/studentManage', //学生管理界面
          component: () => import('@/components/teacher/studentManage')
        },
        {
          path: '/addStudent', //添加学生
          component: () => import('@/components/teacher/addStudent')
        },
        {
          path: '/teacherManage',
          component: () => import('@/components/admin/tacherManage')
        },
        {
          path: '/professionManage',
          component: () => import('@/components/admin/professionManage')
        },
        {
          path: '/addProfession',
          component: () => import('@/components/admin/addProfession')
        },
        {
          path: '/clazzManage',
          component: () => import('@/components/admin/clazzManage')
        },
        {
          path: '/addClazz',
          component: () => import('@/components/admin/addClazz')
        },
        {
          path: '/systemInfo',
          component: () => import('@/components/admin/systemInfo')
        },
        {
          path: '/allPaper',
          component: () => import('@/components/teacher/allPaper')
        },
        {
          path: '/paperDetail',
          component: () => import('@/components/teacher/paperDetail')
        },
        {
          path: '/addPaper',
          component: () => import('@/components/teacher/addPaper')
        },
        {
          path: '/courseManage',
          component: () => import('@/components/admin/courseManage')
        },
        {
          path: '/addCourse',
          component: () => import('@/components/admin/addCourse')
        },
        {
          path: '/addTeacher',
          component: () => import ('@/components/admin/addTeacher')
        },
        {
          path: '/addMessage',
          component: () => import ('@/components/admin/addMessage')
        },
        {
          path: '/techMessage',
          component: () => import ('@/components/admin/techMessage')
        },
        {
          path: '/forumMessage',
          component: () => import ('@/components/admin/forumMessage')
        }
      ]
    },
    {
      path: '/student',
      component: () => import('@/components/student/index'),
      children: [
        {path:"/",component: ()=> import('@/components/student/techMessage')},
        {path:"/myExam",component: ()=> import('@/components/student/myExam')},
        {path:'/myPractise', component: () => import('@/components/student/myPractise')},
        {path: '/manager', component: () => import('@/components/student/manager')},
        {path: '/registFace', component: () => import('@/components/student/registFace')},
        {path: '/examMsg', component: () => import('@/components/student/examMsg')},
        {path: '/message', component: () => import('@/components/student/message')},
        {path: '/modifyPasswd', component: () => import('@/components/student/modifyPasswd')},
        {path: '/studentScore', component: () => import("@/components/student/answerScore")},
        {path: '/scoreTable', component: () => import("@/components/student/scoreTable")},
      ]
    },
    // {path: '/answer',component: () => import('@/components/student/answer')},
    {path: '/doAnswer',component: () => import('@/components/student/doAnswer')}
  ]
  const router = new Router({
    routes
  })
  // vueRoute导航守卫（拦截器或过滤器）
  router.beforeEach((to, from, next) => {
  	// 如果用户访问的登录页，直接放行
  	if (to.path === '/login'){
      return next();
    }
    if (to.path === '/resetPassword'){
      return next();
    }
  	const tokenStr = window.sessionStorage.getItem('token');
  	// 没有token 强制跳转到登录页
  	if (!tokenStr) {
  		// 您还没有登录,请首先登录
  		Vue.prototype.$message.error('您还没有登录,请首先登录')
  		return next('/login')
  	}
  	next();//其它一律放行
  });
  export default router

// })
