// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import echarts from 'echarts'
import axios from 'axios'
import ElementUI from 'element-ui'
import '../theme/index.css'
import '../theme/qweather-icons.css'
import VueCookies from 'vue-cookies'

Vue.use(ElementUI);
Vue.use(VueCookies)
// Vue.use(Layui)
// 配置请求的根路径
// axios.defaults.baseURL = 'api'
// axios.defaults.baseURL = 'http://192.168.14.11:8082'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
// axios如果也有拦截器，就可以挂在令牌进行验证
// 请求拦截
var Auth = ''
var showMsg = true;
axios.interceptors.request.use((request => {
  Auth = window.sessionStorage.getItem('token') ? window.sessionStorage.getItem('token') : ''
	// 为请求头对象，添加token验证的Authorization字段
	request.headers.Authorization = Auth
	// 最后必须return config
	return request
}));
// 响应拦截
axios.interceptors.response.use((res => {
  if(res.headers.authorization != undefined){
    window.sessionStorage.setItem('token',res.headers.authorization)
  }
	if(res.data.code != 200){
    // 在人脸库中找到但是与身份信息不匹配
    if(res.data.code == 201){
      Vue.prototype.$message.error(res.data.message)
      return res;
    }
    // 查询结果为空
    if(res.data.code == 204) return res;
    // 在人脸库中未找到
    if(res.data.code == 205){
      Vue.prototype.$message.error(res.data.message)
      return res;
    }
	}
  if(res.data.code == 401 || res.data.code == 400){
    if(showMsg){
      //清除登录状态
      window.sessionStorage.removeItem('token')
      window.sessionStorage.removeItem('userInfo')
      Vue.prototype.$message.error(res.data.message)
      vm.$router.push('/login')
    }
    showMsg = false;
    setTimeout(()=>{
      showMsg = true;
    },3000)
  }
	return res;
}));

Vue.config.productionTip = false
Vue.prototype.bus = new Vue()
Vue.prototype.$echarts = echarts
Vue.prototype.$axios = axios


var vm = new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

// Vue vm = new Vue({
//   el: '#app',
//   router,
//   render: h => h(App),
//   components: { App },
//   template: '<App/>'
// })
