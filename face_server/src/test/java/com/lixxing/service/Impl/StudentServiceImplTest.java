package com.lixxing.service.Impl;

import com.lixxing.model.Student;
import com.lixxing.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class StudentServiceImplTest {

    @Autowired
    StudentService studentService;

    @Test
    public void findAll() {
        System.out.println(studentService.findAll());
    }

    @Test
    public void findById() {
        Student student = studentService.findById(102);
//        student.setFaceId("1666");
        System.out.println(student);
    }
}