package com.lixxing.util;

import com.lixxing.model.Face;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class IdentityPeopleTest {

    @Autowired
    IdentityPeople identityPeople;

    @Test
    public void getFaceById() {
        Face faceById = identityPeople.getFaceById("6606ddc784404efcacb05d7045e5577a");
        System.out.println(faceById);
    }
}