package com.lixxing;

import com.lixxing.frame.ShowCamera;
import com.lixxing.service.CameraFaceInfoBak;
import com.lixxing.service.CheckCameraFace;
import com.lixxing.service.CheckCameraFaceBak;
import com.lixxing.service.CheckFacePointsBak;
import com.lixxing.util.IdentityPeople;
import com.lixxing.util.IdentityPeopleBak;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameGrabber;

public class Show {
    public static void main(String[] args) throws FrameGrabber.Exception {
        //1.训练人脸库
        IdentityPeopleBak.train();
        //2.启动摄像头
        OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
        grabber.start(); // 开始获取摄像头数据
        ShowCamera showCamera = new ShowCamera();
        while (ShowCamera.close) {
            /*摄像头显示*/
//            showCamera.show(CameraFaceInfoBak.dealTheMat(grabber.grabFrame()));
            /*业务处理*/
//			showCamera.show(CheckCameraFaceBak.dealTheMat(grabber.grabFrame()));
            /*摄像头上显示人脸关键点*/
			showCamera.show(CheckFacePointsBak.dealTheMat(grabber.grabFrame()));
        }
    }
}
