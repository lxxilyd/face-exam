package com.lixxing.controller;

import com.lixxing.frame.CoverUtil;
import com.lixxing.model.Student;
import com.lixxing.service.PictureFaceService;
import com.lixxing.service.StudentService;
import com.lixxing.util.IdentityPeople;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.bytedeco.javacpp.opencv_core;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.util.Map;

@Api(tags = "人脸识别接口")
@RestController
public class FaceController {
    @Autowired
    PictureFaceService pictureFaceService;
    @Autowired
    IdentityPeople identityPeople;
    @Autowired
    StudentService studentService;

    @ApiOperation("验证人脸")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "img", value = "人脸图片" ,required = true, dataType = "MultipartFile"),
            @ApiImplicitParam(name = "studentId", value = "学生学号(Id)" ,required = true, dataType = "Integer")
    })
    @PostMapping("findFace/{studentId}")
    public Map<String, Object> findFace(@PathVariable("studentId") Integer studentId,
                                        @RequestParam("img") MultipartFile img) throws Exception {
        InputStream inputStream = img.getInputStream();
        byte[] bytes = img.getBytes();
        BufferedImage image = ImageIO.read(inputStream);
//        img.transferTo(new File("F:/testFace.png"));
        //1.训练人脸库
        identityPeople.train();
        Map<String, Object> faces = pictureFaceService.findFaces(image, studentId);
        opencv_core.Mat mat = (opencv_core.Mat) faces.get("mat");
        faces.remove("mat");
        bytes = CoverUtil.getBytesToMat(mat);
        faces.put("face",bytes);
        return faces;
    }

    @ApiOperation("获取对应Id的人脸图片")
    @ApiImplicitParam(name = "faceId", value = "面部信息Id(" ,required = true, dataType = "String")
    @GetMapping("findFaceImg/{faceId}")
    public Map<String, Object> findFaceById(@PathVariable("faceId") String faceId) throws Exception {

        Map<String, Object> faces = pictureFaceService.findFacesById(faceId);
        return faces;
    }

    @ApiOperation("注册人脸")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "img", value = "人脸图片" ,required = true, dataType = "MultipartFile"),
            @ApiImplicitParam(name = "studentId", value = "学生学号(Id)" ,required = true, dataType = "Integer")
    })
    @PostMapping("registFace/{studentId}")
    public Map<String, Object> addFace(@PathVariable("studentId")Integer studentId,
                                       @RequestParam("img") MultipartFile img)  throws Exception {
        InputStream inputStream = img.getInputStream();
        BufferedImage image = ImageIO.read(inputStream);
        //1.训练人脸库并添加
        identityPeople.train();
        Map<String, Object> faces = pictureFaceService.addFaces(image);
        opencv_core.Mat mat = (opencv_core.Mat) faces.get("mat");
        faces.remove("mat");
        byte[] bytes = CoverUtil.getBytesToMat(mat);
        faces.put("face",bytes);
        // 将人脸信息ID写入数据库
        Student student = studentService.findById(studentId);
        if (null != faces.get("faceId")){
            student.setFaceId(faces.get("faceId").toString());
            studentService.update(student);
        }
        return faces;
    }
}
