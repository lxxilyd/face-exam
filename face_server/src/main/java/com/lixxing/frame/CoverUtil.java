package com.lixxing.frame;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_imgproc;
import org.bytedeco.javacv.*;
import org.bytedeco.javacv.Frame;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.bytedeco.javacv.Java2DFrameUtils.deepCopy;

/**
 * 摄像头显示窗口-demo工具类
 *
 *
 */
public class CoverUtil {

    private static Java2DFrameConverter biConv  = new Java2DFrameConverter();
    private static OpenCVFrameConverter.ToMat matConv = new OpenCVFrameConverter.ToMat();
	private CanvasFrame canvas = new CanvasFrame("摄像头");// 新建一个窗口

	public static boolean close = true;

	public void show(Frame frame){
		canvas.showImage(frame);// 获取摄像头图像并放到窗口上显示，
	}

    /**
     * 从Mat对象中获取图片字节数组
     * @param mat
     * @return
     * @throws IOException
     */
	public static byte[] getBytesToMat(Mat mat) throws IOException {
		OpenCVFrameConverter cv = new OpenCVFrameConverter.ToIplImage();
		// OpenCV默认颜色模式为BGR，转换为RGB
		Mat rgbMat = new Mat();
//        opencv_imgproc.cvtColor(mat, rgbMat, opencv_imgproc.COLOR_BGRA2BGR555);
//        opencv_imgproc.cvtColor(mat, rgbMat, opencv_imgproc.COLOR_RGBA2RGB);
        opencv_imgproc.cvtColor(mat, rgbMat, opencv_imgproc.COLOR_RGBA2RGB);
        System.out.println(mat.channels());
        BufferedImage image = MatoBufferedImage(mat);
        if (mat.channels() == 4){
            image = MatoBufferedImage(rgbMat);
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ImageIO.write(image,"jpg",outputStream);
		byte[] img = outputStream.toByteArray();
		return img;
	}

    /**
     *
     * @param src
     * @return
     */
	public synchronized static BufferedImage MatoBufferedImage(Mat src) {
		BufferedImage bufferedImage = deepCopy(biConv.getBufferedImage(matConv.convert(src).clone()));
		return bufferedImage;
	}
}
