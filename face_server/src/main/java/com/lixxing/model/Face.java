package com.lixxing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.bytedeco.javacpp.opencv_core.Mat;

@ApiModel
public class Face{
	@ApiModelProperty(value = "人脸信息Id")
	private String faceId;
	@ApiModelProperty(value = "人脸Mat对象，保存图片信息")
	private Mat faceMat;
	@ApiModelProperty(value = "人脸匹配置信度")
	private double confidence;
	public String getfaceId() {
		return faceId;
	}
	public void setfaceId(String faceId) {
		this.faceId = faceId;
	}
	public Mat getFaceMat() {
		return faceMat;
	}
	public void setFaceMat(Mat faceMat) {
		this.faceMat = faceMat;
	}

	public double getConfidence() {
		return confidence;
	}

	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}

	@Override
	public String toString() {
		return "Face{" +
				"id='" + faceId + '\'' +
				", confidence=" + confidence +
				'}';
	}
}