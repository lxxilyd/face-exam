package com.lixxing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

/**
 * 专业实体类
 */
@Entity
@ApiModel
public class Profession extends BaseModel implements Serializable {
    /**
     * 序列化版本号
     */
    private final static long serialVersionUID=1L;

    @Id
    @ApiModelProperty("专业编号")
    private Integer professionId;
    @ApiModelProperty("专业名称")
    private String professionName;
    @ApiModelProperty("所属学院")
    private String school;

    public Profession() {
    }

    public Profession(Integer professionId) {
        this.professionId = professionId;
    }

    public Profession(Integer professionId, String professionName, String school) {
        this.professionId = professionId;
        this.professionName = professionName;
        this.school = school;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getProfessionId() {
        return professionId;
    }

    public void setProfessionId(Integer professionId) {
        this.professionId = professionId;
    }

    public String getProfessionName() {
        return professionName;
    }

    public void setProfessionName(String professionName) {
        this.professionName = professionName;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profession that = (Profession) o;
        return Objects.equals(professionId, that.professionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(professionId);
    }

    @Override
    public String toString() {
        return "Profession{" +
                "professionId=" + professionId +
                ", professionName='" + professionName + '\'' +
                ", school='" + school + '\'' +
                '}';
    }
}
