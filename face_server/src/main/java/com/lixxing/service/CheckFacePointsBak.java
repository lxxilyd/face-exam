package com.lixxing.service;


import com.lixxing.util.CheckFaceAndEyeBak;
import com.lixxing.util.FaceAndEyeToos;
import com.lixxing.util.FaceAndEyeToosBak;
import com.lixxing.util.FaceKeyPointsBak;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.RectVector;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;

/**
 * 检测摄像头是否有人出现
 *
 */
public class CheckFacePointsBak {
	/**
	 * 从摄像头获取图片
	 * @param frame
	 * @param time
	 */
	static OpenCVFrameConverter.ToMat converter = new OpenCVFrameConverter.ToMat();
	static long begin = System.currentTimeMillis();
	
	public synchronized static Frame dealTheMat(Frame frame){ 
		//图像转换
		Mat mat = f2M(frame);
		//检测是否有人员
		RectVector faces = CheckFaceAndEyeBak.findFaces(mat);
		//不存在则直接返回
		if(faces == null){
			return m2F(mat);
		} 
		//如果存在则判断是否是会员 
		FaceAndEyeToosBak.drawCircleEye(faces, mat);
		FaceAndEyeToosBak.drawRectangleFace(faces, mat);
		FaceKeyPointsBak.drawPoints(mat);
		mat = FaceKeyPointsBak.horizontalFace(mat);
		//1发送信息给管理员
		//2放置查询信息到mat中显示
		
		return m2F(mat);
	} 
	
	/**
	 * 数据类型转换 Frame to Mat
	 * @param img
	 * @return
	 */
	public static Mat f2M(Frame img){
		return converter.convertToMat(img);
	} 
	
	/**
	 * 数据类型转换 Mat to Frame
	 * @param img
	 * @return
	 */
	public static Frame m2F(Mat img){
		return converter.convert(img);
	}
}
