package com.lixxing.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.lixxing.dao.StudentDao;
import com.lixxing.model.Student;
import com.lixxing.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentDao studentDao;

    @Override
    public Page<Student> findAll(Pageable pageable) {
        return studentDao.findAll(pageable);
    }

    @Override
    public List<Student> findAll() {
        return studentDao.findAll();
    }

    @Override
    public Student findById(Integer studentId) {
        return studentDao.findByStudentId(studentId);
    }

    @Override
    public int deleteById(Integer studentId) {
        return studentDao.removeByStudentId(studentId);
    }

    @Override
    public int update(Student student) {
        try {
            Student oldStudent = studentDao.findByStudentId(student.getStudentId());
            BeanUtil.copyProperties(student,oldStudent,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            studentDao.saveAndFlush(oldStudent);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int updatePwd(Student student) {
        try {
            Student oldStudent = studentDao.findByStudentId(student.getStudentId());
            BeanUtil.copyProperties(student,oldStudent,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            studentDao.saveAndFlush(oldStudent);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int add(Student student) {
        studentDao.saveAndFlush(student);
        return 1;
    }
}
