package com.lixxing.service;

import com.google.common.collect.Lists;
import com.lixxing.model.Face;
import com.lixxing.util.*;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.RectVector;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.image.BufferedImage;
import java.util.List;

/**
 * 检测摄像头是否有人出现
 *
 */
public class CameraFaceInfoBak {
	/**
	 * 从摄像头获取图片
	 * @param frame
	 * @param time
	 */
	static OpenCVFrameConverter.ToMat converter = new OpenCVFrameConverter.ToMat();
	static long begin = System.currentTimeMillis();
	@Autowired
	CheckFaceAndEye checkFaceAndEye;
	
	public synchronized static Frame dealTheMat(Frame frame){
		//图像转换
		Mat mat = f2M(frame);
		System.out.println("====3进行面部识别====");
		//检测是否有人员
		RectVector faces = CheckFaceAndEyeBak.findFaces(mat);
		//不存在则直接返回
		if(faces == null){
			return m2F(mat);
		}
		System.out.println("====4进行人员查找====");
		//如果存在则判断是否是会员
		
		List<Face> peoples = IdentityPeopleBak.findPeople(mat, faces);
		if(peoples.isEmpty()){
			System.out.println("====5无相关人员操作====");
			//保存陌生人
			FaceAndEyeToosBak.saveFace(faces, mat, false);
			//对图像进行标记
			//SaveImage.drawCircleEye(faces, mat); 
			//SaveImage.drawRectangleFace(faces, mat);  
			FaceAndEyeToosBak.saveOriFace(mat);
			//重新加载人脸信息
			FaceListBak.loadFaceFile();
			//重新训练
			IdentityPeopleBak.train();
			return m2F(mat);
		}
		List<String> msg = Lists.newArrayList();
		for(Face face : peoples){
			GetUserInfoService info = new GetUserInfoService();
			msg.add(info.getById(face.getfaceId()).getName());
		}
		
		FaceAndEyeToosBak.drawCircleEye(faces, mat);
		FaceAndEyeToosBak.drawRectangleFace(faces, mat);
		
		if(!msg.isEmpty()){
			mat = FaceAndEyeToosBak.putMsg(faces, mat, msg);
		}

		//1发送信息给管理员
		//2放置查询信息到mat中显示
		
		return m2F(mat);
	}

	public synchronized static Mat BidealTheMat(BufferedImage image){
		//图像转换
		OpenCVFrameConverter.ToMat frameToMat = new OpenCVFrameConverter.ToMat();
		//利用frameToMat对象的convertToMat方法将BufferedImage转换为Mat对象
		Mat mat = frameToMat.convertToMat(new Java2DFrameConverter().convert(image));
		System.out.println("====3进行面部识别====");
		//检测是否有人员
		RectVector faces = CheckFaceAndEyeBak.findFaces(mat);
		//不存在则直接返回
		if(faces == null){
			return mat;
		}
		System.out.println("====4进行人员查找====");
		//如果存在则判断是否是会员

		List<Face> peoples = IdentityPeopleBak.findPeople(mat, faces);
        System.out.println(peoples.size()+" "+peoples);
		if(peoples.isEmpty()/* || peoples.get(0).getConfidence() > 100*/){
			System.out.println("====5无相关人员操作====");
			//保存陌生人
			FaceAndEyeToosBak.saveFace(faces, mat, false);
			//对图像进行标记
			//SaveImage.drawCircleEye(faces, mat);
			//SaveImage.drawRectangleFace(faces, mat);
			FaceAndEyeToosBak.saveOriFace(mat);
			//重新加载人脸信息
			FaceListBak.loadFaceFile();
			//重新训练
			IdentityPeopleBak.train();
			return mat;
		}
		List<String> msg = Lists.newArrayList();
		for(Face face : peoples){
			GetUserInfoService info = new GetUserInfoService();
			msg.add(info.getById(face.getfaceId()).getName());
		}

		FaceAndEyeToosBak.drawCircleEye(faces, mat);
		FaceAndEyeToosBak.drawRectangleFace(faces, mat);

		if(!msg.isEmpty()){
			mat = FaceAndEyeToosBak.putMsg(faces, mat, msg);
		}


		//1发送信息给管理员
		//2放置查询信息到mat中显示

		return mat;
	}

    public synchronized static Mat addFaces(BufferedImage image){
        //图像转换
        OpenCVFrameConverter.ToMat frameToMat = new OpenCVFrameConverter.ToMat();
        //利用frameToMat对象的convertToMat方法将BufferedImage转换为Mat对象
        Mat mat = frameToMat.convertToMat(new Java2DFrameConverter().convert(image));
        System.out.println("====3进行面部识别====");
        //检测是否有人员
        RectVector faces = CheckFaceAndEyeBak.findFaces(mat);
        //不存在则直接返回
        if(faces == null){
            return mat;
        }
        System.out.println("====4进行人员查找====");
        //如果存在则判断是否是会员

        List<Face> peoples = IdentityPeopleBak.findPeople(mat, faces);
        System.out.println(peoples.size()+" "+peoples);
        if(peoples.isEmpty()/* || peoples.get(0).getConfidence() > 100*/){
            System.out.println("====5无相关人员操作====");
            //保存陌生人
			FaceAndEyeToosBak.saveFace(faces, mat, false);
            //对图像进行标记
            //SaveImage.drawCircleEye(faces, mat);
            //SaveImage.drawRectangleFace(faces, mat);
			FaceAndEyeToosBak.saveOriFace(mat);
            //重新加载人脸信息
			FaceListBak.loadFaceFile();
            //重新训练
            IdentityPeopleBak.train();
            return mat;
        }
        List<String> msg = Lists.newArrayList();
        for(Face face : peoples){
            GetUserInfoService info = new GetUserInfoService();
            msg.add(info.getById(face.getfaceId()).getName());
        }

        FaceAndEyeToosBak.drawCircleEye(faces, mat);
        FaceAndEyeToosBak.drawRectangleFace(faces, mat);

        if(!msg.isEmpty()){
            mat = FaceAndEyeToosBak.putMsg(faces, mat, msg);
        }
        //1发送信息给管理员
        //2放置查询信息到mat中显示

        return mat;
    }

    public synchronized static Mat findFaces(BufferedImage image){
        //图像转换
        OpenCVFrameConverter.ToMat frameToMat = new OpenCVFrameConverter.ToMat();
        //利用frameToMat对象的convertToMat方法将BufferedImage转换为Mat对象
        Mat mat = frameToMat.convertToMat(new Java2DFrameConverter().convert(image));
        System.out.println("====3进行面部识别====");
        //检测是否有人员
        RectVector faces = CheckFaceAndEyeBak.findFaces(mat);
        //不存在则直接返回
        if(faces == null){
            return mat;
        }
        System.out.println("====4进行人员查找====");
        //如果存在则判断是否是会员

        List<Face> peoples = IdentityPeopleBak.findPeople(mat, faces);
        System.out.println(peoples.size()+" "+peoples);
        if(peoples.isEmpty()){
            System.out.println("====未匹配到已存在的人脸信息====");
            return mat;
        }
        System.out.println("====匹配到已存在的人脸信息====\n"+peoples);
        List<String> msg = Lists.newArrayList();
        for(Face face : peoples){
            GetUserInfoService info = new GetUserInfoService();
            msg.add(info.getById(face.getfaceId()).getName());
        }

        FaceAndEyeToosBak.drawCircleEye(faces, mat);
        FaceAndEyeToosBak.drawRectangleFace(faces, mat);

        if(!msg.isEmpty()){
            mat = FaceAndEyeToosBak.putMsg(faces, mat, msg);
        }
        //1发送信息给管理员
        //2放置查询信息到mat中显示

        return mat;
    }

	/**
	 * 数据类型转换 Frame to Mat
	 * @param img
	 * @return
	 */
	public static Mat f2M(Frame img){
		return converter.convertToMat(img);
	}

	/**
	 * 数据类型转换 Mat to Frame
	 * @param img
	 * @return
	 */
	public static Frame m2F(Mat img){
		return converter.convert(img);
	}

	/**
	 * 检测图片中的人脸
	 * @param bi
	 * @return
	 */
	public static Mat findFace(BufferedImage bi) {
		System.out.println("find");
		// 将BufferedImage对象转换的JavaCV的Mat对象
		// 得到frameToMat对象
		OpenCVFrameConverter.ToMat frameToMat = new OpenCVFrameConverter.ToMat();
		//利用frameToMat对象的convertToMat方法将BufferedImage转换为Mat对象
		Mat mat = frameToMat.convertToMat(new Java2DFrameConverter().convert(bi));

		System.out.println("====3进行面部识别====");
		//检测是否有人员，并保存查找到的信息
		RectVector faces = CheckFaceAndEyeBak.findFaces(mat);
		//不存在则直接返回
		if(faces == null){
			System.out.println("未检测到人脸");
			return mat;
		}
		System.out.println("====4进行人员查找====");
		//如果存在则判断是否是会员
		List<Face> peoples = IdentityPeopleBak.findPeople(mat, faces);
		if(peoples.isEmpty()){
			System.out.println("未找到匹配的");
			return mat;
		}
		System.out.println("找到匹配的"+peoples);
		return mat;
	}

	public static Mat addFace(BufferedImage bi) {
		System.out.println("add");
		// 将BufferedImage对象转换的JavaCV的Mat对象
		// 得到frameToMat对象
		OpenCVFrameConverter.ToMat frameToMat = new OpenCVFrameConverter.ToMat();
		//利用frameToMat对象的convertToMat方法将BufferedImage转换为Mat对象
		Mat mat = frameToMat.convertToMat(new Java2DFrameConverter().convert(bi));

		System.out.println("====3进行面部识别====");
		//检测是否有人员，并保存查找到的信息
		RectVector faces = CheckFaceAndEyeBak.findFaces(mat);
		//不存在则直接返回
		if(faces == null){
			System.out.println("未检测到人脸");
			return mat;
		}
		System.out.println("====4进行人员查找====");
		//如果存在则判断是否是会员

		List<Face> peoples = IdentityPeopleBak.findPeople(mat, faces);
		if(!peoples.isEmpty()){
			System.out.println("此人已存在");
			return mat;
		}
		System.out.println("====5无相关人员操作====");
		FaceAndEyeToosBak.saveFace(faces, mat, false);
		FaceAndEyeToosBak.saveOriFace(mat);
		//重新加载人脸信息
		FaceListBak.loadFaceFile();
		//重新训练
		IdentityPeopleBak.train();

//		System.out.println(peoples);
//		List<String> msg = Lists.newArrayList();
//		for(Face face : peoples){
//			GetUserInfoService info = new GetUserInfoService();
//			msg.add(info.getById(face.getId()).getName());
//		}

		FaceAndEyeToosBak.drawCircleEye(faces, mat);
		FaceAndEyeToosBak.drawRectangleFace(faces, mat);

//		if(!msg.isEmpty()){
//			mat = FaceAndEyeToos.putMsg(faces, mat, msg);
//		}else {
//			System.out.println("信息为空");
//		}

		//1发送信息给管理员
		//2放置查询信息到mat中显示
		return mat;
	}
}
