package com.lixxing.service;

import com.lixxing.model.Face;
import com.lixxing.model.Student;
import com.lixxing.util.*;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.RectVector;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PictureFaceService {

	OpenCVFrameConverter.ToMat converter = new OpenCVFrameConverter.ToMat();

	@Autowired
	IdentityPeople identityPeople;
	@Autowired
	FaceAndEyeToos faceAndEyeToos;
	@Autowired
	StudentService studentService;

	public PictureFaceService(){}

    public synchronized Map<String,Object> addFaces(BufferedImage image){
		// 建立返回值
		Map<String,Object> map = new HashMap<>();
        //图像转换
		Mat mat = bufferedImage2Mat(image);
		//检测是否有人脸
        RectVector faces = faceAndEyeToos.findFaces(mat);
        // 存储图片元信息
		map.put("mat",mat);
        //不存在则直接返回
        if(faces == null){
            map.put("code",205);
        	map.put("message","未检测到人脸");
            return map;
        }
        List<Face> peoples = identityPeople.findPeople(mat, faces);
        if(peoples.isEmpty()/* || peoples.get(0).getConfidence() > 100*/){
            //注册人脸信息
			String faceId = faceAndEyeToos.saveFace(faces, mat, false);
			faceAndEyeToos.saveOriFace(mat);
            //重新加载人脸信息
            faceAndEyeToos.loadFaceFile();
            //重新训练
			identityPeople.train();
			map.put("faceId",faceId);
            map.put("code",200);
			map.put("message","注册人脸信息成功");
            return map;
        }
        map.put("code",205);
        map.put("message","人脸信息已存在，无需再次注册");
		faceAndEyeToos.drawCircleEye(faces, mat);
		faceAndEyeToos.drawRectangleFace(faces, mat);
        return map;
    }

    public synchronized Map<String,Object> findFaces(BufferedImage image, Integer studentId){
		// 建立返回值
		Map<String,Object> map = new HashMap<>();
		Mat mat = bufferedImage2Mat(image);
		// 存储图片元信息
        map.put("mat",mat);
        //检测是否有人员
        RectVector faces = faceAndEyeToos.findFaces(mat);
        //不存在则直接返回
        if(faces == null){
            map.put("code",205);
        	map.put("message","未检测到人脸");
            return map;
        }
        List<Face> peoples = identityPeople.findPeople(mat, faces);
        if(peoples.isEmpty()){
            map.put("code",205);
			map.put("message","未匹配到已存在的人脸信息");
			return map;
        }
		String faceId = peoples.get(0).getfaceId();
		Student student = studentService.findById(studentId);
		if (null == student){
			map.put("code",203);
			map.put("message","未找到对应学生");
			return map;
		}
		if (faceId.equals(student.getFaceId())){
			map.put("code",200);
			map.put("faceId",faceId);
			map.put("confidence",peoples.get(0).getConfidence());
			map.put("message","人脸验证成功");
			faceAndEyeToos.drawCircleEye(faces, mat);
			faceAndEyeToos.drawRectangleFace(faces, mat);
			return map;
		}
		map.put("code",201);
        map.put("faceId",faceId);
		map.put("message","匹配到已存在的人脸信息,但不是本人");
		faceAndEyeToos.drawCircleEye(faces, mat);
		faceAndEyeToos.drawRectangleFace(faces, mat);
        return map;
    }

	public synchronized Map<String,Object> findFacesById(String faceId) throws IOException {
		// 建立返回值
		Map<String,Object> map = new HashMap<>();
		map.put("faceId",faceId);
		String filePath = PathUtil.getFilePath(Common.saveFacePath)+faceId+".jpg";
		File file = new File(filePath);
		FileInputStream fis;
		ByteArrayOutputStream os;
		byte[] bytes = new byte[1024];
		if (file.exists()) {
			try {
				fis = new FileInputStream(file);
				os = new ByteArrayOutputStream();
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = fis.read(buffer)) != -1) {
					os.write(buffer, 0, len);
				}
				bytes = os.toByteArray();
				fis.close();
				os.close();
			} catch (IOException e) {
				throw new RuntimeException("读取文件失败", e);
			}
		}
		map.put("face",bytes);
		return map;
	}

	/**
	 * 数据类型转换 BufferedImage to Mat
	 * @param image
	 * @return
	 */
	private Mat bufferedImage2Mat(BufferedImage image) {
		OpenCVFrameConverter.ToMat frameToMat = new OpenCVFrameConverter.ToMat();
		return frameToMat.convertToMat(new Java2DFrameConverter().convert(image));
	}

	/**
	 * 数据类型转换 Frame to Mat
	 * @param img
	 * @return
	 */
	public Mat f2M(Frame img){
		return converter.convertToMat(img);
	}

	/**
	 * 数据类型转换 Mat to Frame
	 * @param img
	 * @return
	 */
	public Frame m2F(Mat img){
		return converter.convert(img);
	}
}
