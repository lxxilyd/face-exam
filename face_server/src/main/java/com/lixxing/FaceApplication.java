package com.lixxing;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
//@EnableOpenApi
@EnableSwaggerBootstrapUI
@SpringBootApplication
public class FaceApplication {

    public static void main(String[] args)  {
        SpringApplication.run(FaceApplication.class, args);
    }

}
