package com.lixxing.util;

import com.google.common.collect.Lists;
import com.lixxing.model.Face;
import org.apache.commons.lang.StringUtils;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.RectVector;
import org.bytedeco.javacpp.opencv_core.Size;
import org.bytedeco.javacpp.opencv_imgcodecs;
import org.bytedeco.javacpp.opencv_imgproc;

import java.io.File;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 人脸加载工具类
 *
 */
public class FaceListBak {
	
	private static List<Face> faceList = Lists.newLinkedList();
	
	/**
	 * 加载人脸
	 */
	public synchronized static void loadFaceFile(){ 
		faceList.clear();
		File files = new File(PathUtil.getFilePath(Common.saveFacePath));
		for (File file : files.listFiles()) {
			Face face = new Face(); 
			face.setFaceMat(opencv_imgcodecs.imread(file.getAbsolutePath(),opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE)); 
			face.setfaceId(StringUtils.substringBefore(file.getName(), "."));
			faceList.add(face);
		}  
		faceList.stream().sorted(Comparator.comparing(face -> face.getfaceId())).collect(Collectors.toList());//根据年龄自然顺序
	}
	
	/**
	 * 获取全部人脸信息
	 */
	public static List<Face> getFaceFiles(){
		if(faceList.isEmpty()){
			loadFaceFile();
		} 
		return faceList; 
	}
	
	/**
	 * 根据索引查询face
	 * @param index
	 * @return
	 */
	public static Face getFace(int index){
		if (index < 0 || index > 1000000000){
			return null;
		}
		return faceList.get(index);
	}

	/**
	 * 根据id查询人脸
	 * @param id
	 * @return
	 */
	public static Face getFaceById(String id){
		for (Face face : faceList) {
			if (face.getfaceId().equals(id)){
				return face;
			}
		}
		return null;
	}
	
	/**
	 * 根据id查询当前人员相关信息
	 * @param id
	 * @return
	 */
	public static List<String> getUserInfo(String id){
		return Lists.newArrayList();
	}
	
	/**
	 * 根据索引查询face
	 * @param
	 * @return
	 */
	public static Mat getFace(String path){  
		  Mat test =  opencv_imgcodecs.imread(path,opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
		  RectVector faces = CheckFaceAndEyeBak.findFaces(test);
		  Mat testImage = new Mat(test, faces.get()[0]);  
		  opencv_imgproc.resize(testImage, testImage,new Size(Common.faceWidth, Common.faceHeight));
		  return testImage;
	} 
} 
