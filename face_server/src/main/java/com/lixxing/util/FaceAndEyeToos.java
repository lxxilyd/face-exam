package com.lixxing.util;

import com.google.common.collect.Lists;
import com.lixxing.model.Face;
import org.apache.commons.lang.StringUtils;
import org.bytedeco.javacpp.*;
import org.bytedeco.javacpp.opencv_core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.IntBuffer;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class FaceAndEyeToos {

	static {
		File saveOriFacePath = new File(Common.saveOriFacePath);
		File saveFacePath = new File(Common.saveFacePath);
		File trainModelPath = new File(Common.trainModelPath);
		if (saveOriFacePath.exists()){
			saveOriFacePath.mkdirs();
		}
		if (saveFacePath.exists()){
			saveFacePath.mkdirs();
		}
		if (trainModelPath.exists()){
			trainModelPath.mkdirs();
		}
	}

	static IdWorker worker = new IdWorker(1, 1, 1);

	static String uuid = UUID.randomUUID().toString().replaceAll("-","");
	//	正面人脸
	opencv_objdetect.CascadeClassifier faceDetector = new opencv_objdetect.CascadeClassifier(PathUtil.getFilePath(Common.faceDetectPath));
	//	眼睛
	opencv_objdetect.CascadeClassifier eyesDetector = new opencv_objdetect.CascadeClassifier(PathUtil.getFilePath(Common.eyesCascadePath));

	private static List<Face> faceList = Lists.newLinkedList();

	static opencv_face.FaceRecognizer faceRecognizer = opencv_face.LBPHFaceRecognizer.create();
	/**
	 * 检测是否有正面人脸
	 * @param image
	 * @return boolean
	 */
	public boolean checkHasFace(Mat image) {
		RectVector faces = findFaces(image);
		boolean flag =  faces != null;
		faces.clear();
		return flag;
	}

	/**
	 * 检测是否有人的眼睛
	 * @param image
	 * @return boolean
	 */
	public boolean checkHasEye(Mat image) {
		RectVector eyes = findEyes(image);
		boolean flag =  eyes != null;
		eyes.clear();
		return flag;
	}

	/**
	 * 获取人脸数据
	 * @param image
	 * @return RectVector
	 */
	public RectVector findFaces(Mat image) {
		Mat imageGray = doColorHist(image);
		//进行人脸识别
		RectVector faceDetections = new RectVector();
		faceDetector.detectMultiScale(imageGray, faceDetections);
		if(faceDetections.empty()){
			return null;
		}
		return faceDetections;
	}

	/**
	 * 获取人眼数据
	 * @param image
	 * @return RectVector
	 */
	public RectVector findEyes(Mat image) {
		Mat imageGray = doColorHist(image);
		// 存储找到的眼睛矩形。
		RectVector eyes = new RectVector();
		eyesDetector.detectMultiScale(imageGray, eyes);
		if(eyes.empty()){
			return null;
		}
		return eyes;
	}

	/**
	 * 保存人脸图片
	 * @param faces
	 * @param oriFrame
	 * @param addToModel 是否添加到识别模型中
	 */
	public String saveFace(RectVector faces,Mat oriFrame,boolean addToModel){
		if(faces == null || faces.empty()){
			return null;
		}
		
		Mat imageGray = doColorHist(oriFrame);
		
		Mat saveFace = null;
		String faceId = "";
		for (Rect rect : faces.get()) {  
			saveFace = new Mat(oriFrame, rect); 
			opencv_imgproc.resize(saveFace, saveFace, new Size(Common.faceWidth,Common.faceHeight));
			if(saveFace.sizeof() > 0){
				faceId = doSave(saveFace,PathUtil.getFilePath(Common.saveFacePath),addToModel);
			} 
		}
		
		saveFace.close();
		imageGray.close();
		return faceId;
	}
	
	/**
	 * 在人脸上画框
	 * @param faces
	 * @param oriFrame
	 */
	public void drawRectangleFace(RectVector faces,Mat oriFrame){
		if(faces == null || faces.empty()){
			return ;
		}
		for (Rect rect : faces.get()) { 
			// 在原图上进行画框
			opencv_imgproc.rectangle(oriFrame, new Point(rect.x(), rect.y()),
					new Point(rect.x() + rect.width(), rect.y() + rect.height()), new Scalar(0, 255, 0, 0));
		} 
	} 
	
	/**
	 * 在人眼睛上画圈
	 * @param faces
	 * @param oriFrame
	 */
	public void drawCircleEye(RectVector faces, Mat oriFrame){
		if(faces == null || faces.empty()){
			return ;
		}
		//脸数值
		for (int i = 0, total = faces.get().length; i < total; i++) { 
			
			Mat faceROI = new Mat(oriFrame, faces.get()[i]);
			// 存储找到的眼睛矩形。
			RectVector eyes = findEyes(faceROI);
			if(eyes == null || eyes.empty()){
				continue;
			}
			//眼睛数组
			for (int j = 0; j < eyes.get().length; j++) {
				//眼睛的坐标点
				Point eyeCenter = new Point(faces.get()[i].x() + eyes.get()[j].x() + eyes.get()[j].width() / 2,
						faces.get()[i].y() + eyes.get()[j].y() + eyes.get()[j].height() / 2);
				//半径
				int radius = (int) Math.round((eyes.get()[j].width() + eyes.get()[j].height()) * 0.25);
				//添加圈
				opencv_imgproc.circle(oriFrame, eyeCenter, radius, new Scalar(255, 0, 0, 0), 4, 8, 0);
			}
		}  
	}
	
	/**
	 * 保存原图
	 * @param oriFrame
	 */
	public void saveOriFace(Mat oriFrame){
		doSave(oriFrame,PathUtil.getFilePath(Common.saveOriFacePath),false);
	}
	
	/**
	 * 图片保存
	 * @param oriFrame
	 * @param path
	 */
	public String doSave(Mat oriFrame,String path,boolean addToModel){
		String fileName = uuid + ".jpg";
//		String fileName = worker.nextId() + ".jpg";
		opencv_imgcodecs.imwrite(path + fileName ,oriFrame);
		if(addToModel){ 
			updateModel(opencv_imgcodecs.imread(path + fileName,opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE));
		}
		return uuid;
	}

	/**
	 * 图像二值化
	 * @param oriFrame
	 * @return
	 */
	public Mat doColorHist(Mat oriFrame){
		// jpg的RGB三通道图片
		if(oriFrame.channels() == 3){
			Mat imageGray = new Mat();
			// 将图片转换至灰度图
			opencv_imgproc.cvtColor(oriFrame, imageGray, opencv_imgproc.COLOR_BGRA2GRAY);
			// 直方图均衡化，增加对比度方便处理
			opencv_imgproc.equalizeHist(imageGray, imageGray);
			return imageGray;
		}
		// png的RGBA四通道图片
		else if (oriFrame.channels() == 4){
			Mat imageGray = new Mat();
            opencv_imgproc.cvtColor(oriFrame, oriFrame, opencv_imgproc.COLOR_BGRA2BGR);
			opencv_imgproc.cvtColor(oriFrame, imageGray, opencv_imgproc.COLOR_BGRA2GRAY);
			opencv_imgproc.equalizeHist(imageGray, imageGray);
			return imageGray;
		}
		return oriFrame;
	}
	/**
	 * 在视频上显示用户信息
	 * @param faces
	 * @param oriFrame
	 * @param msg 
	 */
	 
	public Mat putMsg(RectVector faces,Mat oriFrame,List<String> msg){
		
		for(int i=0, total=faces.get().length; i<total; i++){
			 /*不支持中文*/
			//opencv_imgproc.putText(oriFrame,msg.get(i), new Point(faces.get()[i].x(),faces.get()[i].y()), opencv_imgproc.CV_FONT_HERSHEY_SCRIPT_COMPLEX, 1, new Scalar(255, 0, 0, 0));
			/*转成 bufferdImage进行文字添加*/ 
			oriFrame = ConverterData.b2M(ConverterData.m2B(oriFrame),oriFrame.type(),msg.get(i),faces.get()[i].x(),faces.get()[i].y());
		} 
		return oriFrame;
	}

	/**
	 * 加载人脸
	 */
	public synchronized static List<Face> loadFaceFile(){
		faceList.clear();
		System.out.println(PathUtil.getFilePath(Common.saveFacePath));
		File files = new File(PathUtil.getFilePath(Common.saveFacePath));
		System.out.println(files.getAbsolutePath());
		for (File file : files.listFiles()) {
			Face face = new Face();
			face.setFaceMat(opencv_imgcodecs.imread(file.getAbsolutePath(),opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE));
			face.setfaceId(StringUtils.substringBefore(file.getName(), "."));
			faceList.add(face);
		}
		faceList.stream().sorted(Comparator.comparing(face -> face.getfaceId())).collect(Collectors.toList());//根据年龄自然顺序
        return faceList;
	}

	/**
	 * 获取全部人脸信息
	 */
	public List<Face> getFaceFiles(){
		if(faceList.isEmpty()){
			loadFaceFile();
		}
		return faceList;
	}

	/**
	 * 根据索引查询face
	 * @param index
	 * @return
	 */
	public Face getFace(int index){
		if (index <= 0 || index > 1000000000){
			return null;
		}
		return faceList.get(index);
	}

	/**
	 * 根据id查询人脸
	 * @param id
	 * @return
	 */
	public Face getFaceById(String id){
		for (Face face : faceList) {
			if (face.getfaceId().equals(id)){
				return face;
			}
		}
		return null;
	}

	/**
	 * 根据id查询当前人员相关信息
	 * @param id
	 * @return
	 */
	public List<String> getUserInfo(String id){
		return Lists.newArrayList();
	}

	/**
	 * 更新识别模型 - 目前有问题
	 * @param img
	 */
	public void updateModel(Mat img){
		int index= getFaceFiles().size()+2;
		MatVector images = new MatVector(index);
		Mat labels = new Mat(index, 1, opencv_core.CV_32SC1);
		IntBuffer labelsBuf = labels.createBuffer();
		images.put(index,img);
		labelsBuf.put(index,index);
		faceRecognizer.update(images, labels);
	}
}
