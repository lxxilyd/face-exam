package com.lixxing.util;

import com.google.common.collect.Lists;
import com.lixxing.model.Face;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.*;
import org.bytedeco.javacpp.opencv_face.FaceRecognizer;
import org.bytedeco.javacpp.opencv_face.LBPHFaceRecognizer;
import org.bytedeco.javacpp.opencv_imgproc;

import java.nio.IntBuffer;
import java.util.List;

/**
 * 进行人员识别 -- 要更换人脸识别的引擎，LHPH方便xml更新
 *
 */
public class IdentityPeopleBak {
	
	static FaceRecognizer faceRecognizer = LBPHFaceRecognizer.create();
  //EigenFaceRecognizer.create();
//    FaceRecognizer faceRecognizer = createEigenFaceRecognizer();
//    FaceRecognizer faceRecognizer = LBPHFaceRecognizer.create();
//    FaceRecognizer faceRecognizer = EigenFaceRecognizer.create();
//  FaceRecognizer recognizer = Face.createEigenFaceRecognizer(); //80, 200
//  	static FaceRecognizer faceRecognizer = opencv_face.EigenFaceRecognizer.create();//4000
//	FaceRecognizer recognizer = Face.createLBPHFaceRecognizer();//3, 8, 8, 8, 180
	
	public static List<Face> findPeople(Mat oriFrame, RectVector faces) {
		Mat imageGray = FaceAndEyeToosBak.doColorHist(oriFrame);
		getTainFile(faceRecognizer);
		faceRecognizer.setThreshold(9999);
		Mat saveFace = null; 
		List<Face> result = Lists.newArrayList();
		for (Rect rect : faces.get()) {
			saveFace = new Mat(imageGray, rect); 
			opencv_imgproc.resize(saveFace, saveFace, new Size(Common.faceWidth, Common.faceHeight));
			//人脸对齐
//			IntPointer label = new IntPointer(4);
//			DoublePointer confidence = new DoublePointer(8);
			int[] label = new int[1];
			double[] confidence = new double[1];
			faceRecognizer.predict(saveFace, label,confidence);
//			int predictedLabel = label.get(0);
			int predictedLabel = label[0];
			System.out.println("label:"+predictedLabel+" confidence:"+confidence[0]);
			Face temp = null;
			if(predictedLabel > 0){
				//查找到当前人员信息直接返回
                temp = FaceListBak.getFace(predictedLabel-1);
                temp.setConfidence(confidence[0]);
				result.add(temp);
//				result.add(FaceList.getFaceById());
			}else{
				continue;
			}
		}
		return result;
	}
	
	/**
	 * 人脸图片训练
	 */
	public static void train(){
		int faceSize = FaceListBak.getFaceFiles().size();
	    MatVector images = new MatVector(faceSize+1);
	    Mat labels = new Mat(faceSize+1, 1,opencv_core.CV_32SC1);
	    IntBuffer labelsBuf = labels.createBuffer();
	    labelsBuf.put(0,0);
	    images.put(0,FaceListBak.getFace(PathUtil.getFilePath(Common.invalidFace)));
	    for (int i = 1; i < faceSize+1; i++) {
	        Face p = FaceListBak.getFaceFiles().get(i-1);
	        images.put(i, p.getFaceMat());
	        labelsBuf.put(i, i);
	    }
	    faceRecognizer.train(images, labels);
		saveTainFile(faceRecognizer);
		System.out.println("训练人脸信息完成");
	}

	/**
	 * 人脸图片的训练及添加
	 */
	public static void trainAndAdd(){
		int faceSize = FaceListBak.getFaceFiles().size();
		MatVector images = new MatVector(faceSize+1);
		Mat labels = new Mat(faceSize+1, 1,opencv_core.CV_32SC1);
		IntBuffer labelsBuf = labels.createBuffer();
		labelsBuf.put(0,0);
		images.put(0,FaceListBak.getFace(PathUtil.getFilePath(Common.invalidFace)));
		for (int i = 1; i < faceSize+1; i++) {
			Face p = FaceListBak.getFaceFiles().get(i-1);
			images.put(i, p.getFaceMat());
			labelsBuf.put(i, i);
		}

		faceRecognizer.train(images, labels);
		saveTainFile(faceRecognizer);
		System.out.println("训练并添加人脸信息完成");
	}
	public static void main(String[] args) {
		train();
	}
	//确保读取和写入只有一个进行再做
	public static void getTainFile(FaceRecognizer faceRecognizer){
		 synchronized(IdentityPeopleBak.class){
			 String filePath = PathUtil.getFilePath(Common.trainModelPath);
			 System.out.println(filePath);
			 faceRecognizer.read(filePath);
		 }
	}
	
	public static void saveTainFile(FaceRecognizer faceRecognizer){
		 synchronized(IdentityPeopleBak.class){
			 String filePath = PathUtil.getFilePath(Common.trainModelPath);
			 System.out.println(filePath);
			 faceRecognizer.save(filePath);
		 }
	}
	
	/**
	 * 更新识别模型 - 目前有问题
	 * @param img
	 */
	public static void updateModel(Mat img){
		int index= FaceListBak.getFaceFiles().size()+2;
		MatVector images = new MatVector(index);
	    Mat labels = new Mat(index, 1,opencv_core.CV_32SC1);
	    IntBuffer labelsBuf = labels.createBuffer();  
        images.put(index,img);
        labelsBuf.put(index,index);  
		faceRecognizer.update(images, labels);
	}
}