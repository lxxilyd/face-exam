package com.lixxing.util;

import java.io.File;

/** 
 * 说明：路径工具类
 *
 * @version
 */
public class PathUtil { 
	 
	/**获取classpath1
	 * @return
	 */
	public static String getClasspath(){
		String path = (String.valueOf(Thread.currentThread().getContextClassLoader().getResource(""))).replaceAll("file:/", "").replaceAll("%20", " ").trim();
//		String path = Thread.currentThread().getClass().getClassLoader().getResource("").getPath()+"/static";
		if(path.indexOf(":") != 1){
			path = File.separator + path;
		}
		return path;
	}
	
	/**获取classpath2
	 * @return
	 */
	public static String getClassResources(){
		String path =  (String.valueOf(Thread.currentThread().getContextClassLoader().getResource(""))).replaceAll("file:/", "").replaceAll("%20", " ").trim();	
		if(path.indexOf(":") != 1){
			path = File.separator + path;
		}
		return path;
	} 
	
	 
	public static String getFilePath(String path){
//		path = PathUtil.getClassResources().concat(path);
		return getClasspath()+path;
	} 
	public static void main(String[] args) {
		System.out.println("是否存在文件"+new File("E:/code/workspace/face/target/face-0.0.1-SNAPSHOT.jar/BOOT-INF/classes/face/invalid.jpg").isFile());
	}
}
