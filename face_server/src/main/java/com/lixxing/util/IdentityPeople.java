package com.lixxing.util;

import com.google.common.collect.Lists;
import com.lixxing.model.Face;
import org.apache.commons.lang.StringUtils;
import org.bytedeco.javacpp.*;
import org.bytedeco.javacpp.opencv_core.*;
import org.bytedeco.javacpp.opencv_face.FaceRecognizer;
import org.bytedeco.javacpp.opencv_face.LBPHFaceRecognizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.IntBuffer;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 进行人员识别 -- 要更换人脸识别的引擎，LHPH方便xml更新
 *
 */
@Component
public class IdentityPeople {
	
	static FaceRecognizer faceRecognizer = LBPHFaceRecognizer.create();
	private static List<Face> faceList = Lists.newLinkedList();
	@Autowired
	FaceAndEyeToos faceAndEyeToos;

	public IdentityPeople(){}

	/**
	 * 加载人脸
	 */
	public synchronized static void loadFaceFile(){
		faceList.clear();
		File files = new File(PathUtil.getFilePath(Common.saveFacePath));
		for (File file : files.listFiles()) {
			Face face = new Face();
			System.out.println(file.getName());
			face.setFaceMat(opencv_imgcodecs.imread(file.getAbsolutePath(),opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE));
			face.setfaceId(StringUtils.substringBefore(file.getName(), "."));
			faceList.add(face);
		}
		System.out.println(faceList);
		faceList.stream().sorted(Comparator.comparing(face -> face.getfaceId())).collect(Collectors.toList());//根据年龄自然顺序
	}

	/**
	 * 获取全部人脸信息
	 */
	public List<Face> getFaceFiles(){
		if(faceList.isEmpty()){
			loadFaceFile();
		}
		return faceList;
	}

	/**
	 * 根据索引查询face
	 * @param index
	 * @return
	 */
	public Face getFace(int index){
		if (index < 0 || index > 1000000000){
			return null;
		}
		return faceList.get(index);
	}

	/**
	 * 根据id查询人脸
	 * @param id
	 * @return
	 */
	public Face getFaceById(String id){
		faceList = faceAndEyeToos.getFaceFiles();
		System.out.println(faceList);
		for (Face face : faceList) {
			if (face.getfaceId().equals(id)){
				return face;
			}
		}
		return null;
	}

	/**
	 * 根据id查询当前人员相关信息
	 * @param id
	 * @return
	 */
	public List<String> getUserInfo(String id){
		return Lists.newArrayList();
	}

	/**
	 * 根据索引查询face
	 * @param
	 * @return
	 */
	public Mat getFace(String path){
		Mat test =  opencv_imgcodecs.imread(path,opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
		RectVector faces = faceAndEyeToos.findFaces(test);
		Mat testImage = new Mat(test, faces.get()[0]);
		opencv_imgproc.resize(testImage, testImage,new Size(Common.faceWidth, Common.faceHeight));
		return testImage;
	}

	public List<Face> findPeople(Mat oriFrame, RectVector faces) {
		Mat imageGray = faceAndEyeToos.doColorHist(oriFrame);
		getTainFile(faceRecognizer);
		// 检测阈值
		faceRecognizer.setThreshold(130);
		double threshold = faceRecognizer.getThreshold();
		Mat saveFace = null;
		List<Face> result = Lists.newArrayList();
		for (Rect rect : faces.get()) {
			saveFace = new Mat(imageGray, rect); 
			opencv_imgproc.resize(saveFace, saveFace, new Size(Common.faceWidth, Common.faceHeight));
			//人脸对齐
			int[] label = new int[1];
			double[] confidence = new double[1];
			faceRecognizer.predict(saveFace, label,confidence);
			int predictedLabel = label[0];
			List<Face> faceFiles = faceAndEyeToos.getFaceFiles();
			Face temp = null;
			if(predictedLabel > 0){
				//查找到当前人员信息直接返回
                temp = faceFiles.get(predictedLabel-1);
                temp.setConfidence(confidence[0]);
				result.add(temp);
			}
			continue;
		}
		return result;
	}

	/**
	 * 人脸图片训练
	 */
	public void train(){
		int faceSize = faceAndEyeToos.getFaceFiles().size();
	    MatVector images = new MatVector(faceSize+1);
	    Mat labels = new Mat(faceSize+1, 1,opencv_core.CV_32SC1);
	    IntBuffer labelsBuf = labels.createBuffer();
	    labelsBuf.put(0,0);
	    images.put(0,getFace(PathUtil.getFilePath(Common.invalidFace)));
	    for (int i = 1; i < faceSize+1; i++) {
	        Face p = faceAndEyeToos.getFaceFiles().get(i-1);
	        images.put(i, p.getFaceMat());
	        labelsBuf.put(i, i);
	    }
		faceRecognizer.train(images, labels);
		saveTainFile(faceRecognizer);
	}

	//确保读取和写入只有一个进行再做
	public void getTainFile(FaceRecognizer faceRecognizer){
		 synchronized(IdentityPeople.class){
			 faceRecognizer.read(PathUtil.getFilePath(Common.trainModelPath));
		 }
	}
	
	public void saveTainFile(FaceRecognizer faceRecognizer){
		 synchronized(IdentityPeople.class){
			 faceRecognizer.save(PathUtil.getFilePath(Common.trainModelPath));
		 }
	}

	/**
	 * 更新识别模型 - 目前有问题
	 * @param img
	 */
	public void updateModel(Mat img){
		int index= faceAndEyeToos.getFaceFiles().size()+2;
		MatVector images = new MatVector(index);
	    Mat labels = new Mat(index, 1,opencv_core.CV_32SC1);
	    IntBuffer labelsBuf = labels.createBuffer();
        images.put(index,img);
        labelsBuf.put(index,index);
		faceRecognizer.update(images, labels);
	}
}