package com.lixxing.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.bytedeco.javacpp.presets.opencv_core;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * 公共变量 (后期放入配置文件，这样不用编译java类)
 */
public class Common {
	
	public static final Config ROOTCONFIG =  ConfigFactory.load("conf/config.conf").getConfig("config");

	/*脸部信息保存*/
	public static String saveFacePath = ROOTCONFIG.getString("saveFacePath");

	/*原始图片保存路径*/
	public static String saveOriFacePath = ROOTCONFIG.getString("saveOriFacePath");

	//人脸训练的模型保存路径
	public static String trainModelPath = ROOTCONFIG.getString("trainModelPath");

	//面部识别模型

	public static String faceDetectPath = ROOTCONFIG.getString("faceDetec");
	//眼睛识别模型

	public static String eyesCascadePath = ROOTCONFIG.getString("eyesCascade");

	//人脸关键点识别
	public static String facePointDetectPath = ROOTCONFIG.getString("facePointDetect");

	public static String facePointModelPath = ROOTCONFIG.getString("facePointModelPath");


	//对检测出的头像进行放缩处理
	public static int faceWidth = ROOTCONFIG.getInt("faceWidth");
	public static int faceHeight = ROOTCONFIG.getInt("faceHeight");

	//视频截取间隔时间毫秒
	public static long cutInterval = ROOTCONFIG.getInt("cutInterval");

	//无效的占位人脸图片
	public static String invalidFace = ROOTCONFIG.getString("invalidFacePath");

	/**
	 * 创建保存文件夹路径
	 */
	static {
		File saveFacePathFile = new File(PathUtil.getFilePath(saveFacePath));
		if (!saveFacePathFile.exists()){
			saveFacePathFile.mkdirs();
		}
//		File trainModelPathFile = new File(PathUtil.getFilePath(trainModelPath));
//		if (!trainModelPathFile.exists()){
//			trainModelPathFile.mkdirs();
//		}
		File saveOriFacePathFile = new File(PathUtil.getFilePath(saveOriFacePath));
		if (!saveOriFacePathFile.exists()){
			saveOriFacePathFile.mkdirs();
		}
	}

}
