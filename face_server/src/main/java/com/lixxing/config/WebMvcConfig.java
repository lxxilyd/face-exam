package com.lixxing.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    /**
     * 静态资源处理
     **/
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        File file = new File("/face/");
        if (!file.exists()) {
            file.mkdirs();
        }
        registry.addResourceHandler("/face/**").addResourceLocations("file:/face/");
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }
}
